> Lazy loading allows to identify resources as non-blocking and load these only when needed. This reduces the length of the critical rendering path which translates to faster load times.

## References

- https://gitlab.com/dawsoncollege/520JS/2021-08/520-study/-/blob/main/lectures/05_lazy_loading.md
