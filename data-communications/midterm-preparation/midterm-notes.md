# Midterm Notes

## Analog and Digital Signals

> Signals are the electric or electromagnetic impulses used to encode and transmit data. Computer networks and data/voice communication systems transmit signals. Signals can be analog or digital

> Analog signals are continuous signals in which one time-varying quantity represents another time-based variable. These signals occur naturally in music, voice, etc. It is harder to separate noise from an analog signal than it is to separate noise from a digital signal. Noise is unwanted electrical or electromagnetic energy that degrades the quality of signals

> Digital signals are non-continuous signals which are used to represent data as a sequence of separate values at any point in time. It can only take on one fixed number of values. You can still discern a high voltage from a low voltage. However, with too much noise you cannot discern a high voltage from a low voltage.

E.g. Devices that use analog signals:

- Telegraph
- Telephone
- Answering Machine
- Fax Machine

E.g. Devices that use digital signals:

- Computer
- Smartphone
- Internet
- Broadband/wireless

Key Differences

- An analog signal is a continuous signal whereas Digital signals are time separated signals.
- Analog signal uses a continuous range of values that help you to represent information on the other hand digital signal uses discrete 0 and 1 to represent information.

## Fundamental Signal Properties

All signals have three components:

- Amplitude - The height of the wave above or below a given reference point. Amplitude is usually measured in volts.

- Frequency - The number of times a signal makes a complete cycle within a given time frame. Frequency is measured in Hertz (Hz), or cycles per second (period = 1 / frequency).

- Phase - The position of the waveform relative to a given moment of time or relative to time zero. A change in phase can be any number of angles between 0 and 360 degrees. Phase changes often occur on common angles, such as 45, 90, 135, etc.

## Cloud Service

Cloud service provider (CSP): A third-party company providing on-demand access to computing resources without requiring the user to directly manage such resources.

- Infrastructure as a Service (IaaS) — This is the original service type. CSP customers can provision and launch any number of VMs that originally appear to be newly installed operating systems. It is the responsibility of the CSP customer to configure the VM and install any software they need to make use of the VM. E.g. DigitalOcean, Linode, Rackspace, Amazon Web Services (AWS), Cisco Metapod, Microsoft Azure, Google Compute Engine (GCE)

- Platform as a Service (PaaS) — Some businesses need more than just a new OS when they create a new VM. They may want to support application development and need development libraries, compilers, databases, and web or application services. The PaaS service type provides VMs with pre-installed and configured software that gives users a stable platform from which to start. E.g. AWS Elastic Beanstalk, Windows Azure, Heroku, Force.com, Google App Engine, Apache Stratos, OpenShift

- Software as a Service (SaaS) — Often considered to be the highest level of cloud service type, SaaS offerings rent access to specific software applications, such as SalesForce.com. SaaS customers do not want to install or manage software—they just want to use it. All of the installation, configuration, and maintenance responsibilities fall on the CSP. This service type is generally the most expensive, but the one that requires the lowest level of customer involvement. E.g. Google Workspace, Dropbox, Salesforce, Cisco WebEx, Concur, GoToMeeting

- Anything else as a Service (AaaS, also commonly called AaaS) — Extending the SaaS service type, many businesses offer their SaaS solutions as professional services, not just software their customers can use. The list of service offerings is always growing and currently includes services such as Security as a Service, Database as a Service, Blockchain as a Service, and Privacy as a Service.

## GPIO (General-Purpose Input/Output Header)

GPIO is the interface for connecting components to a Raspberry Pi. Each of the 40 GPIO pins has specific function, and most can be used as either input or output depending on your program or its function.

E.g. Here are some pins on the GPIO and their functions:

- Ground - The ground is very useful for making a common reference between all components in your circuit.

- Power - You can find 2 pins bringing 3.3V and 2 pins bringing 5V. Those pins can be used to power components such as sensors or small actuators.

- Reserved - The pins 27 and 28 are reserved pins. They are usually used for I2C communication with an EEPROM.

- GPIO - GPIO means General Purpose Input/Output. Basically that’s one pin you can use to write data to external components (output), or read data from external components (input).

https://pinout.xyz

## Serial Peripheral Interface (SPI)

> Synchronous serial bus commonly used for sending data between micro controllers and small external peripheral devices. Multiple peripheral devices can be interfaced as SPI slaves and be controlled with the SPI master. SPI uses separate clock (SCK) and data lines (MISO, MOSI), along with a select line to choose among slave devices.

https://radiostud.io/understanding-spi-in-raspberry-pi/

## Inter-Intergrated Circuit (I2C)

> I2C interface allows multiple slave integrated circuits to communicate with one or more master. I2C uses two directional lines: Serial Data Line (SDA) and Serial Clock Line (SCL)

> SPI needs 4 pins for communication; whereas I2C requires only 2 to connect any number of slaves. SPI is good for high rate full-duplex connections.

## Resistor

> Resistors are passive electrical components that limit or regulate flow of current in electronic circuits. Unit of measurement is Ohms (Ω). Bands of colour on a resistor is used to identify the resistor value. Unlike LEDs and diodes, resistors are non-polar.

## ADC (Analog to Digital Converter)

> An integrated circuit use to convert analog signals such as voltage to digital form consisting of ones and zeros

## DHCP (Dynamic Host Configuration Protocol)

> An automatic configuration protocol used to assign device attributes to devices on IP networks.

## DNS (Domain Name System)

> A hierarchical naming system that allows organizations to associate host names with IP address name spaces.

## ARP (Address Resolution Protocol)

> A communications protocol that network devices use to find a Link Layer address (MAC address) from a Network Layer address (IP address).

## OSI and TCP/IP reference model

254

### Open Systems Interconnection (OSI) Reference Model

> The OSI Reference Model is a template for building and using a network and its resources.

> From the top down, these seven layers are Application, Presentation, Session, Transport, Network, Data Link, and Physical. These layers are also referred to by number; for example, Layer 7 is the Application Layer, and Layer 1 is the Physical Layer.

> Most commonly, the functionality of the bottom two layers are implemented in hardware, and the top five are implemented through software; however, in this era of virtualization, all layers of a network stack can be implemented in software.

### OSI Reference Model Layers

- Application Layer — This is responsible for interacting with end users. The Application Layer includes all programs on a computer that interact with the network; for example, your email software is included because it must transmit and receive messages over the network. A simple game like Solitaire doesn’t fit here because it does not require the network to operate.

- Presentation Layer — This is responsible for the coding of data. The Presentation Layer includes file formats and character representations. From a security perspective, encryption generally takes place at the Presentation Layer.

- Session Layer — This is responsible for maintaining communication sessions between computers. The Session Layer creates, maintains, and disconnects communications that take place between processes over the network.

- Transport Layer — This is responsible for breaking data into packets and properly transmitting them over the network. Flow control and error checking take place at the Transport Layer.

- Network Layer — This is responsible for the logical implementation of the network. One important feature of the Network Layer, covered later in this chapter, is logical addressing. In TCP/IP networking, logical addressing takes the familiar form of IP addresses.

- Data Link Layer — This is responsible for transmitting information on computers connected to the same local area network (LAN). The Data Link Layer uses Media Access Control (MAC) addresses. Device manufacturers assign each hardware device a unique MAC address.

- Physical Layer — This is responsible for the physical operation of the network. The Physical Layer must translate the binary ones and zeros of computer language into the language of the transport medium. In the case of copper network cables, it must translate computer data into electrical pulses. In the case of fiber optics, it must translate the data into bursts of light.

### OSI Reference Model Layer Protocols

<img src="./images/osi-layers-with-protocol.png" width="60%" height="60%">
<img src="./images/osi-layers-with-protocol2.png" width="60%" height="60%">

### TCP/IP Reference Model

> An alternative to the OSI model is the Transmission Control Protocol/Internet Protocol (TCP/IP) Reference Model, which defines four layers as opposed to the seven layers in the OSI model. The TCP/IP model represents the same functionality as the OSI model, but several of the OSI layers are combined to create a streamlined model. The simplified model maps well to the backbone of Internet communications, the TCP/IP protocol suite. The biggest difference between the two models is in each model’s purpose. Although they are often used somewhat interchangeably, they address different needs. The OSI Reference Model was originally published as a prescriptive model. The OSI Reference Model provides a prescription as to how organizations should develop network software. On the other hand, the TCP/IP Reference Model was created as a descriptive model. The TCP/IP Reference Model describes how the Internet Protocol suite operates.

<img src="./images/osi-vs-tcp-ip.png" width="60%" height="60%">

### TCP/IP Reference Model Layers

- Application Layer — The Application Layer is at the top of the TCP/IP Reference Model, which maps to OSI Layers 5, 6, and 7. The Application Layer interacts with applications that need to gain access to network services.

- Transport or Host-to-Host Layer — The Transport or Host-to-Host Layer provides end-to-end delivery, which maps to OSI Layer 4 (also called the Transport Layer). This layer segments the data and adds a checksum to properly validate data to ensure that it has not been corrupted.

- Network or Internet Layer — The next layer is the Network or Internet Layer, which handles the routing of packets as they move around the network. It maps to Layer 3 (also called the Network Layer) of the OSI model.

- Physical or Network Access Layer — The Physical Layer resides at the lowest layer of the TCP/IP model and is the point at which the higher-layer protocols interface with the network transport media. When compared to the OSI model, this layer corresponds to OSI Layer 1 and Layer 2.

### TCP/IP Reference Model Layer Protocols

Application Layer Protocols:
- Dynamic Host Configuration Protocol (DHCP)
- Hypertext Transfer Protocol (HTTP)
- Internet Message Access Protocol (IMAP)
- File Transfer Protocol (FTP)
- Post Office Protocol (POP)
- Session Initiation Protocol (SIP)

Transport Layer:
- Transmission Control Protocol (TCP)
- User Datagram Protocol (UDP)

Internet Layer:
- Internet Protocol (IP)
- Internet Control Message Protocol (ICMP)
- Internet Protocol Security (IPSec)

Network Acces Layer:
- Address Resolution Protocol (ARP)
- Layer 2 Tunneling Protocol (L2TP)
- Point-to-Point Protocol (PPP)
- Media Access Control (Ethernet, Digital Subscriber Line [DSL], Fiber Distributed Data Interface (FDDI), and so on)

## TCP/IP Suite

275

Transmission Control Protocol/Internet Protocol (TCP/IP) suite: By far the most popular set of standards used today to communicate over networks, this suite of protocols takes its name from the most common two protocols at its core: Transmission Control Protocol (TCP) and Internet Protocol

> The collection of protocols that define and enable Internet communication is called the Transmission Control Protocol/Internet Protocol (TCP/IP) suite.

<img src="./images/tcp-ip-suite-protocols.png" width="60%" height="60%">

## Difference between circuit switching and packet switching networks

278

### Circuit-Switched Network

Circuit-switched: A network that sets up a circuit for each conversation. All messages during the conversation follow the same path from source to destination.

> A circuit-switched network sets up a path between the source and destination devices. The two devices use the same path, or circuit, throughout the conversation. This is the model the original analog telephone system used.

E.g. Plain old telephone service (POTS).

### Packet-Switched Network

Packet-switched: A network that chops up network messages into smaller chunks and sends each chunk, or packet, separately. Each packet can take a different path from source to destination.

> It separates messages into smaller, manageable-sized chunks called packets. Packets can vary in size but are generally between 512 bytes and 8 kilobytes in length. Different protocols may use different minimum or maximum packet sizes. Different software applications may choose to use different packet sizes even when using the same protocol. The packet size should reflect how you use the data from an effectiveness and efficiency perspective.

### Circuit-Switched & Packet-Switched

> Circuit switching occurs at the Physical Layer and retains message order, because all messages get sent as they are created along the same path. Packet switching is implemented at the Network Layer, which is further up the protocol stack and generally slower than any Physical Layer protocol. Packet switching requires work on the receiving end to enforce message order, because packets can take different routes and may arrive out of order; however, packet switching is extremely flexible and scalable.

## IP-Based Communications

286

> Each protocol addresses a different communication problem.

- TCP — TCP guarantees the delivery of a reliable stream of data between two computer programs. TCP isn’t the only protocol that organizations use to communicate on the Internet, but it’s one of the most popular. TCP operates at OSI Layer 4.

- IP — IP makes it possible to deliver packets across a complex network to a destination. IP handles the routing decisions necessary to get packets from their source to the destination. IP operates at OSI Layer 3.

## Network Topology Overview

292

### Physical Network Topology
> A computer network is how computers, servers, printers, and users share information and communicate with one another. The public Internet is a computer network allowing individuals to share information, send email, and communicate in real time via VoIP, instant message (IM chat), or store-and- forward messaging such as email. Networks interconnect network-attachable devices. A network-attachable device (sometimes simply known as a node) can physically connect to the network and has an assigned IP host address.

### Logical Network Topology

> The logical topology is how the actual network works and how you transfer data in a network. This topology view focuses more on how the network topology operates. A logical topology is not concerned with specific physical properties of the network.

### Common Network Topologies

- Point-to-point — A direct link or connection between two devices
- Ring — A shared ring connecting multiple devices together
- Star — A star-wired connection aggregation point, typically from a wiring closet
- Mesh — Multipoint connections and direct links between network devices
- Bus — A shared network transmission medium allowing only one device to communicate at a single time
- Hybrid/star-wired bus — Also known as a tree topology; a shared, star-wired connection aggregation point to a LAN switch

<img src="./images/common-network-topologies.png" width="60%" height="60%">

### Point-to-Point

> A network that consists of computers or devices that connect directly to one another is called a point-to-point network. Point-to-point communications are based on time slots and polling where communication controllers poll each dumb terminal one at a time to provide a time slot to transmit data to and from the mainframe computer. This type of network is difficult to scale. That means that it’s difficult to add many devices to the network, because each new device requires some new type of direct connection.

<img src="./images/point-to-point-network.png" width="60%" height="60%">

### Bus

> A bus topology (see FIGURE 3-14) starts with a coaxial cable that can support high-speed network communications but is subject to physical distance limitations and maximum number of devices per bus or Ethernet LAN segment. The main cable runs throughout the organization’s physical space and provides accessible connections anywhere along its length. Connecting to a bus network is easy. Any network device can attach an Ethernet transceiver to the bus, allowing for physical connectivity to the network-attached device.

<img src="./images/bus-network.png" width="60%" height="60%">

> The flexibility of a bus does come with a performance cost. Communications signaling traverses in both directions where the transceiver connects to the bus or coaxial cable. Because of this, only one device can communicate at a given time. Each network device’s transceiver must listen on the network (coaxial cable) to see if it’s available before transmitting. When two devices transmit at the same time, a collision occurs on the network, requiring both devices to retransmit again when the network is available. This led to the creation of the IEEE 802 committees focusing on both IEEE 802.3 CSMA/CD (Carrier Sense Multiple Access with Collision Detection).

### Ring

> Within token ring, all stations are connected in a logical ring while being star-wired from a centrally located wiring closet. Token ring–attached devices must have permission to transmit on the network. This permission is granted via a token that circulates around the ring.

> Token ring networks, as shown in FIGURE 3-16, move a small frame called a token. Possession of the token grants the right to transmit. If the network device that receives the available token has no information to send, it alters one bit of the token, which flags it as available for the next network attached device in line. When a network device wants to send information, it must wait for an available token, transmit information to the next upstream device, wait for the destination device to receive the data, and then a new token is released. Token ring– attached devices can transmit only when they have an available token. This eliminates communication collisions. A network collision occurs when two or more computers transmit at the same time on a bus topology. Forcing all communications in a unidirectional manner eliminates network transmission collisions

<img src="./images/ring-network.png" width="60%" height="60%">

### Star

> Network-attached devices used this star-wired cabling to physically and electrically connect to an Ethernet or token ring network. This was the beginning of star-wired topology deployments for both Ethernet and token ring networks.

<img src="./images/star-network.png" width="60%" height="60%">

> For Ethernet or IEEE 802.3 CSMA/CD networks, star wiring was used to provide a physical star, logical bus for the original IEEE 802.3i, 10Base-T standard and specification. The logical bus can be supported by either a hub or a switch. This hub or switch is typically installed in a centrally located wiring closet. Networks use two common types of devices to build star networks. A network hub is a very simple hardware device that has ports for multiple connections and echoes every message it receives to all connected ports. A hub does not make any decisions about message routing—it simply echoes all input. This simple design makes hubs easy and cheap to make. It also means that every node on the network receives messages it doesn’t care about. The other common star network device is the switch. A switch looks like a hub in that it has ports for multiple connections to network devices; however, it’s more sophisticated than a hub. A switch receives a message and examines the destination address. The switch maintains a table of connected devices and knows to which port it should forward the message, so it sends the message directly to the destination. Switches do not send messages to all destinations as hubs do.

<img src="./images/hub-switch.png" width="60%" height="60%">

### Mesh

> A mesh topology describes a network layout in which all nodes are directly connected to all other nodes. Actually, there are two different types of mesh topologies, depending on whether all or just most of the nodes are directly connected. Mesh networking is more common in wide area networking, given that alternative paths and redundancy are typically required.

#### Fully Connected Mesh

> A fully connected mesh follows the strict definition of a mesh topology; that is, every node directly connects with every other node. A fully connected mesh provides a path length of one for every connection. It also provides the maximum number of alternate paths in case any available paths fail. That means fully connected mesh topologies have a high degree of fault tolerance. Fault tolerance describes the ability to encounter a fault, or error, of some type and still be available. A fully connected mesh topology with IP routing can easily bypass a failed connection and still communicate through the mesh using alternate connections and nodes.

<img src="./images/fully-connected-mesh-network.png" width="60%" height="60%">

> The biggest drawback of a fully connected mesh topology is the required number of connections.

#### Partially Connected Mesh

> A partially connected mesh is a network topology in which network nodes connect to only some of the other nodes. Network designers identify the most active or important nodes and ensure those nodes connect to most other nodes. Lower priority nodes do not connect directly. All nodes in the network do connect, but some connections will have a path length greater than one. Partially connected mesh topologies provide more fault tolerance than many other topologies without requiring the expense of a fully connected mesh. This topology can be a good compromise when an organization needs better fault tolerance but cannot afford to set up and maintain a fully connected mesh.

<img src="./images/partially-connected-mesh-network.png" width="60%" height="60%">

### Hybrid

> Nodes that require high fault tolerance may benefit from a mesh topology. Lower usage networks may work better using a bus topology. A hybrid network that combines the two topologies can provide fault tolerance for some nodes while providing flexibility for other parts of the network.

## Internetworking

321

> Internetworking is a term used to describe connecting LANs together.

> A protocol stack is how software operates at different layers of the OSI model

### Internetworking with Bridges

> The easiest way to interconnect two networks is to place a MAC layer bridge in between the two networks. A bridge operates at the OSI Data Link Layer or Layer 2. That means it interacts with the Physical Layer and the Data Link Layer. A bridge can read the Media Access Control (MAC) layer address of each Ethernet frame to make a forwarding or filtering decision. The MAC address is a 6-byte address that is burned into a chip on the network interface card. Each MAC address is unique. A bridge examines the MAC address of each frame received, and then makes a filtering or forwarding decision based on the MAC address forwarding table.

### Internetworking with Switches

> A switch is essentially a bridge with more than two ports. A bridge generally connects two networks. A switch often has the ability to connect many devices and networks. Switches operate as an OSI Layer 2 device. They use MAC addresses and build address tables of devices connected to each physical port. Switches that connect multiple devices or networks keep track of MAC addresses on all ports. When the switch sees an Ethernet frame destined for a specific MAC address, it forwards that frame to that port.

> There are two kinds of LAN switches. A Layer 2 switch operates at the Data Link Layer and examines the MAC layer addresses of Ethernet frames. Using this information, filtering and forwarding tasks are performed by the switch.

> A Layer 3 switch operates at either the Data Link Layer or Network Layer. Layer 3 switches typically have software that lets them function like a Layer 2 switch or multi-port bridge. Layer 3 switches usually operate at the Network Layer that examines the network layer address within the Ethernet frame. A Layer 3 switch can look up the destination IP network number in its IP routing table, and then make a path determination decision. A Layer 3 switch provides each device connected to its ports with its own Ethernet LAN segment. Layer 3 switches typically have resiliency or a redundant path connection, also called a redundant circuit, to a building or campus backbone network.

### Internetworking with Routers

> A router operates at the Network Layer. Routers can make intelligent decisions on where to send packets. Instead of just reading the MAC address and forwarding a packet based on a forwarding table, routers can see the packet’s Network Layer address or IP address. The Network Layer address contains information about the destination network number and host number. In essence, routers perform a path determination calculation to find the best path for the IP packets to traverse. Typically they have redundant processors and plenty of memory, and can support lots of network connections. It takes longer for a router to examine a packet than for a Layer 2 switch or bridge. High-traffic networks may notice performance issues with multiple routers examining each IP packet to make a path determination decision.

### Internetworking with a Gateway

> A gateway interconnects two networks that use different protocols. Unlike a brouter, the gateway translates network packets from one network protocol to another. The main job with a gateway is to translate all incoming packets to a protocol compatible with the destination network. Gateways are commonly placed at entry and exit points of a network. They commonly run either as software on a computer or as a device that performs the same functions as a router.

## Switching Concepts

336

> The main difference between devices to connect networks is how much work they must do to decide where to send each packet. Higher-level devices, such as routers and gateways, operate at the OSI Network Layer or higher. The higher a device operates within the protocol stack, the more processing power and intelligence the device itself needs. Software is what powers a bridge, router, brouter, switch, or gateway; however, the more powerful and intelligent the device, the more costly it becomes. It requires more processing power to examine each Ethernet frame or IP packet and then to determine the best path to take.

### Switch Functionality

> Switches today have been engineered to support some basic functionality that all LANs require. There are seven core functions that every network designer must consider when implementing network switches for edge or access connectivity.


- Layer 2 versus Layer 3 workstation connectivity — LAN switches that are deployed in the wiring closet for workstation connectivity typically require a network backbone connection. This usually is provided via fiber-optic trunk cables. Providing resiliency to the LAN switch requires a redundant physical path or second fiber-optic trunk cable. Layer 2 or Layer 3 resiliency is required to provide redundancy and a failover backbone connection.

- MAC addressing tables — LAN switches build MAC address tables by examining the source MAC address of every Ethernet frame. It checks to see if the source MAC address is already in its address table. If the address is not in the table, it adds the source MAC addresses to the address table for each physical port on the switch. The MAC address table allows the switch to make a rapid filtering or forwarding decision. During the learning process, a switch may receive some Ethernet frames with a destination MAC address that it does not know about. In that case, the switch forwards this frame to every outbound port. Although this practice does cause a flood of packets on all network segments, the real destination node likely will send a reply. When the switch receives the reply, it learns the whereabouts of the new node and updates the MAC-layer address table.

- Forwarding/filtering — A switch takes one of two actions for each Ethernet frame when it’s received. It forwards the frame for all unknown MAC addresses or for known MAC addresses that have a distinct destination port. If the MAC address entry indicates a frame should be forwarded to the same port on which it was received, the switch drops, or filters, the frame. This action helps isolate local traffic.

- Preventing broadcast storms caused by loops — Fault-tolerant networks have a redundant path or connection. This second connection obviously can create a physical loop in the network if switches blindly forward packets between each other. A loop is any complete cycle where packets continue traveling in a circle and never reach a destination. Because of this, loop prevention or broadcast storm prevention is required. Depending on the switch, Layer 2 or Layer 3 resiliency and loop prevention techniques can be deployed. Switches commonly use the Rapid Spanning Tree Protocol (RSTP) or Multiple Spanning Tree Protocol (MSTP) to communicate with one another and identify potential loops. Spanning Tree Protocol aims to prevent broadcast storms and provide for a redundant network connection in the event of physical cable or switch failure.

- Power over Ethernet (PoE) switch ports — Many LAN switches today can support Power over Ethernet (PoE) to power IP phones, Access Points (Aps), cameras, and many other types of devices from the same RJ-45 connector on the switch. For workstation connections that support IP phones, workstations can plug directly into the back of the IP phone using a single four-pair unshielded twisted-pair cable. From here, the workstation cabling then terminates in the wiring closet for patching directly to the PoE switch. PoE is supported by the IEEE 802.3af–2003 and IEEE 802.3at– 2009 standards and specifications. IEEE 802.3af–2003 was the original PoE standard specifying up to 15.4 watts of power per RJ-45 port. IEEE 802.3at–2009 or PoE+ provides up to 25.4 watts of power per RJ-45 port.

- Virtual LANs (VLANs) — Layer 2 switches create flat topology networks. A flat topology means there is no hierarchy to the network structure. One of the drawbacks to a flat topology is that Ethernet broadcast frames are sent to all physical ports on the switch. Network segmentation and Layer 3 routers can limit the destination domain for broadcast messages, but Layer 2 switches cannot. On the other hand, switches work much faster because they operate at a lower network level. To address the problem of global network broadcasts, LAN switches allow the network designer to configure virtual LANs (VLANs). A VLAN defines a single broadcast domain where the Ethernet broadcast frame is allowed to traverse. Switches can map devices to specific VLANs and limit the scope of broadcasts to the assigned VLAN. As networks grow, VLANs can be configured to shrink the size of the broadcast domain. The IEEE 802.1Q standard defines how VLANs can be deployed within Layer 2 switches using Ethernet tagging. Tagging allows you to send specific Ethernet frames to other switches where members of the same department or workgroup reside.

- Layer 2 or Layer 3 switch resiliency—LAN switches that reside at the edge or access point in a wiring closet may require a redundant backbone connection. This connection can be based on Layer 2 or Layer 3 techniques. The important requirement to consider is how quickly the failover connection must converge. Convergence is the amount of time it takes for a network to recoup a connection. The answer to this question must be aligned to whichever applications and time-sensitive protocols must be supported. For example, VoIP and SIP protocols support real-time communication. Hence, subsecond (less than 1.0 seconds) resiliency and failover is critical; otherwise, IP packets will be dropped. This can terminate a voice, video, or real-time communication.

### Switch Forwarding Methods

- Cut-through switching — As soon as the LAN switch reads the destination MAC address, it starts to forward the packet to the destination and does not examine the entire frame. Once the MAC destination address is identified in the MAC address forwarding table, off it goes. No additional processing or storage in memory is required. This method of forwarding increases network throughput and performance.

- Store-and-forward switching — LAN switches that use this method must receive the entire Ethernet frame and then make a forwarding decision based on the MAC address table. The advantage of this method is that the device can drop any packets with errors prior to forwarding them. This method uses less of a network’s capacity because it never forwards partial Ethernet frames; however, it increases latency due to the time required to collect the entire Ethernet frame. Store-and- forward switching tends to work best for networks that encounter high error rates. Because the Ethernet frame has a checksum, frame receipt can be verified, and the integrity of the frame is validated with a valid checksum. A checksum is how a network interface card verifies that the frame integrity is valid when received. In the event of an invalid checksum, the receiving workstation will request a retransmission of the errant frame. This method does not work well when using low-grade unshielded twisted-pair cabling.

## Routing Concepts

345

- Network Layer Address - Layer 3 routing uses the IP address to determine a packet’s destination. Because the IP address is used by Layer 3 devices, it is often called the packet’s Network Layer address.

- Static Route - A static route is a specific rule that defines how to handle specific network packets. Static routing rules can apply to specific IP addresses or to a range of IP addresses.

- Dynamic Route - A dynamic route is a path to a destination that is discovered at run time, not previously defined. When using dynamic routing, each router learns how to forward packets from its neighbors. Each time the router encounters an unknown IP network (from the packet’s IP address), it asks its neighbors if they know anything about the destination.

## Resiliency and Redundancy

348

> Resiliency is the ability to recover from a failure. In the case of networks, resiliency means the ability to identify alternate routes around any failures to maintain uninterrupted packet delivery. Redundancy is a key component to resiliency. Redundancy just means that there are multiple ways to carry out a task, such as transporting communication to its destination. Redundancy may duplicate effort and functionality, but it provides resiliency if any component fails.

### Layer 2 Resiliency

> Several protocols, including Spanning Tree Protocol (STP) and Resilient Ethernet Protocol (REP) are designed to find alternate routes for LAN segment failures. Enabling resiliency protocols is generally easy and recommended to avoid network communication interruptions.

### Layer 3 Resiliency

> Layer 3 resiliency supports alternate routes to a WAN when the primary route is unavailable. One solution is Cisco’s Hot Standby Routing Protocol (HSRP). HSRP aggregates multiple routers and presents them as a single virtual router to the endpoint device. If one of the routers becomes unavailable, HSRP transparently sends packets to another router and on to their destinations.

### Virtual Networking Components

> Network devices also can be virtualized. That means you can build entire networks using virtual machines and containers. Containers are lightweight virtual machines that share some resources with similar containers. Docker (https://www.docker.com) is a popular product that implements containers, called Docker images.

### Network Storage Types

- Storage area network — A storage area network (SAN) is a network architecture that allows multiple disks, disk arrays, and tape devices in a network and allows network nodes to access devices as if they are connected locally.

- Network attached storage — A network attached storage (NAS) system differs from a SAN in that an NAS system provides a generic interface for the underlying storage devices so they can be accessed by computers and devices running different operating systems.

## Transmission Media

## Ethernet Frame Format

> MAC addresses are added to each Ethernet frame’s header to uniquely identify the MAC address of the source and destination nodes. The frame is made up of three main elements: a pair of addresses, the data itself, and an error checking field. Uses a 2-byte type field that indicates the Network Layer protocol.

<img src="./images/ethernet-ieee-frame-formats.png" width="60%" height="60%">

## IEEE 802.3

> All of the IEEE 802 LAN standards define a 6-byte, or 48-bit, MAC address. This address uniquely identifies each NIC and is burned into the Ethernet chip on the board. Uses an 802.2 Logical Link Control (LLC) sublayer within the Data Link Layer that indicates service access points and Network Layer protocols

### IEEE 802.3 CSMA/CD (Carrier Sense Multiple Access with Collision Detection) Standards

> It defines a set of rules for when a PC or workstation can transmit and how to handle collisions. Behind CSMA/CD is a simple concept that only one device can transmit data at a given time or else a collision will occur. The CSMA/CD rules formalize how humans have talked with one another for thousands of years.

- Carrier sense — NIC cards listen on the physical media for specific voltage levels or carrier signals. If the coast is clear, the NIC can transmit on the network. If someone is already transmitting, you will know because the carrier sense voltage level is different.

-  Multiple access — NIC cards can transmit simultaneously. When this occurs, it causes a collision situation on the physical media. This collision is noticed by the NIC card transceivers that listen to specific voltage levels or carrier signals.

- Collision detection — NIC cards react to signal collisions by retransmitting after receiving notification that a collision occurred. Although this solution solves the problem caused by the initial collision, bus topologies with excessive collisions also can cause problems.

> Avoiding collisions and incorporating collision detection is the goal of CSMA/CD. Before any node can transmit, it first listens to the media for any current traffic. If the media is available to transmit, it has a specific voltage level that tells transceivers the coast is clear. If the media is not available to transmit, a carrier signal is detected by the transceiver, indicating that a transmission is occurring. If the transceiver doesn’t detect any current traffic, the node starts to
transmit.

<img src="./images/carrier-sense-with-multiple-access.png" width="60%" height="60%">

<img src="./images/collision-detection.png" width="60%" height="60%">

## The Internet Protocol (IP)

492

> The primary protocol that relays packets across most of today’s diverse networks is the Internet Protocol (IP). IP provides packet routing and host identification to deliver packets to their destinations. IP treats all packets, also called datagrams, separately. It doesn’t support the concept of grouping packets together. This means that each packet needs complete address information for the source and destination hosts. Each packet also needs sequence information to tell the destination how to reassemble the packets into the original message. Each packet contains a header with the destination’s IP address. This address identifies the destination host and provides information about the logical location of the host in its network. IP receives a packet from a higher layer, normally Layer 4, and adds additional data in a new IP header to the packet. The process of adding the IP header data is called encapsulation.

<img src="./images/ip-packet-header.png" width="60%" height="60%">

> Because IP networks are decentralized and dynamic, there is always the possibility that a packet won’t reach its destination node. Even if the packet does reach its destination, it may be different from the original packet. Errors or malicious software can change packets as they travel across a network. Source nodes need to know when a packet does not reach its destination, and destination nodes need to be able to detect when a packet changed during transmission. The earlier IP version, IPv4, includes a checksum for each packet header. The checksum is a numeric value that represents the entire header. The checksum changes when any part of a header changes. Checksums are an easy way for any network node to tell if a header has changed. Each node can calculate a new checksum for a packet header and compare it to the stored checksum. If the two checksums are different, the packet header has changed during transmission. Although this method helps nodes detect errors and changes, it takes time to process each header. The newer version of IP, IPv6, does not include checksums. This decision was made to increase IP’s performance and its ability to quickly route a packet with minimal overhead. Error detection and correction is left to upper layer protocols.

## IPv4 Classful Network

> The original addressing architecture used for the Internet used classful networks. This addressing architecture created five different types of networks based on their required number of nodes. Each class of network was restricted to a range of IP addresses; for example, Class A networks were all networks in which the leading digit on the IP address was 0. Class A networks could contain a large number of nodes. The tradeoff came in balancing the number of networks and the number of nodes. Because of the limited number of bytes in the IPv4 address, the classful network architecture limited the number of larger Class A and Class B networks.

<img src="./images/classful-network.PNG" width="60%" height="60%">

## IPv4 CIDR (Classless Inter-Domain Routing)

> CIDR uses Variable Length Subnet Masking (VLSM) to allow networks to be fragmented into any size subnetwork. CIDR defines networks using IP address prefixes. A CIDR block is a group of addresses that share the same prefix. Classful networks defined only a limited number of network prefixes based on network class. CIDR allows network prefixes of any length, up to the IPv4 address size. The CIDR address format is similar to the IP address dot notation. A network’s address is the base IP address in dot notation, followed by the number of bits in the prefix. The two values are separated by a slash character (/).

<img src="./images/cidr.png" width="60%" height="60%">

# IPv4 Subnet Mask

> Many networks define a subnet mask to serve the same purpose as the prefix length of CIDR address blocks. The subnet mask is a binary number that contains all 1’s in the leftmost prefix length positions. All other bits are 0’s. For example, the subnet mask for the CIDR address block 168.12.0.0/16 would be: 11111111.11111111.00000000.00000000. This subnet mask contains 16 ones and is followed by 16 zeros. The common notation for subnet masks is dot notation. So, this subnet mask would commonly appear as: 255.255.0.0. Networks use the subnet mask to help determine which network a given IP address belongs to by indicating which part of an IP address denotes the network and which part denotes the host. This helps make routing decisions to determine where to send a packet. The IP address 192.168.1.2 with a subnet mask of 255.255.255.0 can be seen as N.N.N.H (with N = network and H = host). All eight bits in each of the first three octets indicate the network and all eight bits in the last octet indicate the host. So, the IP address 192.168.1.2 belongs to network 192.168.1.0.

<img src="./images/subnet-mask.png" width="60%" height="60%">

## IPv4 Private Networks

> The IPv4 protocol defines three ranges of addresses for private networks. A private network is one that contains private IP addresses. Private IP addresses are not routable, which means public routers won’t forward any packets with private IP addresses. The purpose of private IP addresses is to allow organizations to assign their own device IP addresses from the private network ranges. This practice allows organizations to consume only a small number of public IP addresses.

- 10.0.0.0 to 10.255.255.255
- 172.16.0.0 to 172.31.255.255
- 192.168.0.0 to 192.168.255.255

## Resolving Addresses

> Thus far, you have seen only IP addresses used to identify hosts; however, it’s more common to use host names to identify hosts in many applications. Host names are more meaningful than IP addresses; for example, the host name amazon.com is more descriptive than one of Amazon’s IP addresses: 72.21.211.176. Because applications and users like to use host names, it’s important to look up the IP address for a given host name. The process of finding an IP address for a host name is called address resolution. The Domain Name System (DNS) is a hierarchical naming system that allows organizations to associate host names with IP address name spaces. DNS servers store these associations and make the tables available for network users. DNS servers return an IP address when given a known host name. DNS is a simple service, but it’s crucial to making the Internet a usable network. DNS servers keep up with the changing host names and make it easy to react to any organization that changes its IP addresses.

## Application Layer Ports And Sockets

582

> A port is just a number that tells the server what type of service the client is requesting. On the server, many programs run as services, which are programs that monitor a specific port for requests. When a server receives a request for a service at a specific port, the Application Layer passes the request to the service listening on the specified port. An instance of a service listening for traffic on a specific port is called a socket.

> Although there are hundreds of ports and corresponding applications, in practice, fewer than 100 are in common use. Of these, only a handful will be encountered on a regular basis. The most common of these are shown below.

<img src="./images/common-port-numbers.png" width="60%" height="60%">
<img src="./images/common-port-numbers2.png" width="60%" height="60%">

# Coding

## Pi4J to control a circuit involving a button and LED

    package com.mycompany.pi4jexamplecode;

    import com.pi4j.context.Context;
    import com.pi4j.platform.Platforms;
    import com.pi4j.provider.Providers;
    import com.pi4j.registry.Registry;
    import com.pi4j.util.Console;

    /**
     *
     * @author Carlton Davis
     */
    public class PrintInfo {

        //Method to print information about the detected and loaded platforms.
        public static void printLoadedPlatforms(Console console, Context pi4j) {
            Platforms platforms = pi4j.platforms();

            /* Print info about the detected and loaded platform that Pi4J
               detected when it was initialized.
            */
            console.box("Pi4J PLATFORMS");
            console.println();
            platforms.describe().print(System.out);
            console.println();
        }

        //Method to print information about the detected and loaded providers.
        public static void printProviders(Console console, Context pi4j) {
            Providers providers = pi4j.providers();


            /* Print info about the detected and loaded providers that Pi4J
               detected when it was initialized.
            */
            console.box("Pi4J PROVIDERS");
            console.println();
            providers.describe().print(System.out);
            console.println();
        }

        //Method to print info about the state of all I/O Pi4J manages.
        public static void printRegistry(Console console, Context pi4j) {
            Registry registry = pi4j.registry();

            /*Print info about the detected and loaded I/O interfaces
              registered with Pi4Jand included in the 'Registry'
            */
            console.box("Pi4J REGISTRY");
            console.println();
            registry.describe().print(System.out);
            console.println();
        }

    }

https://gitlab.com/crdavis/pi4jexamplecode/-/blob/main/src/main/java/com/mycompany/pi4jexamplecode/PrintInfo.java

    package com.mycompany.pi4jexamplecode;

    import com.pi4j.Pi4J;
    import com.pi4j.io.gpio.digital.DigitalInput;
    import com.pi4j.io.gpio.digital.DigitalOutput;
    import com.pi4j.io.gpio.digital.DigitalState;
    import com.pi4j.io.gpio.digital.PullResistance;
    import com.pi4j.util.Console;


    /**
     *
     * @author Carlton Davis
     */

    /* In this demo, a button is used as a controller to control
       a blinking LED. When the button is pressed 6 times
       the application ends.
    */
    public class PushButtonSwitchAndLED {

        private static final int PIN_BUTTON = 18; //Physical PIN 12
        private static final int PIN_LED = 17; // Physical PIN 11

        private static int pressCount = 0;

        public static void main(String[] args) throws Exception {

            //Create Pi4J console wrapper/helper
            final var console = new Console();


            //Initialize the Pi4J Runtime Context
            var pi4j = Pi4J.newAutoContext();

            //Print information about the initialized platforms and providers
            PrintInfo.printLoadedPlatforms(console, pi4j);

            //Configure the button
            var buttonConfig = DigitalInput.newConfigBuilder(pi4j)
                    .id("button")
                    .name("Press button")
                    .address(PIN_BUTTON)
                    .pull(PullResistance.PULL_DOWN)
                    .debounce(3000L)
                    .provider("pigpio-digital-input");
            var button = pi4j.create(buttonConfig);
            button.addListener(e -> {
                if (e.state() == DigitalState.LOW) {
                    pressCount++;
                    console.println("Button press count: " + pressCount);
                }
            });

            //Configure the LED
            var ledConfig = DigitalOutput.newConfigBuilder(pi4j)
                    .id("led")
                    .name("LED Flasher")
                    .address(PIN_LED)
                    .shutdown(DigitalState.LOW)
                    .initial(DigitalState.LOW)
                    .provider("pigpio-digital-output");
            var led = pi4j.create(ledConfig);


            //Print info about the state of all I/O Pi4J manages.
            PrintInfo.printRegistry(console, pi4j);

            while (pressCount < 6) {
                if (led.equals(DigitalState.HIGH)) {
                    console.println("LED low");
                    led.low();
                } else {
                    console.println("LED high");
                    led.high();
                }
                //Blinks faster as press count increases
                Thread.sleep(600 / (pressCount + 1));
            }

            // Shutdown Pi4J
            pi4j.shutdown();
        }
    }

https://gitlab.com/crdavis/pi4jexamplecode/-/blob/main/src/main/java/com/mycompany/pi4jexamplecode/PushButtonSwitchAndLED.java

## Java and JavaFX Threads

    private void updateScreen(String line) {

        //Use the split method to parse the String
        String[] values = line.split(",");
        if (values.length != 3) {
            LOGGER.log(Level.SEVERE, "Unexpected results in updateScreen()");
            throw new IllegalStateException();
        }

        //Thread to update the screen
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                var timeStamp = new Date();
                if (theChoice.equals(choice1)) {
                    textAreaX_axis.setText("\n\nAt: " + timeStamp + "\n" + values[0]);
                    textAreaY_axis.setText("\n\nAt: " + timeStamp + "\n" + values[1]);
                    textAreaZ_axis.setText("\n\nAt: " + timeStamp + "\n" + values[2]);
                } else {
                    textAreaX_axis.appendText("\n\nAt: " + timeStamp + "\n" + values[0]);
                    textAreaY_axis.appendText("\n\nAt: " + timeStamp + "\n" + values[1]);
                    textAreaZ_axis.appendText("\n\nAt: " + timeStamp + "\n" + values[2]);
                }
            }
        });
    }

    //Setup a thread to get output from the ProcessBuilder Process
    private void getOutputThread() {

        //Initialize the variable that indicates when the thread must stop
        exit = false;

        //Create an anonymous subclass of Thread
        Thread thread = new Thread() {
            @Override
            public void run() {
                BufferedReader input = new BufferedReader(new InputStreamReader(theProcess.getInputStream()));

                //String to store output
                String line = null;
                int lineCount = 0;
                try {
                    while ((line = input.readLine()) != null && exit != true) {

                        //Increment the line counter
                        lineCount += 1;

                        //Normalize the line of input
                        String theLine = Normalizer.normalize(line, Form.NFKC);
                        System.out.println("In getOutputThread(), theLine is: " + theLine);

                        /*Validate that the line of input only contain letters,
                          numbers, _, :, commas and white spaces.
                          NOTE: this pattern only occurs after the first 5 lines
                          of output
                         */
                        Pattern pattern = Pattern.compile("^[A-Za-z0-9_:,\\s+]*$");

                        /*Search for the regex pattern in the normalized
                          line of input.
                         */
                        Matcher matcher = pattern.matcher(theLine);

                        //If the regex pattern isn't found
                        if (lineCount > 5 && !(matcher.find())) {
                            LOGGER.log(Level.SEVERE, "Output from joystick in incorrect format");
                            throw new IllegalStateException();

                        } else {
                            if (lineCount > 5) {
                                updateScreen(theLine);
                            }
                        }
                    }

                    //If exit equals true, close the BufferedReader
                    if (exit == true) {
                        input.close();
                    }

                } catch (IOException ex) {
                    Logger.getLogger(FXScreen.class.getName()).log(Level.SEVERE, null, ex);

                }
            }
        };

        thread.start();
    }

https://gitlab.com/crdavis/joystickoutputtodashboard/-/blob/main/src/main/java/com/mycompany/joystickoutputtodashboard/FXScreen.java

## Secure Coding Practices

    package com.mycompany.securecoding_example_code;

    import de.mkammerer.argon2.Argon2;
    import de.mkammerer.argon2.Argon2Factory;
    import java.io.Console;
    import java.text.Normalizer;
    import java.text.Normalizer.Form;
    import java.util.regex.Matcher;
    import java.util.regex.Pattern;
    import java.util.logging.Level;
    import java.util.logging.Logger;
    import java.util.List;
    import java.util.Scanner;


    /**
     *
     * @author Carlton Davis
     */

    /* Class indicating code that are noncompliant and compliant to good
       secure coding practices
    */
    public class SecureCodingEx {

        //Setup a Logger for the class
        private static final Logger LOGGER = Logger.getLogger(SecureCodingEx.class.getName());


        //This method is non-compliant to secure coding practices
        private void nonCompliantEx(String str) {

            System.out.println("This is the string BEFORE normalization: " + str);

            /*Validation involves checking for angular brackets,
              assume security policy doesn't allow angular brackets as input
            */
            Pattern pattern = Pattern.compile("[<>]");

            //Search for occurance "<" or "<" within the string str
            Matcher matcher = pattern.matcher(str);

            if (matcher.find()) {         
                // Found black listed tag
                LOGGER.log(Level.SEVERE, "Black listed character found in input");
                throw new IllegalStateException();

            } else {
                System.out.println("Input string is acceptable");
            }

            /*  \uFE64 is normalized to < and \uFE65 is normalized to > using
                the NFKC normalization form
            */
            str = Normalizer.normalize(str, Form.NFKC);
            System.out.println("This is the string AFTER normalization: " + str);   
        }


        //This method is compliant to secure coding practices
        private void compliantEx(String str) {

            System.out.println("This is the string BEFORE normalization: " + str);

             /*  \uFE64 is normalized to < and \uFE65 is normalized to > using
                the NFKC normalization form
            */
            str = Normalizer.normalize(str, Form.NFKC);
            System.out.println("This is the string AFTER normalization: " + str);

            /*Validation involves checking for angular brackets,
              assume security policy doesn't allow angular brackets as input
            */
            Pattern pattern = Pattern.compile("[<>]");

            //Search for occurance "<" or "<" within the string str
            Matcher matcher = pattern.matcher(str);

            if (matcher.find()) {         
                // Found black listed tag
                LOGGER.log(Level.SEVERE, "Black listed character found in input!!");

            } else {
                System.out.println("Input string is acceptable");
            }  
        }


        //This method accepts only alphanumeric characters
        private void alphanumericInput(String str){

           //Sanitized string
           String sanitizedStr = "";

           System.out.println("String BEFORE normalization: " + str);

           //Normalize the string using the NFKC normalization form
           str = Normalizer.normalize(str, Form.NFKC);
           System.out.println("String AFTER normalization: " + str);

           if (Pattern.matches("[a-zA-Z0-9]", str)) {
               sanitizedStr = str;
               System.out.println("Acceptable input");

           } else {
               LOGGER.log(Level.SEVERE, "Unacceptable input!!");
           }

        }


        /* Non-compliant method for obtaining username and password from user.
           Return 0 if all is well, and 1 otherwise
        */
        private int getCredentialsNC() {

          int returnValue = 0;

          Console console = System.console();
          if (console == null) {
              System.out.println("No console available");
              returnValue = 1;

          } else { //Get username
              String userName = console.readLine("Enter username: ");
              System.out.println("Username entered: " + userName);

              //Non-Compliant way to get password
              String passwd = console.readLine("Enter password: ");
              System.out.println("Password entered: " + passwd);
          }

          return returnValue;
        }


        /* Method for obtaining username and password, which conforms to good
           secure coding practices.
        */
        private String getCredentialsC() {

          String returnStr = "";

          Console console = System.console();
          if (console == null) {
              System.out.println("No console available");

          } else { //Get username
              String userName = console.readLine("Enter username: ");
              System.out.println("Username entered: " + userName);

              // Create instance of Argon2 to hash user password
              Argon2 argon2 = Argon2Factory.create();

              //Compliant way to get password from user
              char[] passwd = console.readPassword("Enter password: ");

              try {
                   /*Use Argon2 library to generate hash of the password.
                    See https://github.com/phxql/argon2-jvm  
                   */
                   returnStr = argon2.hash(10, 65536, 1, passwd);

              } finally {
                  // Wipe confidential data
                   argon2.wipeArray(passwd);
              }
          }
          return returnStr;
        }


        //This method doesn't do input validation: evidently problematic
        private void noInputValidation() {
            //List for storing the animals
            List<String> animals = List.of("Lion", "Tiger", "Leopard", "cheetah");

            System.out.print("\nEnter a number to select one of the animals: ");
            Scanner theScanner = new Scanner(System.in);
            int choice = theScanner.nextInt();

            String animal = animals.get(choice);
            System.out.println("Choice: " + choice + " is " + animal);   
        }

        //This method does input validation
        private void doesInputValidation() {
            //List for storing the animals
            List<String> animals = List.of("Lion", "Tiger", "Leopard", "cheetah");

            boolean flag;
            String choice;

            Scanner theScanner = new Scanner(System.in);

            do {
                System.out.print("\n[In doesInputValidation] Enter a number to select one of the animals: ");
                choice = theScanner.next();
                if(!isValid(choice)) {
                    System.out.println("Input must be a digit between 0 and 3, inclusively!");
                }
            } while(!isValid(choice));

            String animal = animals.get(Integer.parseInt(choice));
            System.out.println("Choice: " + choice + " is " + animal);
        }


        //Determine if an input is valid
        private boolean isValid(String input) {
            int num;

            try {
                  //If input is not numeric, NumberFormatException will be thrown
                  num = Integer.parseInt(input);
                } catch (NumberFormatException nfe) {
                    return false;
                }


            if (!(num > -1) || !(num < 4)) {
                return false;
            }

            return true;
        }

        //Driver code
        public static void main(String[] args) {

          SecureCodingEx myObject = new  SecureCodingEx();

          String str = "\uFE64" + "script" + "\uFE65";

          System.out.println("\nResult for NON-COMPLIANT method:");
          myObject.nonCompliantEx(str);

          System.out.println("\nResult for COMPLIANT method:");
          myObject.compliantEx(str);

          String str2 = "Input with spaces";
          System.out.println("\nResult for alphanumericInput method:");
          myObject.alphanumericInput(str2);

          System.out.println("\nNON-COMPLIANT method for getting user credential");
            int returnValue = myObject.getCredentialsNC();

          System.out.println("\nCOMPLIANT method for getting user credential");
          String passwdHash =  myObject.getCredentialsC();
          System.out.println("Password hash: " + passwdHash);

          myObject.noInputValidation();
          myObject.doesInputValidation();
        }
    }

https://gitlab.com/crdavis/securecoding_example_code/-/blob/master/src/main/java/com/mycompany/securecoding_example_code/SecureCodingEx.java

## JUnit

    package com.mycompany.tilesfx_example_code;

    import org.junit.jupiter.api.Test;
    import static org.junit.jupiter.api.Assertions.*;

    /**
     *
     * @author Carlton Davis
     */

    /* Test class for ProcessBuilderEx class */
    public class ProcessBuilderExTest {

        public ProcessBuilderEx processInstance;

        //Constructor for the test class
        public ProcessBuilderExTest() {
            String pathAndFile = "src/main/Python/helloWorld.py";
            processInstance = new ProcessBuilderEx(pathAndFile);
        }

        /**
         * Test of startProcess method, of class ProcessBuilderEx.
         * @throws java.lang.Exception
         */
        @Test
        public void testStartProcess() throws Exception {
            System.out.println("startProcess");

            //Determine whether the process input stream is null
            String expResult = processInstance.startProcess();
            assertNotNull(expResult);

            //This assertion will only be executed if the previous assertion is valid
            assertTrue(expResult.contains("Hello World"));
        }

    }

https://gitlab.com/crdavis/tilesfx_example_code/-/blob/master/src/test/java/com/mycompany/tilesfx_example_code/ProcessBuilderExTest.java

    package com.mycompany.joystickoutputtodashboard;

    import org.junit.jupiter.api.Test;
    import static org.junit.jupiter.api.Assertions.*;
    import org.junit.jupiter.api.BeforeEach;

    /**
     *
     * @author Carlton Davis
     */
    public class ProcessBuilderJoystickTest {
        String theCmd;
        ProcessBuilderJoystick instance;

        @BeforeEach
        void setUp() {
            this.theCmd = "src/main/Cpp/Joystick";
            this.instance = new ProcessBuilderJoystick(theCmd);
        }


        /**
         * Test of getTheProcess method, of class ProcessBuilderJoystick.
         */
        @Test
        void testGetTheProcess() throws Exception {
            System.out.println("getTheProcess");
            Process result = this.instance.getTheProcess();
            assertNotNull(result);
        }

    }

https://gitlab.com/crdavis/joystickoutputtodashboard/-/blob/main/src/test/java/com/mycompany/joystickoutputtodashboard/ProcessBuilderJoystickTest.java
