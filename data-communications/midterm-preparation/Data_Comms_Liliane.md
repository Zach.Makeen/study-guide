## **420-540-DW**


## <span style="text-decoration:underline;">DATA COMMUNICATIONS AND NETWORKING</span>




## Table of Contents


[TOC]





## Lecture 1-1: {#lecture-1-1}


### Hyperconnectivity: {#hyperconnectivity}



* Advantages:
    * Few transactions require a physical presence.
    * Ability to have both personal and business activities remotely.
* Drawbacks:
    * People check their smartphones.
    * Available at all times, expectation that everyone is online/connected.
    * Difficulty to put the phone away and focus on face-to-face conversation.


### Evolution from Analog -> Digital -> Internet and IP {#evolution-from-analog->-digital->-internet-and-ip}



* Analog:
    * Telegraph: 
        * Morse code communication device to send messages. It needs several people to decode and give the message to the person.
    * Telephone
    * Answering machine
    * Fax Machine
    * Landline phone
* Digital:
    * Computer
    * Smartphone
    * Internet
    * Broadband/Wireless


### Evolution of communication {#evolution-of-communication}



* Snail Mail:
    * Letters
* Store and Forward Messages:
    * Emails
* Real-Time Communication:
    * Face-to-face meetings
    * Telephone
    * Instant messaging
    * Text messages
* Social Media
    * Social networking


### The Impact of Web on Businesses {#the-impact-of-web-on-businesses}



* Brick-and-mortar stores have global reach by extending business to the Internet
* E-commerce:
    * Allows global customers
    * Changed the organizations every out business transactions
* The web changed the marketing of goods


### The Impact of Internet of Things on Businesses {#the-impact-of-internet-of-things-on-businesses}



* Possibility to track everything
* Data grows even faster
* Administrative delays will shrink
* Waste is easier to identify
* Identity and authorization management will become more complex
* Some jobs/roles may change/go away


### The Impact of Cloud on Business {#the-impact-of-cloud-on-business}



* Cloud computing
    * Renting someone else’s computers and network to run software to support your business
* Cloud service provider (CSP)
    * Businesses that offer cloud computing services. (Ex: Microsoft’s Online Work, etc)
* Virtualization:
    * VMs
    * Dynamic provisioning
    * Hypervisors
    * Snapshot
* Interface as a Service (IaaS):
    * Provision & launch virtual machines
* Platform as a Service (PaaS):
    * Develop applications
* Software as a Service (SaaS):
    * Rent access to specific software applications
* Anything as a Service (XaaS):
    * Extends SaaS solutions as professional services
    * Ex: Security as a Service, Database as a Service, Blockchain as a Service, Privacy as a Service


### Summary of Topics: {#summary-of-topics}



* The hyperconnectivity
* Internet and IP connectivity
* Evolution of Communication 
* Impact of the web, Internet of Things, Cloud Computing on Businesses


## Lecture 1-2:


### Raspberry Pi 4B: {#raspberry-pi-4b}



### Components: {#components}



* 40 Pin General-Purpose Input/Output (GPIO) Header
    * Interface for connecting components (LED, resistors, breadboard, etc) to Raspberry Pi
    * Each of these 40 pins have <span style="text-decoration:underline;">specific </span>functions (It is going to break the RPi if it is not connected to the right pins)
    * Can be used to either input or output depending on the program
    * 3 ways to identify pins:
        * GPIO numbering (not in order): assigned by Broadcom
        * Physical numbering (in order): counting across and down from pin 1
        * WiringPi GPIO numbering: Pin number reference used by Wiring Pi code
        * https://pinout.xyz 
            * The shown numbers are GPIO, if hovered it is WiringPi.
        * The **<span style="text-decoration:underline;">1 of the Physical numbering</span>** must be on the **<span style="text-decoration:underline;">LEFT</span>**
* Serial Peripheral Interface (SPI)
    * Synchronous serial bus commonly used to send data between microcontrollers and small external peripheral devices. 
    * Multiple peripheral devices can be interfaced as SPI slaves and be controlled with the SPI master.
    * SPI uses a separate clock (SCK) & data lines (MISO, MOSI) along with a select line to choose among slave devices.
        * Serial communication data sent one bit at a time
        * Clocking indicate the start & end of the serial communication
        * SS : selecting
        * RPI supports 2 chip select (CE) lines to interface with 2 SPI slave devices: GPIO 8 and 7.



* 
Inter-Integrated Circuit (I<sup>2</sup>C)


    * Interface that allows multiple circuits to communicate with 1 and more master
    * Uses 2 directional lines:
        * Serial Data (SDA) Line
        * Serial Clock Line (SCL)
    * SPI = 4 lines to connect vs I<sup>2</sup>C = 2 lines


## Lecture 2-1 to 3-2: {#lecture-2-1-to-3-2}


### Pi4J: {#pi4j}



* Java library to interface with RPi
* Uses pigpio as the underlying framework



    * 
Pigpio : allows control if GPIO (General purpose Input/Output)


    * 
Pi4J refers to pigpio as a provider


* Pi4J uses SLF4J (Simple Logging Facade for Java) for logging
* Steps to get started with Pi4J

    * Initialization: 


        * Context is an immutable runtime obj that holds the configured state & manages the lifecycle of a Pi4J instance
        * “newAutoContext” method which is a member of the ‘Pi4J’ static class can be used to create automatically, accepting all default context configs.


```
var pi4j = Pi4J.newAutoContext();

```



* Output Pi4J Context information


    * Pi4J library contains helper functions to output info about available platforms and providers


```
Platforms platforms = pi4j.platforms();
platforms.describe();

```



   * Handle the button presses


        * Handles digital input events config must first be made; Pi4J can then create the obj and state changes can be handled
        * Working with Broadcom numbers
        * Debounce -> disables click, L = Long



* Toggle a LED

    * Approach is similar to the button, it must be config, Pi4J can then create the obj and state changes can be handled
    * Digital Output receives output (receiver)
    * PIN_LED = 22
    * Keyword “HIGH” will open it


```
// Handle button presses
var buttonConfig = DigitalInput.newConfigBuilder(pi4j);
    .id("button")
    .name("Press button")
    .address(PIN_BUTTON)
    .pull(PullResistance.PULL_DOWN)
    .debounce(3000L)
    .provider("pigpio-digital-input")

// Create the button obj
var btn = pi4j.create(buttonConfig);

// Setup event handler for the button to listen for state change
button.addListener( e-> {
// Toggle a LED
var ledConfig = DigitalOutput.newConfigBuilder(pi4j)
    .id("led")
    .name("LED Flasher")
    .address(PIN_LED)
    .shutdown(DigitalState.LOW)
    .initial(DigitalState.LOW)
    .provider("pigpio-digital-output");

//Create the LED object
var led = pi4j.create(ledConfig);

while(pressCount<6){
    if (led.equals(DigitalState.HIGH){
        led.low();
    } else {
        led.high();
    }
    Thread.sleep(500 / (pressCount+1));
}
```



### JavaFX {#javafx}



* Stage: JavaFX representation of native OS window
* Scene: a container for JavaFX scene graph
* All elements in JavaFX scene graph are represented as Node
* 3 types of JavaFX nodes:



    * Root:


        * Only node that doesn’t have a parent and is directly contained by a scene



    * Branch:


        * Has one/more child/children



    * Leaf


        * Has no children


### Java and JavaFX threads {#java-and-javafx-threads}



* Java:



    * Similar to a virtual CPU that can execute designated code within a Java app


    * When it starts, the main method is executed by the main thread


        * It is created by the Java VM to run your app



    * More thread can be created and started within your app 

```
Thread thread = new Thread();
thread.start();

public class MyThread extends Thread{
    public void run(){
    System.out.println("MyThread is running");
    }
}

Mythread mythread = new MyThread();
myThread.start();

```



* start()
    * Call with return as soon as the thread get started:
        * It will not wait until the run() method completes executing
    * The second way of specifying the code that a thread should execute is by creating a class that implements the java.lang.runnable interface
        * Java obj that implements the Runnable interface can be executed by a thread
        * The runnable interface has a single method run()
        * 3 ways to implement the Runnable interface:


```
// Method 1
public class MyRunnable implements the Runnable{
    public void run(){
        System.out.println("MyRunnable is running");
    }
}
// Method 2 (Anonymous function)
Runnable MyRunnable = new Runnable(){
    public void run(){
        System.out.println("Runnable running");
    }
}

// Method 3 (Create a Java Lambda)
Runnable runnable = () -> {System.out.println("Lambda Runnable is running");};

```



* Runnable interface
    * Have the run() method executed by a thread, it must pass an instance of a class, anonymous class or lamba that implements the Runnable interface to a Thread in its constructor
    * When it starts, it calls the run() method of the Myrunnable instance instead of executing its own run() method
    * [Creating and Starting Java Threads](http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html) 
    * It is better to separate the threads if there is several tasks (because it can cause issues)
    * Advantages:
        * Preference mostly
        * No clear cut advantages
        * 2 ways of doing threading


```
Runnable runnable = new MyRunnable();
Thread thread = new Thread(runnable);
thread.start();

```



* JavaFX threading 



    * Only JavaFX can render anything on the screen


    * Task threads cannot update JavaFX scene graph directly


    * Provides a Platform class containing a runLater() method


        * Takes a runnable which is executed by JavaFX app thread when it has time
        * Scene graph can be modified within this runnable
* Java ProcessBuilder Class



    * Used to create OS processes


    * A program file can be executed with the ProcessBuilder command() method


    * The process for the command is launched with start()


    * getInputStream() method of the process can be used to get the input stream from the standard output of the process


* Java.net.HttpURLConnection


    * Commonly used for interacting with web servers


        * Constructor
            * Protected HttpURLConnection(URL u)
        * Methods
            * String getRequestMethod() : Returns request method
            * Int getResponseMessage() : Gets the status code from an HTTP response message

                <table>
                <tr>
                <td colspan="2" >
                    Response status
                    </td>
                    </tr>
                    <tr>
                    <td>1XX
                    </td>
                    <td>Information
                    </td>
                    </tr>
                    <tr>
                    <td>2XX
                    </td>
                    <td>Success
                    </td>
                    </tr>
                    <tr>
                    <td>3XX
                    </td>
                    <td>Redirection
                    </td>
                    </tr>
                    <tr>
                    <td>4XX
                    </td>
                    <td>Client error
                    </td>
                    </tr>
                    <tr>
                    <td>5XX
                    </td>
                    <td>Server error
                    </td>
                    </tr>
                    </table>



### Secure Coding Practices {#secure-coding-practices}



* [Carlton’s example code](https://gitlab.com/crdavis/securecoding_example_code) (GitLab repo)
* Input validation 



    * Validation of user or other external input to a program is a key tenet of secure coding


* String validation



    * String inputs should be normalized then validated


    * Normalization is important because in Unicode, the same string can have different representations


* Data type of input



    * If an invalid data type or a number that is out of range is passed to an array


* Protect confidential information



    * Person data such as login credentials and other important personal information should not be stored in memory or sent to log files


    * When passwords need to inputted by user, Console.readPassword() method should be used rather than Console.readLine() method


* Avoid using serialization and deserialization



    * Serialization involved encoding objs in streams of bytes


    * Deserialization involves reconstruction of obj graph from a stream


* Java Security Manage



    * Allows user to run untrusted bytecode in a sandbox environment


    * Untrusted code not allow to read/write files on the local system


* Dependencies analysis



    * Sources of vulnerabilities are libraries


    * It is coding practice to scan libraries used in codes to verify vulnerabilities


    * [OWASP Dependency-Check](https://jeremylong.github.io/DependencyCheck/dependency-check-maven ) is good for analyzing 

### JUnit Testing {#junit-testing}



* JUnit Review



    * Unit tests are small programs that tests code


    * It is typically test for one functionality or partially


    * It is good practice to write numerous unit tests to ensure that it handles the code correctly 


* Test classes must have a single constructor and at least one test method that is not abstract
* Test methods must not be abstracted, not return a value and be annotated with the following:



    * @test


    * @RepeatedTest


    * @ParameterizedTest


    * @TestFactory


    * @TestTemplate


* Lifecycle methods are methods annotated with the following:



    * @BeforeAll


    * @AfterAll


    * @BeforeEach


    * @AfterEach


    * Assertions:
        * assertEquals
        * assertTrue
        * assertNotNull
        * assertTimeout


## 


## Lecture 4-1: {#lecture-4-1}



* Open Systems Interconnection Reference Model



    * Defines & abstracts network communications


    * Logical layers based on communication function


    * Hosts interact on same layer



<table>
  <tr>
   <td>OSI Reference Model Layer
   </td>
   <td>Common Protocols and Applications
   </td>
   <td>Common Devices
   </td>
  </tr>
  <tr>
   <td>Application
   </td>
   <td>BitTorrent, DHCP, HTTP(S)
   </td>
   <td>Gateway, Firewall, Endpoint Device
   </td>
  </tr>
  <tr>
   <td>Presentation
   </td>
   <td>SSL, TLS
   </td>
   <td>Gateway, firewall, server, PC
   </td>
  </tr>
  <tr>
   <td>Session
   </td>
   <td>SSH, RPC
   </td>
   <td>Gateway, firewall, server, PC
   </td>
  </tr>
  <tr>
   <td>Transport
   </td>
   <td>TCP, UDP
   </td>
   <td>Gateway, firewall
   </td>
  </tr>
  <tr>
   <td>Network
   </td>
   <td>IPv4, IPv6, ICMP
   </td>
   <td>Router, Layer 3 switch
   </td>
  </tr>
  <tr>
   <td>Data Link
   </td>
   <td>ARP, Ethernet (IEEE 802.3), VLAN, MAC
   </td>
   <td>Bridge, modem, Layer 2 switch
   </td>
  </tr>
  <tr>
   <td>Physical
   </td>
   <td>Bluetooth, USB, Wi-Fi
   </td>
   <td>Hub, repeater, cable, fiber, wireless
   </td>
  </tr>
</table>




* TCP/IP Reference Model



    * Represents the same functionality as the OSI model, several layers are combined


    * Maps well to the TCP/IP protocol suite


    * Describes how the Internet Protocol suite operates

<table>
  <tr>
   <td colspan="2" >TCP/IP Reference Model Layers
   </td>
  </tr>
  <tr>
   <td>Application Layer
   </td>
   <td>Interacts with application that need to gain access to network services
   </td>
  </tr>
  <tr>
   <td>Transport Layer
   </td>
   <td>Segments the data and adds a checksum to properly validate data to ensure that it has not been corrupted
   </td>
  </tr>
  <tr>
   <td>Internet Layer
   </td>
   <td>Handles the routing of packets as they move around the network
   </td>
  </tr>
  <tr>
   <td>Network Access Layer
   </td>
   <td>Point at which the higher layer protocols interface with the network transport media
   </td>
  </tr>
</table>




* TCP/IP Suite



    * Provides all the protocol necessary for applications to exchange messages and data using IP-based networks


    * Supports communication across networks ranging from small LANs to the Internet


    * TCP/IP family of protocols facilitate voice, video, data transfer , and Internet communications between devices located anywhere in the world


    * Nearly all Internet-capable operating systems and devices provide support for the protocols defined in the TCP/IP suite


* Type of Switched Networks



    * Circuit-switched network


        * Two devices use the same path, circuit, throughout a conversation
        * Ex: Plain old telephone service


    * Packet-switched network


        * Separates messages into smaller, manageable-sized chunks called packets
        * Software apps may choose to use different packet size even when using the same protocol



* IP-Based Communications



    * Applications are designed and built knowing that TCP/IP is the transport protocol of choice


    * Standard is so prevalent that network presence is often expressed as an IP address


    * IP address identifies a device or computer to a network as a unique node


    * Private IP address identify nodes within an organization


    * Applications that want to communicate over networks only need to reference the IP address of the destination node

### Network Topology Overview {#network-topology-overview}



* Network topology



    * The layout of how devices connect to a network


    * A map of the network that shows how devices connect to one another and how they use medium to communication


* A node (server, computer, smartphone, etc.) can physically connect to the network and has an assigned IP host address
* Physical Topology



    * The picture of the actual network devices and the medium the devices use to connect to the network



* Logical Topology



    * The actual network works and how you transfer data in a network; view focuses more on how the network topology operates


### Common Network Topologies {#common-network-topologies}



    * Point-to-Point Networks


        * Networks that consist of computers or devices that connect directly one another
        * Difficult to add many devices because each new device requires a direct connection
        * Found in very small environments that only need to connect a few PCs
        * Are considered archaic



    * Bus Topology


        * Coaxial cable (bus) runs throughout a physical space; network devices attach to bus using an Ethernet transceiver
        * Supports high-speed network communications
        * Is simple to construct
        * Is subject to physical distance limitations and maximum number of devices
        * When two devices transmit at the same time, a collision occurs on the network
        * Both devices must retransmit when the network is available



    * Ring Topology


        * All stations are connected in a logical ring
        * Attached devices need permission to transmit on the network
        * Permission is granted via a token (small frame) that circulates around the ring
        * Unidirectional manner eliminates network transmission collisions
        * A cut in the cable will break a ring network



    * Star Topology


        * Star-wiring cabling from a centrally located wiring closet
        * Network-attached devices physically and electrically connect to network
        * Fixes path length and attenuation issues



    * Mesh Topology


        * A network layout in which all nodes are directly connected to all other nodes



    * Hybrid Topology


        * A network that contains several different topologies
        * Can provide fault tolerance for some nodes while providing flexibility for other parts of the network



### Internetworking {#internetworking}



* It is a term used to describe connected LANs together
* LANs communicate using protocols and the OSI model
* Different network devices interoperate at different layers of the protocol stack
* A protocol stack is how software operates at different layers of the OSI model 
* Internetworking LANs must follow the OSI model protocol stack definition
* Network devices must operate at either the Data Link Layer/Network Layer
* Client/Server architecture



    * Users interact with enterprise applications via local software running on devices that use a network to connect to, and request services from, one or more servers


    * Depends on the client/server internetworking


* Peer-to-peer internetworking



    * The use of an enterprise network for peer to exchange messages without depending on a central server to manage connections or message handling


* Intranet



    * An internal network that only employees can access


    * A private network within that organization’s IP network infrastructure


* Extranet



    * A remotely accessible network that an organization makes accessible to its business partners and suppliers through the public Internet


* Bridges



    * Internetworking at the Data Link Layer requires a bridge


    * Easiest way to interconnect two networks is to place a MAC layer bridge in between the two networks


        * Examines the MAC address of each frame received
        * Makes a filtering or forwarding decision based on the MAC address forwarding table
* Routers



    * Operates at the Network Layer


    * The same thing as a Layer 3 switch, but usually used for WAN circuit connections, campus backbone connections, and building backbone connections


    * Can make intelligent decisions on where to send packets


        * Instead of just reading the MAC address and forwarding a packet based on a forwarding table, can see the packet’s Network Layer address or IP address
        * Network Layer address contains information about the destination network number and host number
        * Performs a path determination calculation to find the best path for the IP packets to traverse



    * Typically has redundant processors and plenty of memory


    * Takes longer to examine a packet than for a Layer 2 switch or bridge


* Gateway



    * Interconnects two networks that use different protocols


    * Translates network packets from one network protocol to another


    * Main job is to translate all incoming packets to a protocol compatible with the destination network


    * Is commonly placed at entry and exit points of a network


    * Commonly runs either as a software on a comp or as a device that performs the same functions as a router


* Switches



    * Is essentially a bridge with more than two ports


    * Often has the capability to connect many devices and networks


    * Operates an OSI Layer 2 devices


    * Uses MAC addresses and builds address tables of devices connected to each physical port


    * Switches that connect multiple devices/networks keep track of Media Access Control (MAC) addresses on all ports


    * When the switch sees an Ethernet frame destined for a specific MAC address, it forwards that frame to that port


    * Layer 2 switches


        * Operate at the Data Link Layer
        * Examine the MAC layer address of Ethernet frames



    * Layer 3 switches


        * Operate at either the Data Link Layer or Network Layer
        * Typically have software that lets them function like Layer 2 or multiport bridges
        * Usually operate at the Network Layer that examines the network layer address within the Ethernet frame
        * Can look up the destination IP network number in their IP routing tables and then make a path determination decision



    * Functionality


        * Layer 2 or 3 workstation connectivity
        * MAC addressing tables
        * Forwarding/filtering
        * Preventing broadcast storms caused by loops
        * Power over Ethernet (PoE) switch ports
        * Virtual LANs (VLANs)
        * Layer 2 or 3 switch resiliency 



    * Forwarding Methods


        * Cut-through switching
            * As LAN switch reads the destination MAC address, it starts to forward the packet to the destination and does not examine the entire frame
            * Increases network throughout and performance
        * Store-and-forward switching
            * LAN switches must receive the entire Ethernet frame and make forwarding decision based on MAC address table
            * Device can drop any packets with errors prior to forwarding them
            * Uses less of a network’s capacity but increases latency
* Routing Concepts



    * Network layer address


    * Static Route


    * Dynamic route


* Resiliency and Redundancy



    * Layer 2 Resiliency


        * Enabling resiliency protocols, such as Spanning Tree Protocol (STP) and Resilient Ethernet Protocol (REP), to avoid network (REP) to avoid network communication interruptions



    * Layer 3 Resiliency 


        * Supporting alternate routes to a WAN when the primary route is unavailable. Cisco Hot Standby Routing Protocol (HSRP)



    * Virtual Networking Components


        * Implementing some network devices as VM or containers, or building entire virtualized networks



    * Network Storage Types


        * Implementing network storage devices as VMs; storage area networks and network attached storages


## Lecture 4-2: {#lecture-4-2}


### Circuit Switching VS Packet Switching {#circuit-switching-vs-packet-switching}



* Types of Switched Networks:
    * Circuit:
        * Two devices use the same path, or circuit, throughout a convo
    * Packet:
        * Separates messages into smaller, manageable-sized chunks called packets
    * Router decides the path of the packets to the destination
    * IP Based Communications:
        * TCP:
            * Guarantees the delivery of a **reliable **stream of data (No packet loss , each receiver has to acknowledge the receive, if it does not receive it it will resend)
            * One of the most popular protocols that organizations use to communicate on the Internet
            * Operates at OSI Layer 4
            * TCP/IP Layer 3
        * IP
            * Makes it possible to deliver packets across a complex network to a destination
            * Handle the routing decisions necessary to get packets from their source to the destination
            * OSI Layer 3
            * TCP/IP Layer 2
        * Applications are designed and built knowing that TCP/IP is the transport protocol of choice
        * Standard is so prevalent that network presence is often expressed as an IP address
    * Network Topology
        * The layout of how device connect to a network
        * Map of the network that shows how devices connect to one another and how they use a connection medium to communicate
        * A node (server, computer, smartphone, etc.) can physically connect to the network and has an assigned IP host address
        * Physical topo:
            * Wireless router but more like a switch (Works on Layer 2)
        * Point to point network
            * Network that consist of computers or devices that** connect directly to one another**
            * Difficult to add many devices because each new device requires a direct connection
            * Found in very small environments that only need to connect a few pcs
            * Are considered ..
        * Bus topology
            * Coaxial cable runs throughout a physical space; network devices attach to bus using an Ethernet transceiver
            * Supports high-speed network communications 
            * Simple to construct
            * Subject to physical distance limitations and maximum number of devices
            * Only one device can communicate at a given time
            * Whole network crashes down if one server goes down 
            * Problematic
            * When two devices transmit at the same time, a collision occurs on the network
            * Both devices must retransmit when the network is available
            * Led to creation of IEEE 
        * Ring topology
            * All stations are connected in a logical ring
            * Attached devices need permission to transmit on the network
            * Permission is granted via a token that circulars around the ring
            * Unidirectional manner eliminates network transmission collisions
            * A cut in the cable will break a ring network
        * Star topology
            * Star wiring cabling from a centrally located wiring closet creates Ethernet or token ring network
            * Wiring creates a physical star logical bus
            * Can be supported by a hub/switch typically 
        * Mesh topology
            * A network layout in which all nodes are directly connected to all other nodes
    * Internetworking
        * It is a term used to describe connecting LANs together
        * LAN communicate using protocols and the OSI model
        * Different network devices interoperate
    * Data layer/network layer (on test)
    * Internetworking Terms
        * Client/Server architecture
            * User interact with enterprise app via local software running on client devices that use a network to connect to and request services from one or more servers
            * Depends on client/server internetworking
        * Peer-to-peer internetworking
            * The use of an enterprise network for peers to exchange messages without depending on a central server to manage connections or message handling
        * Intranet
            * Internal network that only employee can access
            * Private network within that organization IP network infra
        * Extranet
            * Remotely accessible network that an organization makes accessible to its business partners and suppliers through the public Internet
        * Internetworking with Bridges
            * 