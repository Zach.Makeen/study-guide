# Revision
#DataComm

## Cloud services
- IaaS (Infrastructure as a Service) : Supply users with VMs as a service
- PaaS (Platform as a Service): Along with VMs, supplies preinstalled software as a platform.
- SaaS (Software as a Service): Rents out software. Provider handles the installation and maintenance of software. Users just use.
- XaaS (Anything as a Service):	Extends SaaS solutions as a professional service and includes many other. Database,Security, etc.

*Pages : 142 -143*

## Analog vs digital
Analog is a continuous variable signal that varies in intensity in function to time,
Where digital is two distinct signal values. 1 or 0.

*Pages: 1506 (Digital) & 1495 (Analog)*

## GPIO (General Purpose Input/Output):	
Interface for connecting external components to PI using pin connectors.
Each pin has its own specific function.

-  Reference GPIO: 	1.	GPIO numbering (BroadCom, Cpu manufacturer)
					2.	Physical numbering 
					3.	WiringPi number


## SPI (Serial Peripheral Interface): 
Synchronous serial bus used for sending data between micro controllers and peripheral devices

Can have multiple slaves and one controller.

	- SPI uses: 	1.    SCK(serial clock): Controls when data is changed/read
			 2.	MOSI (Master output Slave input)
			 3.	MISO(Master input Slave output)
			 4.	SS (Slave Select)

## I2C(Inter-Integrated Circuit):
Allows for Slave circuits to communicate with one or more master.
	
- I2C Uses: 
		1. SDA(Serial Data Line): for data transmission
		2. SCL(Serial Clock Line): Used to synchronize all data transfers

## SPI vs I2C
I2C needs half the pins that SPI needs for communication. 2 -> 4
SPI can only have one master while I2C can have multiple
SPI is full duplex, simultaneous two way data transfer

## Resistor 
An electrical component that regulates the flow of current within a circuit
- Bands of color on the resistor are used to determine the resistor value
- Non polar (Current passes the same no matter the orientation)
- Unit of Measure:	Ohms (Ω)

## Signals
**NOISE**:  Unwanted electromagnetic/electrical energy in the signal, degrades signal quality.

### Analog
A continuous signal/waveform.
Difficult to extract/remove **noise** from the signal.

### Digital
Non-continuous waveform/signal.
Can only have two forms, 1 or 0.
If too much **noise**, voltage can be hard to read (On or Off?).

### Signal Fundamentals
**Amplitude**:	Height of the wave
	- Unit of measure: Volts
**Frequency**:  Cycles per given time frame
	- Unit of measure: Hertz or cycles/second
	- *Spectrum*: Range of frequencies that a signal can span.
**Phase**: Position of waveform at a specific time.
**Bandwidth**: Difference of spectrum
	- Ex: Human voice: 300 - 3100 Hz
		- Bandwidth = 2800 Hz
		- Spectrum = 300 - 3100 Hz

## ADC Module
Converts analog voltage to digital.






