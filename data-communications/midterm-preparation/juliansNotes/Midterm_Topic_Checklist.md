#DataComm
# What to know
- Nothing after slide 17 wk6-1
- 
### Coding questions on Midterm
- How to write thread to run code
	- both JavaFx and Java
- Pi4J, write code to control circuit controlling button and LED.
- Junit5 Tests 

## Topics
- [x]  Difference analog vs digital
- [x] cloud services(IaaS, PaaS, SaaS, XaaS)
- [x] Fundamental signal properties
- [ ] Layers/Protocols of OSI, TCP/IP
- [ ] Common TCP/UDP port numbers
- [x] Serial Peripheral Interface (SIP)
- [x] IC2 (Inter integrated Circuits)
- [ ] ADC (Analog to Digital Converter ) Module
	- Whats the role
- [x] Resistor
	- Whats the role
- [ ] GPIO (General purpose Input/Output Header)
	- [x] 	What it be
	- [ ] What do the ports do
- [ ] Secure coding practices
	- What are they and why
- [ ] Circuit vs Packet switching
- [ ] IEEE 802.3 CSMA/CD (Carrier Sense Multiple Access with Collision Detection)
- [ ] IP Protocol
- [ ] Ethernet frame format
- [ ] IPv4 Classfull Network architecture
- [ ] IPv4 CIDR and Subnet Mask
- [ ] Private address range
	- If given address, can you tell if private or no
- [ ] DHCP protocol
- [ ] DNS protocol
- [ ] ARP protocol
- [ ] My dads SIN 