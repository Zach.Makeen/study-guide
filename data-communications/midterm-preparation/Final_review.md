# Final Review
## Analog and Digital Signals

> Signals are the electric or electromagnetic impulses used to encode and transmit data. Computer networks and data/voice communication systems transmit signals. Signals can be analog or digital

> Analog signals are continuous signals in which one time-varying quantity represents another time-based variable. These signals occur naturally in music, voice, etc. It is harder to separate noise from an analog signal than it is to separate noise from a digital signal. Noise is unwanted electrical or electromagnetic energy that degrades the quality of signals

> Digital signals are non-continuous signals which are used to represent data as a sequence of separate values at any point in time. It can only take on one fixed number of values. You can still discern a high voltage from a low voltage. However, with too much noise you cannot discern a high voltage from a low voltage.

E.g. Devices that use analog signals:

- Telegraph
- Telephone
- Answering Machine
- Fax Machine

E.g. Devices that use digital signals:

- Computer
- Smartphone
- Internet
- Broadband/wireless

Key Differences

- An analog signal is a continuous signal whereas Digital signals are time separated signals.
- Analog signal uses a continuous range of values that help you to represent information on the other hand digital signal uses discrete 0 and 1 to represent information.

## Fundamental Signal Properties

All signals have three components:

- Amplitude - The height of the wave above or below a given reference point. Amplitude is usually measured in volts.

- Frequency - The number of times a signal makes a complete cycle within a given time frame. Frequency is measured in Hertz (Hz), or cycles per second (period = 1 / frequency).

- Phase - The position of the waveform relative to a given moment of time or relative to time zero. A change in phase can be any number of angles between 0 and 360 degrees. Phase changes often occur on common angles, such as 45, 90, 135, etc.

## Cloud Service

Cloud service provider (CSP): A third-party company providing on-demand access to computing resources without requiring the user to directly manage such resources.

- Infrastructure as a Service (IaaS) — This is the original service type. CSP customers can provision and launch any number of VMs that originally appear to be newly installed operating systems. It is the responsibility of the CSP customer to configure the VM and install any software they need to make use of the VM. E.g. DigitalOcean, Linode, Rackspace, Amazon Web Services (AWS), Cisco Metapod, Microsoft Azure, Google Compute Engine (GCE)

- Platform as a Service (PaaS) — Some businesses need more than just a new OS when they create a new VM. They may want to support application development and need development libraries, compilers, databases, and web or application services. The PaaS service type provides VMs with pre-installed and configured software that gives users a stable platform from which to start. E.g. AWS Elastic Beanstalk, Windows Azure, Heroku, Force.com, Google App Engine, Apache Stratos, OpenShift

- Software as a Service (SaaS) — Often considered to be the highest level of cloud service type, SaaS offerings rent access to specific software applications, such as SalesForce.com. SaaS customers do not want to install or manage software—they just want to use it. All of the installation, configuration, and maintenance responsibilities fall on the CSP. This service type is generally the most expensive, but the one that requires the lowest level of customer involvement. E.g. Google Workspace, Dropbox, Salesforce, Cisco WebEx, Concur, GoToMeeting

- Anything else as a Service (AaaS, also commonly called AaaS) — Extending the SaaS service type, many businesses offer their SaaS solutions as professional services, not just software their customers can use. The list of service offerings is always growing and currently includes services such as Security as a Service, Database as a Service, Blockchain as a Service, and Privacy as a Service.

## GPIO (General-Purpose Input/Output Header)

GPIO is the interface for connecting components to a Raspberry Pi. Each of the 40 GPIO pins has specific function, and most can be used as either input or output depending on your program or its function.

E.g. Here are some pins on the GPIO and their functions:

- Ground - The ground is very useful for making a common reference between all components in your circuit.

- Power - You can find 2 pins bringing 3.3V and 2 pins bringing 5V. Those pins can be used to power components such as sensors or small actuators.

- Reserved - The pins 27 and 28 are reserved pins. They are usually used for I2C communication with an EEPROM.

- GPIO - GPIO means General Purpose Input/Output. Basically that’s one pin you can use to write data to external components (output), or read data from external components (input).

https://pinout.xyz

### Common Network Topologies

- Point-to-point — A direct link or connection between two devices
- Ring — A shared ring connecting multiple devices together
- Star — A star-wired connection aggregation point, typically from a wiring closet
- Mesh — Multipoint connections and direct links between network devices
- Bus — A shared network transmission medium allowing only one device to communicate at a single time
- Hybrid/star-wired bus — Also known as a tree topology; a shared, star-wired connection aggregation point to a LAN switch

<img src="./images/common-network-topologies.png" width="60%" height="60%">

### Point-to-Point

> A network that consists of computers or devices that connect directly to one another is called a point-to-point network. Point-to-point communications are based on time slots and polling where communication controllers poll each dumb terminal one at a time to provide a time slot to transmit data to and from the mainframe computer. This type of network is difficult to scale. That means that it’s difficult to add many devices to the network, because each new device requires some new type of direct connection.

<img src="./images/point-to-point-network.png" width="60%" height="60%">

### Bus

> A bus topology (see FIGURE 3-14) starts with a coaxial cable that can support high-speed network communications but is subject to physical distance limitations and maximum number of devices per bus or Ethernet LAN segment. The main cable runs throughout the organization’s physical space and provides accessible connections anywhere along its length. Connecting to a bus network is easy. Any network device can attach an Ethernet transceiver to the bus, allowing for physical connectivity to the network-attached device.

<img src="./images/bus-network.png" width="60%" height="60%">

> The flexibility of a bus does come with a performance cost. Communications signaling traverses in both directions where the transceiver connects to the bus or coaxial cable. Because of this, only one device can communicate at a given time. Each network device’s transceiver must listen on the network (coaxial cable) to see if it’s available before transmitting. When two devices transmit at the same time, a collision occurs on the network, requiring both devices to retransmit again when the network is available. This led to the creation of the IEEE 802 committees focusing on both IEEE 802.3 CSMA/CD (Carrier Sense Multiple Access with Collision Detection).

### Ring

> Within token ring, all stations are connected in a logical ring while being star-wired from a centrally located wiring closet. Token ring–attached devices must have permission to transmit on the network. This permission is granted via a token that circulates around the ring.

> Token ring networks, as shown in FIGURE 3-16, move a small frame called a token. Possession of the token grants the right to transmit. If the network device that receives the available token has no information to send, it alters one bit of the token, which flags it as available for the next network attached device in line. When a network device wants to send information, it must wait for an available token, transmit information to the next upstream device, wait for the destination device to receive the data, and then a new token is released. Token ring– attached devices can transmit only when they have an available token. This eliminates communication collisions. A network collision occurs when two or more computers transmit at the same time on a bus topology. Forcing all communications in a unidirectional manner eliminates network transmission collisions

<img src="./images/ring-network.png" width="60%" height="60%">

### Star

> Network-attached devices used this star-wired cabling to physically and electrically connect to an Ethernet or token ring network. This was the beginning of star-wired topology deployments for both Ethernet and token ring networks.

<img src="./images/star-network.png" width="60%" height="60%">

> For Ethernet or IEEE 802.3 CSMA/CD networks, star wiring was used to provide a physical star, logical bus for the original IEEE 802.3i, 10Base-T standard and specification. The logical bus can be supported by either a hub or a switch. This hub or switch is typically installed in a centrally located wiring closet. Networks use two common types of devices to build star networks. A network hub is a very simple hardware device that has ports for multiple connections and echoes every message it receives to all connected ports. A hub does not make any decisions about message routing—it simply echoes all input. This simple design makes hubs easy and cheap to make. It also means that every node on the network receives messages it doesn’t care about. The other common star network device is the switch. A switch looks like a hub in that it has ports for multiple connections to network devices; however, it’s more sophisticated than a hub. A switch receives a message and examines the destination address. The switch maintains a table of connected devices and knows to which port it should forward the message, so it sends the message directly to the destination. Switches do not send messages to all destinations as hubs do.

<img src="./images/hub-switch.png" width="60%" height="60%">

### Mesh

> A mesh topology describes a network layout in which all nodes are directly connected to all other nodes. Actually, there are two different types of mesh topologies, depending on whether all or just most of the nodes are directly connected. Mesh networking is more common in wide area networking, given that alternative paths and redundancy are typically required.

#### Fully Connected Mesh

> A fully connected mesh follows the strict definition of a mesh topology; that is, every node directly connects with every other node. A fully connected mesh provides a path length of one for every connection. It also provides the maximum number of alternate paths in case any available paths fail. That means fully connected mesh topologies have a high degree of fault tolerance. Fault tolerance describes the ability to encounter a fault, or error, of some type and still be available. A fully connected mesh topology with IP routing can easily bypass a failed connection and still communicate through the mesh using alternate connections and nodes.

<img src="./images/fully-connected-mesh-network.png" width="60%" height="60%">

> The biggest drawback of a fully connected mesh topology is the required number of connections.

#### Partially Connected Mesh

> A partially connected mesh is a network topology in which network nodes connect to only some of the other nodes. Network designers identify the most active or important nodes and ensure those nodes connect to most other nodes. Lower priority nodes do not connect directly. All nodes in the network do connect, but some connections will have a path length greater than one. Partially connected mesh topologies provide more fault tolerance than many other topologies without requiring the expense of a fully connected mesh. This topology can be a good compromise when an organization needs better fault tolerance but cannot afford to set up and maintain a fully connected mesh.

<img src="./images/partially-connected-mesh-network.png" width="60%" height="60%">

### Hybrid

> Nodes that require high fault tolerance may benefit from a mesh topology. Lower usage networks may work better using a bus topology. A hybrid network that combines the two topologies can provide fault tolerance for some nodes while providing flexibility for other parts of the network.

## Internetworking

321

> Internetworking is a term used to describe connecting LANs together.

> A protocol stack is how software operates at different layers of the OSI model

### Internetworking with Bridges

> The easiest way to interconnect two networks is to place a MAC layer bridge in between the two networks. A bridge operates at the OSI Data Link Layer or Layer 2. That means it interacts with the Physical Layer and the Data Link Layer. A bridge can read the Media Access Control (MAC) layer address of each Ethernet frame to make a forwarding or filtering decision. The MAC address is a 6-byte address that is burned into a chip on the network interface card. Each MAC address is unique. A bridge examines the MAC address of each frame received, and then makes a filtering or forwarding decision based on the MAC address forwarding table.

### Internetworking with Switches

> A switch is essentially a bridge with more than two ports. A bridge generally connects two networks. A switch often has the ability to connect many devices and networks. Switches operate as an OSI Layer 2 device. They use MAC addresses and build address tables of devices connected to each physical port. Switches that connect multiple devices or networks keep track of MAC addresses on all ports. When the switch sees an Ethernet frame destined for a specific MAC address, it forwards that frame to that port.

> There are two kinds of LAN switches. A Layer 2 switch operates at the Data Link Layer and examines the MAC layer addresses of Ethernet frames. Using this information, filtering and forwarding tasks are performed by the switch.

> A Layer 3 switch operates at either the Data Link Layer or Network Layer. Layer 3 switches typically have software that lets them function like a Layer 2 switch or multi-port bridge. Layer 3 switches usually operate at the Network Layer that examines the network layer address within the Ethernet frame. A Layer 3 switch can look up the destination IP network number in its IP routing table, and then make a path determination decision. A Layer 3 switch provides each device connected to its ports with its own Ethernet LAN segment. Layer 3 switches typically have resiliency or a redundant path connection, also called a redundant circuit, to a building or campus backbone network.

### Internetworking with Routers

> A router operates at the Network Layer. Routers can make intelligent decisions on where to send packets. Instead of just reading the MAC address and forwarding a packet based on a forwarding table, routers can see the packet’s Network Layer address or IP address. The Network Layer address contains information about the destination network number and host number. In essence, routers perform a path determination calculation to find the best path for the IP packets to traverse. Typically they have redundant processors and plenty of memory, and can support lots of network connections. It takes longer for a router to examine a packet than for a Layer 2 switch or bridge. High-traffic networks may notice performance issues with multiple routers examining each IP packet to make a path determination decision.
