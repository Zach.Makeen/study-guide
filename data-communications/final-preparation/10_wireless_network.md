---
title: 'Wireless Network'
---

## Wireless Media

Radio, satellite transmissions, and infrared light are all different forms of electromagnetic waves that are used to transmit data.

<img src="../assets/10_wireless_media.png" width="60%" height="60%">  
<br />
<br />

### Terrestrial Microwave Transmission

- Land-based, line-of-sight transmission
- Approximately 20-30 miles between towers
- Transmits data at hundreds of millions of bits per second
- Signals will not pass-through solid objects
- Popular with telephone companies and business to business transmissions

<img src="../assets/10_terrestrial_microwave1.png" width="60%" height="60%">  
<br />
<br />

<img src="../assets/10_terrestrial_microwave2
.png" width="60%" height="60%">

### Satellite Microwave Transmission

- Similar to terrestrial microwave except the signal travels from a ground station on earth to a satellite and back to another ground station
- Can also transmit signals from one satellite to another
- Satellites can be classified by how far out into orbit each one is (LEO, MEO, GEO, and HEO)
- LEO (Low-Earth-Orbit): 100 to 1000 miles out  
  - Used for wireless e-mail, special mobile telephones, pagers, spying, videoconferencing
- MEO (Middle-Earth-Orbit): 1000 to 22,300 miles
  - Used for GPS (global positioning systems) and government
- GEO (Geosynchronous-Earth-Orbit): 22,300 miles
  - Always over the same position on earth (and always over the equator)
  - Used for weather, television, government operations
- HEO (Highly Elliptical Earth orbit): satellite follows an elliptical orbit
  - Used by the military for spying and by scientific organizations for photographing celestial bodies

<img src="../assets/10_satellite_microwave1.png" width="60%" height="60%">  
<br />
<br />

<img src="../assets/10_satellite_microwave2.png" width="60%" height="60%">  
<br />
<br />

Satellite microwave can also be classified by its configuration:

- Bulk carrier configuration
- Multiplexed configuration
- Single-user earth station configuration

<img src="../assets/10_satellite_configuration.png" width="60%" height="60%">

### WiMax - Broadband Wireless Systems

- Delivers Internet services into homes, businesses and mobile devices
- Designed to bypass the local loop telephone line
- Transmits voice, data, and video over high frequency radio signals
- Maximum range of 20-30 miles
- Theoretical speeds of 128 Mbps
- IEEE 802.16 set of standards

<img src="../assets/10_wimax.png" width="60%" height="60%">

### Cellular Telephones

- Wireless telephone service, also called mobile telephone, cell phone, and PCS
- To support multiple users in a metropolitan area (market), the market is broken into cells
- Each cell has its own transmission tower and set of assignable channels

<img src="../assets/10_cellular.png" width="60%" height="60%">  
<br />
<br />

Placing a call on a cell phone:
- You enter a phone number on your cell phone and press Send  
- Your cell phone contacts the nearest cell tower and grabs a set-up channel
- Your mobile identification information is exchanged to make sure you are a current subscriber
- If you are current, you are dynamically assigned two channels: one for talking, and one for listening; the telephone call is placed; you talk

Receiving a call on a cell phone:
- Whenever a cell phone is on, it “pings” the nearest cell tower every several seconds, exchanging mobile ID information;  this way, the cell phone system knows where each cell phone is
- When someone calls your cell phone number, since the cell phone system knows what cell you are in, the tower “calls” your cell phone

1st Generation:
- AMPS (Advanced Mobile Phone Service): first popular cell phone service; used analog signals and dynamically assigned channels
- D-AMPS (Digital AMPS): applied digital multiplexing techniques on top of AMPS analog channels

2nd Generation:
- PCS (Personal Communication Systems) – essentially all-digital cell phone service
- PCS phones came in the following technologies:
  - TDMA – Time Division Multiple Access
  - CDMA – Code Division Multiple Access
  - GSM – Global System for Mobile Communications
  - GPRS (General Packet Radio Service) used in GSM networks

3rd Generation:
- UMTS (Universal Mobile Telecommunications System) – also called Wideband CDMA
  - The 3G version of GPRS
  - UMTS not backward compatible with GSM (thus requires phones with multiple decoders)
- Whereas the first 2 generations of cellular telephones supported voice then text, 3G defined transition to broadband access

4th Generation:
- LTE (Long Term Evolution) – theoretical speeds of 100 Mbps or more, actual download speeds 10-15 Mbps
- HSPA (High Speed Packet Access) – 14 Mbps downlink, 5.8 Mbps uplink;
- HSPA+  – theoretical downlink of 84 Mbps, 22 Mbps uplink

What is 5G?
- New radio multiplexing technology
- Utilizes more efficient spectrum usage techniques
- New spectrum
- Allows up to 10 – 20 Gbits/s data rate
- Expected to provide support for immersive user interfaces, such as AR (Augmented Reality)/VR (Virtual Reality), mission-critical applications

5G won’t just support humans access the Internet from their smartphone; expected use cases include the following:
- Control of home appliances, industrial robots, and even self-driving cars
- Provisioning a large number of autonomous devices working together to provide service to end users
- Allows interconnection of large number of Internet-of-Things (IoT)

### Bluetooth

- Bluetooth is a specification for short-range, point-to-point or point-to-multipoint voice and data transfer
- Bluetooth can transmit through solid, non-metal objects
- Its typical link range is from 10 cm to 10 m, but can be extended to 100 m by increasing the power
- Bluetooth enables users to connect to a wide range of computing and telecommunication devices without the need of connecting cables
- Typical uses include phones, pagers, LAN access devices, headsets, notebooks and desktop computers

### ZigBee

- Based upon IEEE 802.15.4 standard
- Used for low data transfer rates (20-250 Kbps)
- Also uses low power consumption
- Ideal for heating, cooling, security, lighting, and smoke and CO detector systems
- ZigBee can use a mesh design – a ZigBee-enabled device can both accept and then pass on ZigBee signals

### Near-Field Communications

- Very close distances or devices touching
- Magnetic induction (such as radio frequency ID) used for transmission of data
- Commonly used for data transmission between cellphones

### Wireless Local Area network (WLAN)

- IEEE 802.11 specifies the standard for WLAN
- Wireless devices must have a client radio (containing a transceiver), usually as a network card with an integrated antenna installed

IEEE 802.11 supports 3 basic topologies for WLANs:
- Independent Basic Service Set (IBSS),
- Basic Service Set (BSS),
- Extended Service Set (ESS)

Independent Basic Service Set (IBSS)
- IBSS configurations are commonly referred to asad-hoc wireless network
- Networks utilizing this configuration are similar to peer-to-peer networks:
  - Group of wireless devices configured to communicate with each other without connection to a wired network
  - None of the nodes is required to serve as an access point

Basic Service Set (BSS)
- BSS consists of at least one access point (AP) connected to a wired network and a set of wireless devices that connects wirelessly to the AP
  - Communications between, for example, device A and device B, flows from device A to the AP and then from the AP to device B
  - Access point acts as a bridge between the wired and wireless networks and can perform basic routing functions

Extended Service Set (ESS)
- ESS consists of two or more overlapping BSSs, each containing an AP connected to a wired network
- The Mobile devices within an ESS can seamlessly roam between the APs
  - Thus, wireless network coverage over a relatively large area is possible
