---
title: 'Wans'
---
Wide area network (WAN):
- Connectivity between multiple locations via the Internet.

Software-defined WAN (SD-WAN):
- Enables customer to control and configure WAN using point-and-click interface
- Requires physical connection to be preinstalled

## WAN Technologies in the 1970s

A **modem** is a device that transmits and receives signals via tones. Modulation and demodulation is a technique for transmitting 1’s and 0’s over an analog transmission line. Modem transmission speeds peaked at 56 Kbps when analog dial-up services were deployed for initial Internet access.

**Analog leased lines or circuits** provide a point-to-point transmission path. This connection is always on.

## WAN Technologies in the 1980s

**Circuit switching** requires telecommunication switches to define a complete end-to-end path through the network before transmission can occur. This is the equivalent of an end-to-end communication path. Examples of circuit-switched networks are the PSTN and the Integrated Services Digital Network (ISDN). ISDN was a digital replacement for the archaic analog phone system.

Frame and **packet switching** requires the packet switches to know about the next hop switch. The source identifies the destination and transmits packets to the next hop switch. Packet switches derive the best path to take within the packet- switched network to reach the destination via virtual circuits.

During the late 1980s, service providers deployed **digital transmission switches**, which replaced analog transmission switches. These switches support the DS0/DS1/DS3 family of digital communications.

**Satellite communications** provide the equivalent of a leased line.

## WAN Technologies in the 1990s

The 1990s saw the use of **optical fiber**, **SONET backbone networks**, and **ATM Layer 2 circuit-switching and packet-switched** technologies.

Prior to the broadband boom, service providers were looking for other high-speed transport services. Support for voice, video, and data transmissions was needed. Asynchronous Transfer Mode (ATM) emerged as a solution.

ATM is a Layer 2–type technology. ATM switches establish point-to-point connections between endpoints to support data transmission.

### WAN Technologies in the 2000s

Service providers needed a standardized method to deploy high-speed optical fiber backbone networks. **Synchronous Optical Network (SONET)** is a standard developed by the American National Standards Institute (ANSI), which defines the specifications for synchronous digital transmission on optical fiber. SONET is deployed by service providers in a ring topology.

**MPLS** is a packet-switching technology, operates at Layer 2, and does not route at Layer 3. Service providers adopted MPLS switches for use as enterprise WAN solutions. MPLS allows IP traffic to flow in a full or partial mesh configuration. It supports Quality of Service (QoS) and Class of Service (CoS), allowing IP traffic to be prioritized through the network. MPLS is ideal for Voice over Internet Protocol (VoIP), streaming, and time-sensitive applications.

- Quality of service (QoS): A measure of how successful a network is at meeting packet delivery timing and error rate goals.

### WAN Technologies in the 2010s

Software-Defined Networking (SDN):
- A network in which the control plane is physically separate from the forwarding plane, and a single control plane controls several forwarding devices
- Its an emerging architecture that is dynamic, adoptable and cost-effective
- This architecture separates the network control and forwarding functions; thus, enabling the network control to become programmable

SDN is widely deployed by cloud provides; other use cases involved the following:
- Network Virtualization is the first widely-adapted use case of SDN
   - Example of network virtualization include VPN (Virtual Private Network) and VLAN (Virtual Local Area Network)

## Extra

Virtual LANs (VLANs): Layer 2 switches create flat topology networks. A flat topology means there is no hierarchy to the network structure. One of the drawbacks to a flat topology is that Ethernet broadcast frames are sent to all physical ports on the switch. Network segmentation and Layer 3 routers can limit the destination domain for broadcast messages, but Layer 2 switches cannot. On the other hand, switches work much faster because they operate at a lower network level. To address the problem of global network broadcasts, LAN switches allow the network designer to configure virtual LANs (VLANs). A VLAN defines a single broadcast domain where the Ethernet broadcast frame is allowed to traverse. Switches can map devices to specific VLANs and limit the scope of broadcasts to the assigned VLAN. As networks grow, VLANs can be configured to shrink the size of the broadcast domain. The IEEE 802.1Q standard defines how VLANs can be deployed within Layer 2 switches using Ethernet tagging. Tagging allows you to send specific Ethernet frames to other switches where members of the same department or workgroup reside.
