---
title: 'Error Detection'
---

## Errors
Despite the best prevention techniques, errors may still happen. To detect an error, something extra must be added to the data/signal.

There are three basic techniques for detecting errors:

### Parity Checking
- Simple parity :
    - If performing an even parity, add an parity bit such that an even number of 1s are maintained
    - If performing an odd parity, add a parity bit such that an odd number of 1s are maintained

- Logitudinal parity:
    - Adds a parity bit to each character then adds a row of parity bits after a block of characters
    - The row of parity bits is actually a parity bit for each "column" of characters
    - The row of parity bits plus the column parity bits add a great amount of redundancy to a block of characters

Both parity checking do not catch all errors, simple parity only catches odd numbers of bit errors, whereas longitudinal parity is better at catching errors but requires too many check bits added to a block of data 

### Arithmetic Checksum

It is used in TCP and IP on the Internet. 
It transmits characters by converting them into a numeric form and sums them. The sum is placed in some form at the end of the transmission.
Reciever performs the same coversion and sum to then compare the new sum with the sent sum.
TCP and IP process is a little more complex but the idea is the same.

### Cyclic Redundancy Checksum
CRC error detection method treats the packet of data to be transmitted as a large polynomial.
Transmitter takes the message polynomial and using polynomial arithmetic, it divides it by a given generating polynomial. The remainder is attached to the end of the message after doing a division.
The message with the remainder is then transmitted to the receiver.
The receiver divides the message and remainder by the same generating polynomial. If the remainder is not equal to zero results, there was no error during transmission.

<img src="../assets/06_crc.png" width="60%" height="60%">  
<br />
<br />

## Error correction
For a receiver to correct the error with no further help from the transmitter requires a large amount of redundant infos to accompany the original data. The redundant info allows the receiver to determine the error and make correction. This type of error control is often called forward error correction and involves codes called Hamming Codes.

### Hamming Codes
It adds additional check bits to a character, it performs parity checks on various bits.



