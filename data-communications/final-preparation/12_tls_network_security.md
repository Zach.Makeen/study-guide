---
title: 'TLS protocol and Network Security'
---
## TLS
For every encrypt connections, it uses symmetric key to encrypt and decrypt. However, it is necessary to have the session key changed every session to remain secure. It uses key generation algorithms, even with a hacker intercepting the message, they will be unable to reproduce that key. After verifying for the secure key, signature and digital certificate, it will then start exchanging session key.

## Network Security
- Confidentiality: 
    - Prevent unauthorized entities from accessing sensitive data
    - It is a driver for enabling seurity controls, like role-based access controls, data encryption in transit/at rest.
    - TLS offers confidentiality by using HTTPS to encrypt connection between browsers and web servers.
- Integrity: 
    - Protect data from unauthorized change
    - It is verified at different layers of the protocol stack
    - Ethernet frames have a built-in data integrity checker called CRC
    - IP packets have a checksum fct within the packet header
    - IEEE 802.3 Frame Format with CRC:
    <img src="../assets/12_crc_packets.png" >
    <br>
    *IP packet header checksum*
- Availability: 
    - Data is available to authorized entity when it is needed
    - Requirements for a user to access a system:
        - Authorized user with valid workstation/laptop
        - NTP (Network time protocol) && DNS (Domain Name System) servers
        - Physical network connection via wired/wireless local area (LAN)
        - Identification, authorization and authentication
        - Role-based access to systems, applications and data
        - Auditing, monitoring and logging of user session
        <img src="../assets/12_network_security_scope.png" >
    
### Network Risks, Threats and Vulnerabilities
| Network Risks   |   Network Threats |   Network Vulnerabilities |
|---|---|---|
| Availability to production apps && data is impacted  |   Compromised access controls | O/S software vulnerability and software bugs  |
| Integrity of data is compromised  |  Insider threat |   Application software vulnerabilities|
|  Access controls are compromised |   Malware |   Weak layered security design|
|   Critical network systems are down| No visibility into the network   |   |
|  Critical network systems are down |  Phising email |   |
| Network performance is degraded  |  Social engineering |   |
|   | Unaware users  |   |




