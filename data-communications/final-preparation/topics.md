---
title:  'Topics'
---

## List of topics for the final

- Cloud service model (IaaS, PaaS and SaaS)
- Raspberry Pi GPIO pin reference
- JavaFX scene graph
- Network topologies
- The roles of switches, bridges and routers
- Difference between switches and hubs
- What layer of the OSI reference model do they operate?
- The role of ARP
- VLAN
- Fundamental signal properties
- Analog vs digital signal
- Baud rate
- Header that gets placed on packet at each layer of OSI or TCP/IP reference model
- IPv4 and IPv6 addresses
- Broadcast, multicast, unicast and anycast messages
- Encoding schemes (Ascii, Extended Ascii, Unicode code points, Unicode plane, UTF-8, UTF-16)
- Data compression schemes
- Noise
- Error detection schemes (parity check, Longitudinal parity, Arithmetic checksum, Cyclic redundancy check)
- Wireless technologies
- Wireless security
- Cryptographic schemes and
- Domain name resolution
- NAT
- Cellular Telephone technologies
- WAN technologies
- Authentication schemes
- TLS protocol
- Malware
