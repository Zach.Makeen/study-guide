---
title:  'Physical & Data Link Layer'
---

## Introduction to Signals

Signals are the electric or electromagnetic impulses used to encode and transmit data. Computer networks and data/voice communication systems transmit signals. Signals can be analog or digital.

### Analog Signals

Analog signals are continuous signals in which one time-varying quantity represents another time-based variable. These signals occur naturally in music, voice, etc. It is harder to separate noise from an analog signal than it is to separate noise from a digital signal.

<img src="../assets/05_analog.png" width="60%" height="60%">  
<br />
<br />

E.g. Devices that use analog signals:

- Telegraph
- Telephone
- Answering Machine
- Fax Machine

### Digital Signals

Digital signals are non-continuous signals which are used to represent data as a sequence of separate values at any point in time. It can only take on one fixed number of values. You can still discern a high voltage from a low voltage. However, with too much noise you cannot discern a high voltage from a low voltage.

<img src="../assets/05_digital.png" width="60%" height="60%">  
<br />
<br />

E.g. Devices that use digital signals:

- Computer
- Smartphone
- Internet
- Broadband/wireless

### Noise

Noise is unwanted electrical or electromagnetic energy that degrades the quality of signals.

### Fundamental Signal Properties

All signals have three components:

- Amplitude: The height of the wave above or below a given reference point. Amplitude is usually measured in volts.

<img src="../assets/05_amplitude.png" width="60%" height="60%">  
<br />
<br />

- Frequency: The number of times a signal makes a complete cycle within a given time frame. Frequency is measured in Hertz (Hz), or cycles per second (period = 1 / frequency).

<img src="../assets/05_frequency.png" width="60%" height="60%">  
<br />
<br />

- Phase: The position of the waveform relative to a given moment of time or relative to time zero. A change in phase can be any number of angles between 0 and 360 degrees. Phase changes often occur on common angles, such as 45, 90, 135, etc.

<img src="../assets/05_phase.png" width="60%" height="60%">

## Baud Rate

Serial communication is done at a predefined speed. This speed is referred to as baud rate. It refers to the number of bits transmitted per second. The most frequently used baud rate is 9600 bps. Lower baud rate, i.e., 1200, 2400, 4800 or high baud rate, i.e., 19200, 38400, 57600, 115200 can be used depending on the speed that can be handled reliable by the device. The sender and receiver need to handle the data at the same baud rate.
