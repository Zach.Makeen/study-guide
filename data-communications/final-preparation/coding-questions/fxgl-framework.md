---
title: 'FXGL'
---

These are the 6 requirements for a basic FXGL game:

1. A game window: we will use a 700x700 size window
2. A player on the screen: shapes or images can be used; we will use a blue circle
3. Moving the player: designated keystrokes can be used; we will use UP, DOWN, LEFT and RIGHT arrows to move the player
4. UI that the player will interact with: we will use a single line of text to represent our UI
5. Updating the player moves: we will show the total number of pixels the player moved during its lifetime

## Requirement 1: Window

- All settings for the window are changed within initSettings()
- Once the settings are made, changes cannot be made during runtime

```
@Override
protected void initSettings(GameSettings settings) {
    settings.setWidth(700);
    settings.setHeight(700);
    settings.setTitle("Basic Game App");
    settings.setVersion("0.1");
}
```

## Requirement 2: Player

- Players can be added to the screen via initGame()
- initGame() can be used to add all the stuff that needs to be ready before the game starts

```
@Override
protected void initGame() {
    player = FXGL.entityBuilder()
            .at(300, 300)
            .view(new Circle(300.0f, 300.0f, 15, Color.BLUE))
            .buildAndAttach();
}
```

## Requirement 3: Input

- Input handing code is placed in initInput()

```
@Override
protected void initInput() {
    Input input = FXGL.getInput();

    input.addAction(new UserAction("Move Right") {
        @Override
        protected void onAction() {
            player.translateX(5); // move right 5 pixels
        }
    }, KeyCode.RIGHT);
}
```

- The previous code can be shortened using lambda

```
@Override
protected void initInput() {
    FXGL.onKey(KeyCode.RIGHT, () -> {
        player.translateX(5); // move right 5 pixels
    });
}
```

- The full code for inputs is as follows:

```
@Override
protected void initInput() {
    FXGL.onKey(KeyCode.RIGHT, () -> {
        player.translateX(5); // move right 5 pixels
    });

    FXGL.onKey(KeyCode.LEFT, () -> {
        player.translateX(-5); // move left 5 pixels
    });

    FXGL.onKey(KeyCode.UP, () -> {
        player.translateY(-5); // move up 5 pixels
    });

    FXGL.onKey(KeyCode.DOWN, () -> {
        player.translateY(5); // move down 5 pixels
    });
}
```

## Requirement 4: UI

- UI is handled in initUI()

```
@Override
protected void initUI() {
    Text textPixels = new Text();
    textPixels.setTranslateX(50); // x = 50
    textPixels.setTranslateY(100); // y = 100

    // Add UI node to the scene graph
    FXGL.getGameScene().addUINode(textPixels);
}
```

## Requirement 5: Gameplay

- A game variable is needed for gameplay
- In FXGL, a game variable can be accessed and modified from any part of the game
- Game play variable is like global variable with its scope tied to FXGL game instance
- The following code create a gameplay variable

```
@Override
protected void initGameVars(Map<String, Object> vars) {
    vars.put("pixelsMoved", 0);
}
```

- The game variable needs to be updated when the player moves
- This can be done in the input handling section

```
FXGL.onKey(KeyCode.RIGHT, () -> {
    player.translateX(5); // move right 5 pixels
    FXGL.inc("pixelsMoved", +5);
});
```

- The last step involves binding the UI text object to the variable pixelsMoved
- In initUI(), once we created the textPixels object, we can do the following:

```
textPixels.textProperty().bind(FXGL.getWorldProperties().intProperty("pixelsMoved").asString());
```

- This allows the UI text to automatically show the number of pixels the player has moved
