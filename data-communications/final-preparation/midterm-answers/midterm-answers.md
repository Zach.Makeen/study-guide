# Midterm Questions

## Multiple Choice

### 1. Which of the following uses analog signal for communication?
a. Telegraph
b. Answering machine
c. Fax machine
d. All the above
ANSWER: D


### 2. Which of the following cloud service involves paying rent to access specific software applications?
a. IaaS
b. SaaS
c. PaaS
d. All the above
ANSWER: B


### 3. What layer of the TCP/IP Model is responsible for transmitting signal?
a. Layer 1
b. Layer 2
c. Layer 3
d. Layer 4
ANSWER: B


### 4. What is the signal property that changes in the following diagram?
**Can't put pictures in MD but here: https://gyazo.com/c737c6cb2c498ee1ec76e1bd78c33cf5
a. Frequency
b. Phase
c. Amplitude
d. None of the above
ANSWER: B

### 5. Processes of programs that interact with human, and use network to send and receive, reside at what layer of the OSI Reference model?
a. Network Layer
b. Presentation Layer
c. Application Layer
d. None of the above
ANSWER: C


### 6. Domain Name Service (DNS) uses which TCP/UDP port?
a Port 53
b. Port 80
c. Port 22
d. Port 443
ANSWER: A


### 7. Which of the following statements is true concerning SPI (Serial Peripherical Interface) or I^2C
(Inter-Integrated Circuit)?
a. 2 lines are required to send data via SPI
b. 4 lines are required to send data via PC
C. 2 lines can be used send data both via SPI and 1°C
d. None of the above is correct
ANSWER: D

### 8. Which of the following fields is not contained in an IPv4 header?
a. Source IP address
b. Destination address
c. Mac address
d. All the above are contained in an IPv4 header
ANSWER: C


### 9. In reference to IPv4 classful network architecture, the IP address 172.217.13.195 belongs to what class?
a. Class A
b. Class B
c. Class C
d. Class B or Class C
ANSWER: B


### 10. Which of the following is not a private IPv4 address?
a. 10.20.50.3
6. 192.166.72.10
c. 172.17.43.87
d. All the above are private addresses
ANSWER: B


### 11. Which of the following is not a field in Ethernet header?
a. Source MAC address
b. Destination MAC address
c. Source IP address
d. CRC
ANSWER: C


### 12. Consider the following: node A determines that a node that it wishes to communicate with (node B) is on the same network as itself.Assume that node A doesn't have the necessary address for node B, what protocol would node A uses to find the required address for node B?
a. DNS
b. ARP
c. DHCP
d. IP
ANSWER: B
