---
title:  'Communication Technology Evolution'
---

## Cloud Services

Cloud service provider (CSP): A third-party company providing on-demand access to computing resources without requiring the user to directly manage such resources.

- Infrastructure as a Service (IaaS): This is the original service type. CSP customers can provision and launch any number of VMs that originally appear to be newly installed operating systems. It is the responsibility of the CSP customer to configure the VM and install any software they need to make use of the VM. E.g. DigitalOcean, Linode, Rackspace, Amazon Web Services (AWS), Cisco Metapod, Microsoft Azure, Google Compute Engine (GCE)

- Platform as a Service (PaaS): Some businesses need more than just a new OS when they create a new VM. They may want to support application development and need development libraries, compilers, databases, and web or application services. The PaaS service type provides VMs with pre-installed and configured software that gives users a stable platform from which to start. E.g. AWS Elastic Beanstalk, Windows Azure, Heroku, Force.com, Google App Engine, Apache Stratos, OpenShift

- Software as a Service (SaaS): Often considered to be the highest level of cloud service type, SaaS offerings rent access to specific software applications, such as SalesForce.com. SaaS customers do not want to install or manage software—they just want to use it. All of the installation, configuration, and maintenance responsibilities fall on the CSP. This service type is generally the most expensive, but the one that requires the lowest level of customer involvement. E.g. Google Workspace, Dropbox, Salesforce, Cisco WebEx, Concur, GoToMeeting

- Anything else as a Service (AaaS, also commonly called AaaS): Extending the SaaS service type, many businesses offer their SaaS solutions as professional services, not just software their customers can use. The list of service offerings is always growing and currently includes services such as Security as a Service, Database as a Service, Blockchain as a Service, and Privacy as a Service.
