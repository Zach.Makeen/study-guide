---
title:  'Network & Transport Layer'
---

## IPv4

IPv4 addresses are 4-byte (32-bit) numbers. This means IPv4 can address 232 or 4,294,967,296 unique devices.

Organizations that use Network Address Translation (NAT) and private IP addresses make it possible to continue using IPv4 even through the rapid Internet expansion. Although it’s being replaced with IPv6, IPv4 will be around for many years.

Network Address Translation (NAT) is the process of replacing a private IP address with a public IP address, or vice versa.

The organization’s perimeter network devices will use NAT to map internal private IP addresses to external routable IP addresses. As defined in RFC 1918, the ranges of private IPv4 addresses are:

- 10.0.0.0 to 10.255.255.255
- 172.16.0.0 to 172.31.255.255
- 192.168.0.0 to 192.168.255.255

IPv4 addresses are not commonly expressed as 32-bit integers. Instead, they are written as four separate 8-bit numbers, called octets, separated by periods or dots. This common IPv4 representation is called dot notation. An 8-bit number can be in the range of 0 to 255, so these numbers are easier for people to handle.

## IPv6

The primary motivation for creating IPv6 was to increase the network address space. IPv6 accomplishes this goal by increasing the address size from the IPv4 32 bits to 128 bits. IPv4 could address 232 devices, or around 4.3 billion. IPv6’s larger address size can address 2128 devices, or over 340 undecillion.

IPv6 does much more than just switch to a bigger address. In using a bigger address, IPv6 effectively does away with the need for NAT. Recall that NAT was introduced to deal with the eventual exhaustion of IPv4 addresses. With a nearly endless pool of IPv6 addresses, NAT is no longer needed to reuse network addresses.

IPv6 addresses are 128 bits in length, so they need a different notation. The classic IPv4 dot notation would require 16 octets and be awkward to use. IPv6 addresses are expressed as eight groups of four hexadecimal digits. Each group is separated by colons. For example, you would write an IPv6 network address as:

    2001:0db8:0000:0000:0206:0000:a80c:052b

IPv6 addresses that specify a unique device are called unicast addresses. Generally, IPv6 unicast addresses are logically divided into two 64-bit segments (see FIGURE 5-7). The first 64-bit segment (the first four groups of hexadecimal digits) represents the network prefix, which identifies the network.

IPv6 uses a variable length network prefix similar to CIDR in IPv4, which also uses a variable length network prefix. The second 64-bit segment (the second group of four hexadecimal digits) represents the interface identifier (IID) or host.

<img src="../assets/05_ipv6_format.png" width="60%" height="60%">  
<br />
<br />

The IPv6 address format can still be long and sometimes
repetitive; for instance, consider the following IPv6 address:

    2001:0db8:0000:0000:0206:0000:a80c:052b

The IPv6 specification offers two rules to abbreviate IPv6 addresses. Abbreviated addresses are smaller and contain less redundant information. The two rules for IPv6 address abbreviating are:

1. You may omit any leading zeros within any group of hexadecimal digits.
2. You may replace only one grouping of two or more consecutive groupings of zeros in an address with a double colon.

<img src="../assets/05_redundancy_rule.png" width="60%" height="60%">

## Network Methodologies

IPv6 supports three different methodologies to send packets. The different methodologies make it easier to send packets to their desired destinations. IPv6 includes methods to send packets to one, several, or many destinations. These methodologies include:

- Unicast: Sending a packet to a single destination
- Anycast: Sending a packet to the nearest node in a
specified group of nodes
- Multicast: Sending a packet to multiple destinations

Unlike IPv4, IPv6 does not support sending broadcast-type packets. A broadcast address sends a packet to a complete range of IP addresses. Sending broadcast messages to a large subnet can cause network congestion and allow attackers to affect a network’s availability.

## IP Communications

The Address Resolution Protocol (ARP) is the common protocol in IPv4 networks to provide local MAC addresses. IPv6 networks use the Neighbor Discovery Protocol (NDP) to provide a service similar to ARP. To find a local MAC address, the network device would first look up the supplied IPv4 address in its ARP table. A device’s ARP table contains the IPv4 address and the corresponding MAC address of known nodes. If the device finds no entry for the IPv4 address, it sends an ARP broadcast query to the network.
