---
title: 'Data Communication Security'
---

## Security Goals
- Confidentiality: Prevent unauthorized entities from accessing sensitive data
- Integrity: Protect data from unauthorized change
- Availability: Data is available to authorized entity when it is nedded

Categories of security attacks:
<br><br>
<img src="../assets/07_security_attacks.png">
<br><br>

## Confidentiality service
### Encryption
Confidentiality can be achieved using encryption. The definition of encryption is the practice of hiding messages so that they cannot be read by anyone other than the intended recipient.

### Type of encryption algorithms:
- Symmetric-key encryption algorithms: 
    - Algorithms in which the same key is used for both encryption and decryption.
    - Block ciphers: Encypts data one block at a time (size is typically 64 || 128 bits)
        - Data Encryption Standard (DES): Use a 56-bit key to encode 64-bit blocks of data &&& insecure due to the small key size
        - TripleDES: Uses DES 3x in tandem && also insecure
        - Advanced Encryption Standard (AES): Replacement of DES with 3 sizes (128/192/256 bits keys) && uses 128 bit block size
    - Stream ciphers: Encrypts one bit or one byte at a time && encrypt/decrypt faster than block cipher
        - RC4: Most widely known stream cipher and used in WEP, WPA and TLS/SSL. However it is insecure and should no longer be used.
- Asymmetric/Public-key encryption algorithms: 
    - Algorithms in which a private key is used to decrypt and a public key is used to encrypt. The encryption algorithms are based on a mathematical intractable problem. It is much slower than a symmetric-key.
    - RSA:
        - It is the most commonly used asymmetric-key encryption algorithm. It is based on the difficulty of factoring prime numbers. It supports variable length key sizes, but the recommended key size is minimum of 2048 bits.

Strength of cryptographic algorithms is determined by the length of the key in bits (A 128-bit key has 2<sup>128</sup> possible keys). Set of possible keys for a cryptographic algorithm is called the key space. Brute-force must be used to crack the key.

### Java AES Encryption and Decryption
- CFB (Cipher FeedBack): The IV is encrypted, then the result is XORed to the first block of plaintext and afterwards the result of the last operation is encrypted. Those steps are repeated.
- OFB (Output FeedBack): Similar to CFB, but the IV is encrypted each time to be XORed with the blocks of plaintext.
- CTR (Counter Mode): It uses a counter as an IV, similar to OFB, the counter is encrypted each time and the result XORed with the blocks of plaintext.
- GCM (Galois/Counter Mode): This mode is an extension of CTR mode. It encrypts and calculates an authentication tag (at least 128 bits long). It will typically be appended to the ciphertext.

### Implementing AES-GCM in Java
It is the most widely used authenticated cipher. It inputs : data, AES secret key, 12 bytes IV, lengths in bits of authenticated tag. See example of Carlton [here](https://gitlab.com/crdavis/crytpoexamplecode).


## Integrity Service
### Message Authentication and Integrity
One of the following cryptographic scheme is required for integrity check:
- Hash function
- Message Authentication Code (MAC)
- Digital Signature

### Hash Function
It is an algorithm that takes data or messages of any length and produces a fix-length output called a message digest/fingerprint. It must be strongly collision-free to be useful. A collision occurs when there are 2 different messages but gives the same message digest.

Examples of Hash functions:
    - MD5: Produces 128-bit digest, collision against it can be calculated within seconds = insecure.
    - SHA-1 (Secure Hash Algorithm 1): Produces 160-bit digest, can be calculated within a relative short time period = insecure.
    - RIPEMD-160 (Race Integrity Primitives Evaluation Message Digest): Produces 160-bit digest
    - SHA-2: Algorithm which has SHA-256 and SHA-512
    - SHA-3: Provides the same output sizes as SHA-2

### Message Authentication Code (MAC)
It is similar to the message digest produced by hash functions, however a secret key is required to produce the MAC. HMAC (hash-based MAC) is the algorithm that is used to produce a message authentication code with a secret key. Any hash function can be used to construct a HMAC. E.g.: HMAC-SHA-256, HMAC-RIPEMD-160...

<img src="../assets/07_mac.png">
<br><br>

### Password Hashing

Regular hash function is not suitable for password hashing due to processor speed && CPU cores being more performant and attackers that have easier access to faster computers and can use dictionary attack to discover passwords. A solution to handle this issue is to use hashing algorithms and make brute force attack decrease significantly. A configurable hash-time parameter is needed.

### Argon2
It is currently the recommended choice of hash functions that satisfied this requirement. It accepts 5 params:
    - Salt length: Length of the random salt, recommends 16 bytes
    - Hash length: Length of the generated hash, recommends 16 || 32 bytes
    - Iterations: Number of iterations, affect the time cost
    - Memory: Amount of memory used by the algorithm (k, 1k = 1024 bytes), it affects memory cost
    - Parallelism: Number of threads/lanes used by the algorithm, it affects the degree of it.
It also has 3 variants:
    1. Argon2d: Maximized resistance to GPU cracking attacks (Good for cryptocurrency)
    2. Argon2i: Optimized to resist side-channel attacks (Good for password hashing)
    3. Argon2id: A hybrid version of the previous 2.


### Digital Signature
It consists of a signing algorithm and a verification algorithm using a key pair. The private key is used to generate the signature (signing), while the public key is used to decrypt the signature (verification).

<img src="../assets/07_digital_signature.png">
<br><br>

To reduce the size, the message is first hashed using a hash function and then the message digest is signed.

<img src="../assets/07_hash_sign.png">
<br><br>

Examples of schemes:
    - Elliptic Curve Digital Signature Algorithm (ECDSA)
    - RSA
    - Edwards-Curve Digital Signature Algorithm (EdDSA)

ECDSA and EdDSA provide higher security for much smaller key size. [Carlton's example](https://gitlab.com/crdavis/crytpoexamplecode)


### Digital Certificate
It is an electronic data that binds a public key to an individual || organization || system || entity. Certficate Authority (CA) is a trusted entity that creates, signs and manage it. A CA will attest the validity of the certficate by signing with their private key.

### User authentication Schemes
- Password
- Digital Certificates
- Radio-frequency Identification (RFID) cards
- Biometric (Fingerprints, eye scans, etc)