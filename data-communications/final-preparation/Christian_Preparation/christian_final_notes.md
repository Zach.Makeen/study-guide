---
title:  'Christian Review notes ma geule'
---

## Cloud Service models

### IaaS
Infrastructure as a service is when a company provides barebone infra (no programs in it, no nothing (double negative omega)). This is basically when 
CSP(Cloud Serviec provider) provides a VM that appears as a newly installed operating system. It only provides server and bandwidth . The customer then has the responsability to install 
all the software and configure the VM to their liking to use it. 

Examples: DigitalOcean, Linode, Rackspace, Amazon Web Services (AWS), Cisco Metapod, Microsoft Azure, Google Compute Engine (GCE)
(stolen from zach btw. Mf I'm on dawson parole I gotta put attribution and shit now.)

### PaaS
Platform as a service is when the CSP provides a VM with pre-installed programs, libraries, compilers, databases and web or application services.
That way the machine is ready to use as soon as the customer gets it. 

Examples: AWS Elastic Beanstalk, Windows Azure, Heroku, Force.com, Google App Engine, Apache Stratos, OpenShift
(stolen again.)

### SaaS 
Software as a service is when the CSP provides the software that the customer wants to use. The CSP is the entity responsable for the installation,
configuration and maintenance. This is often the most expensive and considered the highest level of cloud service type.

Examples: Google Workspace, Dropbox, Salesforce, Cisco WebEx, Concur, GoToMeeting

### AaaS or XaaS
Anything as a service is extending the SaaS type. From my understading, this is basically being able to pick out separately what you want and then
put it all togheter in a VM. Say you want a software but not all the bullshit that comes with it, you could select your own Infrastructure, the 
specific platform that you want, the software that you want and then you could take a specific database over another etc or certain functionalities.

Examples: Security as a Service, Database as a Service, Blockchain as a Service, and Privacy as a Service.


## RaspberryPi GPIO pin reference

Honestly don't know what the fuck he wants for this part but hey here I go.

The GPIO(General purpose Input/Output header) is the interface for connecting components such as LEDs, resistors, breadboard and others to raspberryPi.
It's basically all the pins on the Pi. Each of the 40 GPIO pins has a specific function and most cane be used either to input or output data depending
on what you need your program to do. 

There are two ways to number the pins. The GPIO numbering, assigned by Broadcom (BCM). The physical way is by counting across and down from pin 1.

SEE THIS LINK FOR THE PIN NUMBERS https://pinout.xyz/

I'm guessing he's gonna want us to name some pins (hopefully not know all of them by heart), so here are some pin examples

Ground - The ground is very useful for making a common reference between all components in your circuit.
Power - You can find 2 pins bringing 3.3V and 2 pins bringing 5V. Those pins can be used to power components such as sensors or small actuators.
Reserved - The pins 27 and 28 are reserved pins. They are usually used for I2C communication with an EEPROM.
GPIO - GPIO means General Purpose Input/Output. Basically that’s one pin you can use to write data to external components (output), or read data from
external components (input).

### Serial Peripheral Interface
The SPI are commoncly used for sending data between micro controllers and small external peripheral devices.
Multiple peripheral devices can be interfaced as SPI slves and must be controlled with the SPI master.
SPI Uses separate clock (SCK) and data lines (MISO,MOSI)(think of miso soup and mossy stones) along with a select line(SS) to choose among slave devices.

Basically, the SPI allows us to connect multiple devices(slaves) to the RaspberryPi(Master) and allows us to send and receive data between them using
the MOSI AND MISO pins. MOSI(Master output, Slave input) is used to send data FROM the master TO the slave. And as the smart individual that you are,
you guessed that MISO(Master input, Slave output) is used to send data FROM the salve TO the master. The SCK is used to read data from every bus at
every tick of the clock. 

### I2C 
The I2c interface allows multiple slave integrated circuits to communicate with one or more masters.
I2C uses two directional lines: Serial Data Line(SDA)(So 1 bit at a time) and Serial Clock Line(SCL).

This is most likely not in the exam, but if you want to know! SPI uses a Select Slave data line to let it choose which slave it wants to communicate with,
while the I2C uses slave addresses to choose which slave to communicate with. Hence, when using SPI, you're gonna need multiple wires for each slave
that you want to communicate with, while in I2C you could just connect them in a series type of circuit(one connected to the other)

## JavaFX Scene Graph
<img src="../../assets/ChristianAssets/JavaFXSceneGraph.PNG" width="40%" height="40%">

Stage: The stage is the JavaFX represntation of native os window. So like the application window default of your operating system, like lower window, small 
the x and windows and the 3 ugly colored dots of macOS(imagine).

Scene: The scene is the container for the JavaFX scene graph, a stage can only have 1 scene attached to it. all the elements inside the scene are nodes.
- Root: The only node that doesn't have a parent and is directly contained by the scene.
- Branch: Is a node that has on or more children
- Leaf: Is a node that doesn't have a child

Remember: A node cannot have more than 1 parent. A node that is actively attached to a scene graph can only be modified from the JavaFX application Thread.
(this sentence is gonna give me PTSD lmao)

## Network topologies

Network topologies are the layout of how devices connect to a network.
A node in a network is a server, computer, smartphone that can physically connect to the network and has an assigned IP host address.

Physical topology: The picture of the actual network devices and the medium the devices use to connect ot the network
Logical topology: How the actual network works and how you transfer data in a netwrok; view focuses more on how the network topology operates;

There are many Network topologies:
-Simple networking diagram: 1 switch connecting many devices

<img src="../../assets/ChristianAssets/SimpleNetwork.jpg" width="20%" height="20%">

-Physical Netowrk Topology: 1 bus(cable) connecting many netowrks

<img src="../../assets/ChristianAssets/physicalNetwork.jpg" width="30%" height="30%">

-Common Network Topologies

<img src="../../assets/02_common_topologies.png" width="30%" height="30%">

### Point to point networks
Networks that consists of computers or devices that connect directly to one another.
- Difficult to add many devices because each new device requires direct connection.
- Found in very small environments that only need to connect a few PCs
- Are considered archaic(very old, mf using archaic is archaic)

<img src="../../assets/02_point_to_point.png" width="30%" height="30%">

### Bus topology
Coaxial cable (bus) runs throughout a physical space; network devices attach to bus using an ethernet transceiver
- Supports high-speed network communications
- Is simple to construct
- Is subject to physical distance limitations and maximum number of devices
- Only one device can communicate at a given time

<img src="../../assets/02_bus.png" width="30%" height="30%">

When two devices transmit at the same time, a collision occurs on the network.
Both devices must retransmit when the network is available.
Led to the creation of IEEE 802.3 CSMA/CD (Carrier sense multiple access with collision detection) and IEEE 802.5 token ring standards for
local area networking. CSMA/CD is found in the data link of the OSI model.

When a collision is detected, the protocol timesout all transmissions for a given timeframe and sends a jam singal(the jam signal is used to tell the devices 
to not transmit data for the moment) and then tries to send the packets.

### Ring topology
- All stations are connected in a logical ring
- Attached devices need permission to trasnmit on the network
- Permission is granted via a token(small frame) that circulates around the ring
- Unidirectional manner eliminates network transmission collisions
- A cut in the cable will break the ring network

<img src="../../assets/02_ring.png" width="30%" height="30%">

### Star topology
- Star wiring cabling from centrally locates wiring closet creates ethernet or token ring network.
- Wiring creates a physical star, logical bus.
- Can be supported by a hub or a switch typically installed in a centrally located wiring closet.
- Network-attached devices physically and electrically connect to network
- Fixes path lenght and attenuation issues

<img src="../../assets/02_star.png" width="30%" height="30%">

### Partial Mesh, Fully connected mesh, And Hybrid Topologies
Partial Mesh:
- Some devices in the partial mesh have priority. If a device needs to send packets to all devices it's connected to all of them, otherwise it is connected only to the machines
it needs to communicate with.

Mesh topology:
- A network layout in which all nodes are directly connected to all other nodes

<img src="../../assets/ChristianAssets/fullmesh.jpg" width="10%" height="10%">

Hybrid topology:
- A network that contains several different topologies
- Can provide fault tolerance for some nodes while providing flexibility for other parts of the network

## The roles of switches, Bridges and routers

### Bridges
Bridges operate at the OSI Data Link layer(Layer 2). Bridges read the MAC addresses of each Ethernet frame to make a forwarding or filtering decision. It examines the MAC address of each frame received and makes a filtering or forwarding decision based on the MAC address forwarding table. The bridge is the apparatus that keeps the table of MAC addresses for each port. To put more basically, a bridge is a device that connects 2 networks together using ports and keeps a table of the MAC addresses associated with the ports and uses that to
transmit information between the 2 networks in a quick manner.

### Switches
Switches operate at OSI Data Link layer(layer 2). Switches are basically like bridges but instead of connecting only 2 networks together, they can use multiple ports to connect multiple networks together. Switches also use MAC address tables to forward frames to a port associated with the destination MAC address.

There are also switches that operate at Layer 3 of the OSI model(Network layer). These switches have extra software that lets them look up the destination IP network number in the IP routing table and then make a path determination decision.

### Routers
Routers operate at the OSI Network Layer(Layer 3). They are the same thing as a Layer 3 Switch except they are used for wide area network circuit connections. Campus backbone connections and building backbone connections. Routers can make intelligent decisions on where to send packets. They get the IP from the packet header and performs a path determination calculation to find the best path for the IP packets to be sent.

### Differences between all of them
- Bridges and switches usually operate at Layer 2(Data link) while the Router operates at layer 3(Network Layer).
- Bridges and switches usually use MAC addresses while routers use IP addresses.
- Bridges connect only 2 networks together while the other 2 devices can connect multiple networks together.


## Difference between Hubs and Switches
The main difference between a Port and a Hub is that:
- A hub operates in the Physical Layer(Layer 1) while a Switch operates in the layer 2(Data Link Layer)
- A switch sends a packet to only the appropriate port(Using MAC address table) while a Hub sends packets to all ports.

## To recapitulate
Hubs operate in the Physical Layer(Layer 1) AND layer 2
Bridges operate in Data Link Layer(Layer 2)
Switches operate in Data Link Layer(Layer 2) OR Network Layer(Layer 3)
Routers operate in Network Layer(Layer 3).

## Role of ARP
The Address Resolution Protocol(ARP) is a protocol that translates IP addresses to MAC addresses. ARP is used in networks to communicate between devices.
Devices first look through their ARP Cache(IP/Mac Address table) to see if it has the MAC address for the IP they want to send a message to.
If no MAC address matches the IP, the computer sends a Broadcast message to all devices on the network asking who has the MAC adress for the IP.
The computer has that IP will send a message to the computer telling them what their MAC address is. The communication can then begin.

Probably not in the exam but. There are 2 ARP entries in the table. Static entries and dynamic entries.
- Dynamic entries are created when the computer sends a broadcast message to find a MAC address. They are not permanent and are deleted periodically to not have unused entries.
- Static addresses are entries that the user enters manually in the cache using command line.

## VLAN
VLAN is a subnetwork that connects multiple LANS with each being able to have their own set of rules. (S/O mate for this one man answered the question beautifully like 5 times).
An example of a VLAN would be a company having different LANs used by the departments in a company.
VLANs operate in the OSI Data Link Layer(layer 2).

VLANs are secured using WPA(Wifi Protected acces).
WPA uses dynamic key encryption and mutual authentication for wireless client. It uses RC4 encryption algorithm.
WPA has been replaced by WPA2 and WPA3.
- The main improvement of WPA2 is that it uses AES instead of RC4 encryption algorithm.
- WPA3 introduced in 2018, is built upon WPA2 but it provides higher security.


## Fundamental Signal Properties.
Signals are the electric or electromagnetic impulses used to encode and trasnmit data.

Analog Signals:
- Is a continous waveform, like sound or voice. (mf both are sounds)

<img src="../../assets/05_analog.png" width="30%" height="30%">

Digital Signals:
- Is a discrete or non-continious waveform. (Series of 0s and 1s)
- The signal can only appear in a fixed number of forms.

<img src="../../assets/05_digital.png" width="30%" height="30%">

Signals have three components:
- Amplitude
- Frequency
- Phase

### Amplitude
The amplitude is the height of the wave above or below a given reference point.
Amplitude is usually measured in volts.

<img src="../../assets/05_amplitude.png" width="30%" height="30%">

### Frequency
Frequency is the number of times a signal makes a complete cycle within a given time frame
The frequency is measured in hertz(Hz) or in Period(Cycles per second). Period = 1/frequency.

The Spectrum is the range of frequencies that a signal spans from minimum to maximum

Bandwidth is the absolute value of the difference between the lowest and highest frequencies of a signal.

Example: The average voice has a frequency range of roughly 300hz to 3100hz. The spectrum is equal to 300hz-3100hz
and the bandwidth would be 2800hz

<img src="../../assets/05_frequency.png" width="30%" height="30%">

### Phase
- The position of the waveform relative to a given moment of time or relative to zero time. So I'm guessing like the value of Y.
- A change in a phase can be any number of angles between 0 and 360 degrees.
- Phase changes often occur on common angles such as 45, 90, 135, etc.

<img src="../../assets/05_phase.png" width="30%" height="30%">

## Baud Rate
Serial communications are done at a predefined speed.
- This speed is called baud rate.
- It refers to the number of bits transmitted per second
- The most frequently used baud rate is 9600bps
- Lower baud rate, i.e., 1200, 2400, 4800 or high baud rate, i.e., 19200, 38400, 57600, 115200 can be used depending on the speed that can be handled reliable by the device.
- The sender and the receiver need to handle the data at the same baud rate.

### Noise
Noise is unwanted electrical or electromagnetic energy that degrades the quality of signals.

If too much noise is present in a communcation line, the signal could be lost or corrupted.
Communications systems should chbeck for transmission errors and poerform some action.
Some systems perform no error control and just discard the data.

### White noise
- Also known as thermal or Gaussian(Linear algebruh ptsd) noise.
- Relatively constant and can be reduced.
- If white noise gets too strong, it can completely disrupt the signal.
- A common source of white noise is the thermal vibration of atoms in conductors.

<img src="../../assets/ChristianAssets/noise.png" width="30%" height="30%">

### Impulse Noise
- One of the most disruptive forms of noise
- Random spikes of power that can destroy one or more bits of information.
- Difficult to remove from an analog signal because it may be hard to distinguish from the original signal.
- Impulse noise can damage more bits if the bits are closer together(transmitted at fast rate).

<img src="../../assets/ChristianAssets/noise.png" width="30%" height="30%">

### Crosstalk
Unwated coupling between two different signal paths.
- For example hearing another conversation while talking on the telephone.
- Relatively constant and can be reduced with proper measures.

<img src="../../assets/ChristianAssets/cross_talk.png" width="30%" height="30%">

### Echo
A signal bouncing off at the end of the cable creating echo.

<img src="../../assets/ChristianAssets/echo.png" width="30%" height="30%">

### Jitter
- The result of small timing irregularities during the transmission of digital signals.
- Occurs when a digital signal is repeated over and over
- If serious enough, jitter forces systems to slow down their transmission.

<img src="../../assets/ChristianAssets/jitter.png" width="30%" height="30%">

### Other categories of noise
- Delay Distortion: Occurs because the velocity of propagation of a signal through a medium varies with the frequency of the signal.
- Attenuation: The continuous loss of a signal's strength as it travels through a medium.

For Noise, I don't think he'll ask all this but the things he specifically pointed out in the review were:
- Need to be able to give examples of noise. So I'm guessing like Crosstalk, jitter, echo, whitenoise all that.
- Name a possible source of white noise: The thermal vibrations of atoms.

### Difference between Analog and Digital signals
Analog signals are a continuos wave while Digital signal is a sequence of 1s and 0s.

Analog is rarely used today because it's harder to separate noise from Analog signal than from digital signal.

This might not be in the exam but it's not a bad thing to know!
Devices that use Analog Signals
- Telegraph
- Answering machine
- Fax machine

Devices that use Digital Signals:
- Smartphones
- Computers
- CDs

## Header that gets placed on packet at each layer of OSI or TCP/IP reference models

<img src="../../assets/ChristianAssets/packet_headers.png" width="50%" height="50%">

Made on paint by yours truly btw.

I'm not sure if he will ask what's inside the headers, and I can't find many of the info but I'll try to put the ones I feel are most important.

Ethernet Header:
- Preamble: Used to synchronize the exchange of data
- Destination MAC
- Source MAC
- Payload
- CRC: Cyclic Redundancy Check
- DOES NOT CONTAIN AN IP HEADER

IP header:
- Checksum
- Source IP
- Destination IP
- Data

## IPv4 and IPv6 Addresses

### IPv4:
- Is given by Dynamic Host Configurate Protocol(DHCP).
- 32 bits(4 bytes)
- Four seperate 8bit numbers(octects)
- Octets separated by periods
- Octet value is between 0 and 255
- IPv4 Can be classful or classless

Network address translation(NAT) is a process that enables us to use an unique PUBLIC IP address to represent a group of PRIVATE IP addresses in a network. It is used by Routers

The next ranges are PRIVATE address ranges:
- 10.0.0.0 to 10.255.255.255
- 172.16.0.0 to 172.31.255.255
- 192.168.0.0 to 192.168.255.255

Private IP addresses are not routable and are used to allow organizations to assign their own device IP addresses for private networks. This is where NAT is used.

Domain Name System(DNS) is a hierarchical naming system that allows organizations to associate host names with IP address name spaces.
Example: Amazon.com -> 72.21.211.176.

### IPv6:
- 128 bits
- Eight groups of 4 hexadecimal numbers
- First 64 bits identify network
- Last 64 bits identify host(Based on MAC address)

Leading 0s in address can be dropped.
Example: 2001:0db8:0000:0000:0000:0053:0000:0004 -> 2001:db8:0:0:0:53:0:4

Group of 0s can be deleted.
Example: 2001:0db8:0000:0000:0000:0053:0000:0004 -> 2001:db8::53:0:4

idk if I should put all the classes bullshit and subnet masks stuff.

## Different message casting methods

### Broadcast
Broadcast is sending message to all devices within range.

### Multicast
Multicasting is sending messages to subscribed users, such as television cable.

### Unicast
Unicast is sending a message one to one.

### Anycast
Anycast is sending a packet to the nearest node in a specified group of nodes.

## Encoding schemes

### ASCII
American Standard Code For Information Interchange(ASCII)
- Character encoding standard.
- uses 7-bit codes.
- 8th bit is unused or used for parity check.
- 128 codes (2^7).
- Two general types of codes: 95 are printable characters, 33 are non-printable control characters (used to control console or communications channel features).

It has two limitations.
- Missing cent symbol (my boy carlton always thinking that mula bro. I get him doe, you need cents to make dollars).
- Limited on the number of characters represented

### Extended ASCII
An encoding scheme to provide additional characters from the extra bit added to the already existing 7-bit ASCII code.
- Set 1 and set 15 used to represent most western european symbols
- Set 2 represent eastern european symbols (Shout out to my kurwa mac and Cyka Blyat people). Actually nvm polish people use normal characters lmao.
- Inability to reliably interchange document between computers using different code pages.
- Different characters encoding using 8-bits.
- Identical only in the first 128 codes (ASCII PART).
- While better than ASCII, this is not sufficient to handle asian languages that have thousands of symbols. Asian moment.

### Unicode
A character coding system desgined to support processing and display of texts from diverse languages.
- Each character  symbol is presented by a code point.
- A code point is a theoretical concept; It does not represent how the chracter is stored in memory.
- Code point representation in memory is dependent on the encoding scheme (UTF-8, UTF-16).
- A continuous group of 65,536(2^16) code points is called a plane. REMEMBER THE 2^16, HE SPECIFICALLY ASKED THIS IN THE REVIEW.
- There are 17 planes.
- Plane 0 is referred to as the basic multilingual plane (BMP) and it is intended for modern use.

### UTF-16 encoding
- All characters in the modern languages are represented with a two-byte code
- A Unicode byte order mark determines the order in which the bytes are written
- Big Endian defines the byte order in memory where the most significant byte is first
- Little Endian defines the byte order in memory where least significant byte is first

### UTF-8 encoding
- One byte for standard english letters and symbols.
- Two bytes for additional Latin and Middle Eastern characters.
- Three bytes for Asian characters.
- Any other characters can be represented with four bytes.
- UTF-8 is backwards compatible with ASCII, since the first 128 characters are mapped to the same values

If I'm being honest, there is no shot the guy asks us for all of this. In the review session the specific points he said we're
- Need to know how many code points in a plane: 2^16.
- How many bits are used for ASCII: 7-bits
- How many bits are used for Extended ASCII: 8-bits.
- How many bits are used for UTF-16: always 2 bytes.

So might as well add that 
- UTF-8 uses one byte for english letters, 2 for latin and middle eastern, and 3 for asian ones.

## Data Compression
Compression is a technique used to squeeze more data over a communications line or into a storage space.

Two groups of compression:
- Lossless, when data is uncompressed, original data returns.
- Lossy, when data in uncompressed, you do not have the complete original data.

If you want to compress financial files: Use lossless.
If you want to compress video image, movies or audio files, lossy is ok.

Examples of lossless compression: Huffman codes, run-length compression, Lempel-ziv Compression, and FLAC.
Examples of lossy compression: MPEG, JPEG, and mp3

Audio and video files do not compress well using lossless techniques but we can take advantage of the fact that the human ear and eye can be tricked
into hearing and seeing things that aren't really there. 

Most audio is now compressed using mp3 compression.
Images are mostly compressed with JPEG.

## Error detection schemes
Despite the best prevention techniques, errors may still happen.
To detect an error, something extra must be added to the data/signal.
- This extra is an error detection code.

Three basic techniques for detecting errors:
- Parity Checking
- Arithmetic Checksum
- Cyclic Redundancy Checksum

### Simple Parity Check
If performing even parity, add a parity bit such that an even number of 1s are maintained. 
If performing odd parity, add a parity bit such that an odd number of 1s are maintained.
For example, if 1001010 is to sent using even parity, 11001010 is sent.

What happens if the character 10010101 is sent and the first two zeroes accidentally become two ones?
- The message received becomes 11110101.
- Simple parity only detects odd numbers of bits in errors

Basically, the 2 devices communicating will choose wether they use an odd or even parity check.
Say they agree on an odd parity check and want to send 10110110. The number of ones is odd so it all checks out.
But what if during transmission the last 2 bits interchange places and the data becomes 10110101.
Fundamentally the data is no longer the same, but the number of 1s is still odd, so the protocol will not detect the error.

### Longitudinal parity
- Adds a parity bit to each character then adds a row of parity bits after a block of characters.
- The row of parity bits is actually a parity bit for each "column" of characters.
- The row of parity bits plus the column parity bits add a great amount of redundancy to a block of characters.

For this longitudinal parity, the data blocks are put one on top of the other in a table with columns.
The parity row is determined by the number of 1's in the column.
If a column has an odd number of 1s, we put 1, if the column has an even number of 1s, we put 0.

The computer gets sent the data blocks and the LRC (the row with the parity bits). It then computers the LRC with the data it received and checks
if the one he calculated is the same one as the one he received. If they are equal, the data is correct.

<img src="../../assets/ChristianAssets/longitudinal_parity.png" width="50%" height="50%">

It still doesn't detect all errors. If you look in the example below the 2nd column has 2 bits changed and the 3rd column has also 2 bitch changed.
The data is once again changed during transmission, but when calculating the parity bits of the LRC, the number of 1s is still even in the 2nd column
and odd in the 3rd row, hence the parity row stays the same and the program doesn't detect an error.

<img src="../../assets/ChristianAssets/longitudinal_parity_error.png" width="50%" height="50%">

Both Simple and longitudinal parity do not catch all the errors
- Simple parity only catches odd number of bit errors
- Longitudinal parity is better at catching errors but requires too many check bits added to a block of data.
- We need a better error detection method, What about arithmetic checksum.

### Arithmetic checksum
- Used in TCP and IP on the Internet.
- Characters to be transmitted are converted to numeric form and summed.
- Sum is placed in some form at the end of the transmission.

I don't understand carlton's "Simplified" example and since he didn't explained it deeply I don't think we will be asked, in any case it's better to know more
than not enough!

but basically to get the checksum of a packet, you need to separate the packet into k packs of n bits.
So say you have a 32-bit packet you separate the data into 4 packs(k) of 8 bits(n).
Then you add each binary column togheter that creates the parity row.
And to get the checksum you do a 1s complement on the parity row. So changing the 1s to 0s and 0s to 1s.

Checksum is used by IP and TCP headers


### Cyclic Redundancy Checksum
- Crc error detection method treats the packet of data to be transmitted as a large polynoimial
- Transmitter takes the message polynomial and using polynomial arithmetic, divides it by a given generating polynomial.
- Quotient is discarded but the remainder is attached to the end of the message.

- The message is transmitted to the receiver.
- The receiver divides the message and remainder by the same generating polynomial.
- If a remainder not equal to zero results, there was an error during transmission.
- If a remainder of zero results, there was no error during transmission.

The ethernet packet header uses CRC.

Now CRC is pretty hard to understand just from the slides alone. If you want, you can check out theses videos: https://www.youtube.com/watch?v=A9g6rTMblz4. & https://www.youtube.com/watch?v=wQGwfBS3gpk
I will try to explain it in my own words. However, do not quote me on this as it is my own understanding.

- First, the sender takes the message polynomial(data) and devides it by a pre-generated polynomial(divisor).
- Then, the sender performs a binary division with the data and the divisor.
- The remainder of that division is the CRC code.
- The sender then sends the data with the CRC code appended to it.
- The receiver then does a division using the data he received (data+crc) and devides it by the divisior(the divisor is chosen by both devices before communcation begins and both machines have the same divisor).
- If the remainder of that division is all 0s, that means that there were no errors during transmission.


### Know how to go from Hexadecimal to binary
This was explicitely stated by Carlton during the review session.

So lets remember in hex
- 0 is 0
- 1 is 1
- 2 is 2
- ...
- 9 is 9
- 10 is A
- 11 is B
- 12 is C
- 13 is D
- 14 is E
- 15 is F

Example: Say we have the hexadecimal number C4.
- C is equal to 12 which in binary is 1100.
- 4 is equal to 0100 in binary.
- C4(Hex) = 11000100(binary) = 196(decimal).

Example2: Say we have the Hexadecimal number 3b7.
- 3 is equal to 0011 in binary.
- B is equal to 11 which is 1011 in binary.
- 7 is equal to 0111 in binary.
- 3B7(Hex) = 001110110111(Binary) = 951(Decimal).

Carlton gave an example of a short answer question that he could give for parity checking.
Say that we want to transfer the star symbol. The star's value is 2b and we're using odd parity check(or whatever he asks us), first we need to convert 2b(hex) to binary.
Each hex value has 4 bits. 2b = 0101011(remove first significant bit because ascii uses 7 bits.) Since we're using odd parity, there has to be an odd number of 1 bits. Therefore,
we have to add 1 parity bit becoming 10101011. If we're using an even parity, we would just set the parity bit to 0, becoming 00101011.

## Wireless technology
The two primary types of wireless network connections used are Cellular and Wi-Fi.

- Radio, Satellite transmissions and infrared light are all different forms of electromagnetic waves that are used to transmit data.
- Technically speaking, in wireless transmissions, space is the medium.

### Terrestrial Microwave Transmission
- Land-based, line-of-sight transmission
- Approximately 20-30 miles between towers (mf who uses miles its 32-48km)
- Transmits data at hundreds of millions of bits per second.
- Signals will not pass through solid objects
- Popular with telephone companies and business to business transmissions

<img src="../../assets/ChristianAssets/microwave_tower.png" width="30%" height="30%">

<img src="../../assets/ChristianAssets/microwave_tower_receiver.png" width="50%" height="50%">

### Satellite Microwave Transmission
Similar to Terrestrial Microwave except the signal travels from a ground station on earth to a satellite and back to another ground station.
- Can also transmit signals from one satellite to another.
- Satellites can be classified by how far out into the orbit each one is (LEO, MEO, GEO, AND HEO).
- LEO(Low-Earth-Orbit) - 100 to 1000 miles out: Used for wireless e-mail, special mobile telephones, pagers, spying, videoconferencing.
- MEO(Middle-Earth-Orbit) - 1000 to 22300 miles: Used for GPS and Government.
- GEO(Geosynchronus-Earth-Orbit) 22300 miles: Always over the same position on earth(and always over the equator), used for weather, television, governement operations.
- HEO(Highly Elliptical Earth Orbit) - Satellite follows an elliptical orbit: Used by military for spying and by scientific organizations for photographing celestial bodies.

<img src="../../assets/ChristianAssets/orbits.png" width="50%" height="50%">

Satellite Microwave can also be classified by its configuration.
- Bulk Carrier configuration.
- Multiplexed Configuration.
- Single-user earth station configuration.

<img src="../../assets/ChristianAssets/satelliteconfigs.png" width="50%" height="50%">

### WinMax - Broadband wireless system
Delivers internet services into homes, businesses and mobile devices.
- Designed to bypass the local loop telephone line.
- Transmits voice, data, and video over high frequency radio signals.
- Maximum range of 20-30 miles.
- Theoretical speed of 128 mbps.

<img src="../../assets/ChristianAssets/WinMax.png" width="50%" height="50%">

### Cellular telehpones
- Wireless telehpone service, also called mobile telephone, cell phone and PCS.
- To support multiple users in a metropolitan area, the market is broken into cells
- Each cell has its own transmission tower and set of assignable channels

<img src="../../assets/ChristianAssets/cellulartowers.png" width="30%" height="30%">

<img src="../../assets/ChristianAssets/cellularphones.png" width="30%" height="30%">

Placing a call on a cell phone:
- You enter a phone number and press send.
- Your cell phone contacts the nearest cell tower and grabs a set-up channel.
- Your mobile identification information is exchanged to make sure you are a current subscriber.
- If you are, you are dynamically assigned two channels: One for talking, and one for listening; the call is placed and you talk to your significant other or mama.

Receiving a call on a cell phone:
- Whenever a cell phone is on, it "pings" the nearest cell tower every several seconds, exchanging mobile ID information; this way, the cell phone system knows where each cell phone is.
- When someone calls your cell phone number, since the cell phone system knows what cell you are in, the tower "calls" your cell phone.

Remember the pinging part with towers, he specifically asked for it in the review session.

For the wireless technologies part, Carlton said that he wanted to know the different technologies and their range.
So to recapitulate:
- Terrestrial Microwave Transmission, range: 20-30 miles.
- Sattelite microwave Transmission, range: look at satelite distances.
- WinMax, range: 20-30 miles.

## Cellular Telephone Technologies

### 5G
New radio multiplexing technology

- Utilizes more efficient spectrum usage techniques
- New spectrum
- Allows up to 10-20 Gbits/s data rate (mega fast eh)
- Expected to provide support for immersive user interfaces, such as AR/VR, mission-critical applications.

### 5G use cases
- Control of home appliances, industrial robots and even self driving cars.
- Provisioning a large number of autonomous devices working together to provide service to end users.
- Allows interconnection of large number of Internet-of-Things.

### Bluetooth
Bluetooth is a specification for short range, point-to-point or point-to-multipoint voice and data transfer.
- Bluetooth can transmit through solid, non-metal objects.
- Its typical link range is from 10cm to 10m, but can be extended to 100m by increasing the power.
- Bluetooth enables users to connect to a wide range of computing and telecommunication devices without the need of connecting cables.
- Typical uses include phones, pagers, LAN access devices, headsets, notebooks and desktop computers. (bought Motherboard for bluetooth the verge moment)

### ZigBee
- Based upon IEEE 802.15.4
- Used for low data transfer rates (20-250 kbps).
- Also uses low power consuption
- Ideal for heating, cooling, security, lightning, and smoke and CO detector systems.
- ZigBee can use a mesh design - A ZigBee enabled device can both accept and then pass on ZigBee signals. 

### Near-Field Communications
- Very close distances or devices touching
- Magnetic induction(such as radio frequency ID) used for transmission of data.
- Commonly used for data transmission between cellphones.

## Wireless Security

Security has 3 tenets:
- Confiudentiality.
- Integrity.
- Availability.

### Confidentiality
Implies data is sensitiver and not all users are authorized to access that data.
- Is a driver for enabling security controls such as role-based access controls, data encryption in transit and at rest, and audit and monitoring.
- Data encryption ensures confidentiality.

### Integrity
- Pertains to the accurate transmission of bits, bytes and frames
- Confirmation that data was not altered during transmission is verified at different layers of the protocol stack. (CRC in ethernet header and Checksum in TCP and IP headers).

### Availability
The system, application and ddata are accesible to the user as needed.
Required for user access to a system:
- Authorized user with valid workstation or laptop device.
- Network Time Protocol(NTP) and Domain Name System(DNS).
- Physical network connection via wired or wireless local area network(LAN).
- Identification, authorization and authentication.
- Role-based access to systems, applications and data.
- Auditing, monitoring and logging of user session.

### Network risks, threats and vulnerabilities

<img src="../../assets/ChristianAssets/security_vulnerabilities.PNG" width="50%" height="50%">

## Malware 
Software whose purpose is malicious:
- Cause damage.
- Steal information. (desjardins moment).
- Steal resources. (Currently mining bitcoin on your pc btw).
- Cause inconvenience.

### Possible effects of Malware
- Display annoying messages (Search up Frisco motherfucking high school that one is funny).
- Slows down your computer.
- Logs keyboard activity to steal passwords.
- Blocks access to specific websites.
- Uses your computer to send spam messages or malware to other computers.
- Gives criminals remote access to your computer.
- Deletes your data.
- Uploads or downloads files on your computer.

Anything can be affected: Pc, phone, Atm, Identification chip on cat.
Types of Malware:
- Virus.
- Worm.
- Trojan Horse.

The different types of malware are not mutually exclusive. Some malware can blend characteristics of several basic types, like a trojan horse that delivers a virus.

### What virus/worm/trojan have in common
- They are all programs
- They need to execute
- Something has to trigger their execution(like, you).
- They find a way to gain security clearnence to:
- Stay in RAM, Write your hard disk, access your network connection.

### Virus
- Like a Word document, a JPEG iamge or a program.
- When you open the legitimate file, the virus automatically executes its instructions to:
- Embed itself into other legitimate files(Self-replication).
- Cause other damage to your machine.
- The file that the virus embeds itself in is called the host file.
- The host file is used by the virus to gain access to privileged resources on your computer like: Large portions of RAM and the right to execute certain kinds of instructions.

### Worm
A standalone program that automatically spreads copies of itself to other computers(Through network, email or other means).
- It gains access to privileged resources by taking advantage of a design flaw in a legitimate.
- Note that, like a virus, it also self-replicates.

### Trojan Horse
A program that is disguised as a harmless program.
- Through looks or advertising, a computer user is tricked into installing/executing the trojan.
- Instead of performing its advertised task, the trojan instead executes malicious instructions.

### Ransomware
Is a malware which can lock your computer, and claim to represent the FBI(or another police force), and demand payment of a fine in order to get access to your computer.
- Is like other malware downloaded as an attachment, or when you click on a link.

### Prevention
Be careful about what you open or download.
- 1 in 500 tweets contain malware.
- e-mail links can contain malware.
- Any free software.
- Install antimalware programs and scan pc frequently.
- Avira, Avast, windows defender.
- Spybot and Anti-Malware.
- Make sure OS is running a firewall.
- Install software updates as soon as they are available.
- Turn off "shared" folders or make them password protected.
- Don't click on any of albin's links.

## Cryptographic schemes

### Encryption
- Confidentiality can be achieved using encryption.
- Practice of hiding messages so that they cannot be read by anyone other than the intended recipient.

### Types of encryption algorithms
Symmetric-key encryption algorithms.
- Algorithms in which the same key is used for both encrpytion and decryption(1 secret key).

<img src="../../assets/ChristianAssets/symmetric.png" width="50%" height="50%">

Asymmetric-key encryption algorithms.
- Use a pair of keys for encryption:
- Public key for encryption.
- Private key for decryption.

Encryption algorithms based on mathematically intractable problem.
Operates much slower than symmetric key algorithms.
Asymmetric-key encryption algorithms are also reffered to as public-key encryption algorithms. (useless term imo, would make much more sense in symmetric lol).

<img src="../../assets/ChristianAssets/asymmetric.png" width="50%" height="50%">

### Types of encryption algorithms
RSA is the most commonly used asymmetric-key encryption algorithm.
- Based on the difficulty of factoring prime numbers.
- Supports variable length key sizes(512,1024,2048,3072).
- Current recommended key size: Minimum 2048 bits.

### Types of symmetric-key algorithms
- Block ciphers: Encrypt data one block at a time, typically block sizes of 64 bits or 128 bits.
- Stream Ciphers: Encrypt data one bit or one byte at a time.

### Block cyphers
Data Encryption Standard (DES).
- Uses a 56-bit key to encode 64-bit blocks of data.
- Insecure due to the small key size.
- Key can be ascertain using exhaustive search within a few minutes.

### TripleDES
- Uses DES 3 times in tandem
- Output is 1 DES input to the next DES
- Is also insecure and should not be used.

### Advanced Encryption Standard (AES)
- Replacement of DES
- 3 Possible key sizes: 128, 192 or 256 bits keys.
- Uses 128 bit block size.

### Stream ciphers

### RC4
- Most widely known stream cipher.
- Used in WEP(Wired equivalent privacy) and WPA (Wifi protected Access) and TLS/SSL.
- Insecure, should no longer be used.

### Encryption Algorithms key strenght
- Strength of cryptographic algorithms is determined by the length of the key in bits.
- Set of posible keys for a cryptographic algorithm is called a key space.
- For a 128-bit key, there are 2^128 possible keys
- To crack the key, one must use brute force attacks(try all keys until a key that works is found).

### Java AES Encryption and Decryption
6 modes of operation for symmetric key crypto schemes; each mode divides plaintext into 128-bit blocks:
- ECB (Electronic Code Book): Each block of the plaintext is encrypted with the same key; thus, it produces the same result for the same block(Not recommended).
- CBC (Cipher Block Chaining): The first block of the plaintext is XOR with Initialization Vector (IV); the block of ciphertext is then XORed with the next block of plaintext.
- CFB (Cipher FeedBack): First the IV is encrypted, the result is then XORed to the first block of plaintext; the result of the last operation is then encrypted, and the next result XORed to the next block of plaintext.
- OFB (Output FeedBack): Similar to CFB, except that it’s the IV that is encrypted each time to be XORed with the blocks of plaintext.
- CTR (Counter Mode): This mode uses a counter as an IV; it’s similar to OFB, except that it is the counter that is encrypted each turn and the result XORed with the blocks of plaintext.
- GCM (Galois/Counter Mode): This mode is an extension of CTR mode; in addition to encrypting it also calculates an authentication tag (at least 128 bits long) that is typically appended to the ciphertext. (Recommended by National Institute of Standard and technology).

### Implementing AES-GCM in Java
AES-GCM is the most widely used authenticcated cipher.
AES-GCM inputs:
- Input data
- AES secret key(128,192,256 bits).
- 12 bytes IV.
- Lengths in bits of authenticated tag (128 bits).

### Message authentication and integrity
The cryptographic schemes we discussed in previous classes provide secrecy or confidentiality, but not integrity.

For integrity check, one of the following cryptographic scheme is required:
- Hash function.
- Message Authentication Code(MAC).
- Digital Signature.

### Hash function 
- A cryptographic hash function is an algorithm that takes data or message of any length and produces a fix length output called a message digest or fingerprint.
- If a hash function is to be useful, it must be strongly collision-free. (A collision occurs for a hash function when 2 different messages give the same message digest).

<img src="../../assets/ChristianAssets/hash.PNG" width="50%" height="50%">

### Examples of hash functions
MD5:
- produces 128-bit digest
- Collision against md5 can be calculated within seconds; therefore this hash is insecure.

SHA-1(secure hash Algorithm 1):
- Produces 160-bit digest.
- Collision agaisnt sha-1 can be calculated within a relative short time period, therefore sha-1 is considered insecure.

RIPEMD-160(Race integrity  Primitives Evaluation Message Digest):
- Producest 160-bit digest.

SHA-2 (Secure Hash Algorithm 2):
- 2 hash algorithms classified as SHA-2: SHA-256 and SHA-512

SHA-3:
- Producest the same output sizes as SHA-2.

### Message Authentification Code(MAC)
- Similar to message digest produced by hash functions, except that a secret key is required to produce the MAC.
- HMAC(Hash based MAC) is the algorithm that's used along with a secret key, to produce message authentication code.
- Any cryptographic hash function can be used to construct a HMAC.
- Examples of HMAC: HMAC-SHA-256, HMAC-RIPEMD-160.

<img src="../../assets/ChristianAssets/hash_mac.PNG" width="50%" height="50%">

### Password Hashing
Argon2 won the passowrd hashing competition in 2015.
- Thus recommended as the algorithm to be used for password hashing

Regular hash functions not suitable for password hashing for the following reasons:
- Availability of faster computers makes it easier for attackers to discover passwords using dictionary attack.
- Processor speed and CPU cores will continue to increase.

Solution: A solution for this problem is to use hashing algorithms whose speed can be slowed down.
- If the time it takes to produce password hash increases significantly, the probability of attackers discovering passwords via brute force attack decreases significantly.

Password hashing functions thus need to have configurable hash-time parameter:
- Argon2 is currently the recommended choice of hash functions that satisfied this requirement.

### Argon2 Algorithm

Argon2 accepts the following 5 parameters:
- Salt length: Length of random salt, recommends 16 bytes.
- Hash length: Lenght of the generated hash, recommends 16 or 32 bytes.
- Iterations: Number of iterations, affect the time cost.
- Parallelism: Number of threads(or lanes) used by the algorithm, affect the degree of parallelism

- Argon2d: Maximizes resistance to GPU cracking attacks, suitable for Cryptocurrency.
- Argon2i: Optimized to resist side-channel attacks, suitable for password hashing.
- Argon2id: Hybrid Version

## Digital signatures
Digital signature Schemes:
- Uses a pair of private-public keys and consist of a signing algorithm and a verification algorithm.
- The signing algorithm uses a private key to generate the signature.
- The verification algoriuthm uses the signer's public key to decrypt the signature.

<img src="../../assets/07_digital_signature.png" width="50%" height="50%">

### To reduce the size of digital signatures, the message to be signed is first hashed using a cryptographic hash function, then the message digest is signed.

<img src="../../assets/07_hash_sign.png" width="50%" height="50%">

Examples of digital signature schemes:
- RSA
- DSA
- The recommended key length foir both RSA and DSA signature schemes is at least 2048 bits.


## Digital Certificates
- An electronic data that binds a public key to an individual, organization, system or entity.

### Certificate authority(CA)
- A trusted entity that creates, signs and manage digital certificates.
- A CA is required to sign a digital certificate to attest to its validity.

<img src="../../assets/ChristianAssets/digital_cert.PNG" width="50%" height="50%">


## Commonly Used authentication schemes
- Passwords
- Digital Certificates
- Radio-frequency identification cards
- Biometrics

## TLS PROTOCOL

The transport layer security(TLS) is the protocol used by HTTPs to encrypt connection between browsers and web servers.
Basically the client and server get together to chose a symmetric key used to encrypt the data passed between them through the channel.
it is necessary to have the session key changed every session to remain secure.

## WAN networks
Wide area network:
- Connectivity between multiple locations via the internet. 

### WAN technologies in the 2000s

### Synchronus Optical Network(SONET)
- A standard for high-speed optical fiber backbone networks.
- Synchronus digital transmission.
- Deployed by service providers in a ring topology.

<img src="../../assets/ChristianAssets/sonet.jpg" width="50%" height="50%">

### Multiprotocol Label Switching (MPLS)
- MPLS is a packat switching technology that operaates at layer 2; is independent of routing tables and any routing protocol.
-  Service providers adopted MPLS switches for use as enterprise WAN solutions. MPLS allows IP traffic to flow in a full or partial mesh configuration. It supports Quality of Service (QoS) and Class of Service (CoS), allowing IP traffic to be prioritized through the network. MPLS is ideal for Voice over Internet Protocol (VoIP), streaming, and time-sensitive applications. (stolen from zach but this mf just copy pasted the book. Mf tell me right now if you understand any of this).

In a more basic way, when a packet is sent to a network, MPLS gives it a class of service, an MPLS header, which is basically like a label made out by a short sequence of bits.
These labels can be chosen by the company and could use classes like real time (voice or video), mission critical(crm) or best effort (internet, email).
The network then prioritises the packets depending on the label(CoS) that it has. The fastest, low-latency path would be reserved for real-time apps like voice and video, thereby ensuring quality is high, while packets with best effort like internet or email are put in one of the lower quality routes, *because I'm guessing the can be re-sent multiple times.* don't quote me on that, it just what's logical. MPLS Allows us to use VPNs and allows IP traffic to flow in a full or partial mesh configuration. and then stores the paths used for sending a packet in a table of Indoubt interface/Path information pairs.

### Label Switched Paths
- Are unidirectional routes through a network or autonomous system (AS).
- In normal IP routing, the packet has no predetermined path.
- Instead each router forwards a packt to the next hop address stored in it's forwarding table, based only on the packet's destination address.

In contrast, MPLS routers within an AS determine paths through a network through the exchange of MPLS traffic engineering. So basically instead of calculating the next hop like IP routing, MPLS checks their path information table and sends the packet to the next predetermined hop address.

### Software-Defined Networking (SDN):
- A network in which the control plane is physically separate from the forwarding plane, and a single controls several forwarding devices.
- Its an emerging architecture that is dynamic, adoptable and cost-effective.
- This architecture separates the network control and forwarding functions; thus, enabling the network control to become programmable.
- SD-WAN has become a popular alternative to MPLS












