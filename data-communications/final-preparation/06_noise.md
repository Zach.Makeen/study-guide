---
title: 'Noise & Errors'
---

Noise is always present. If a communications line experiences too much noise, the signal will be lost or corrupted. Communication systems should check for transmission errors. Once an error is detected, a system may perform some action. Some systems perform no error control, but simply let the data in error be discarded

## White Noise

Also known as thermal or Gaussian noise. Relatively constant and can be reduced. If white noise gets too strong, it can completely disrupt the signal.

A common source of white noise:
- The thermal vibration of atoms in conductors

<img src="../assets/06_white_noise.png" width="60%" height="60%">  
<br />
<br />

## Impulse Noise

One of the most disruptive forms of noise. Random spikes of power that can destroy one or more bits of information. Difficult to remove from an analog signal because it may be hard to distinguish from the original signal. Impulse noise can damage more bits if the bits are closer together (transmitted at a faster rate).

<img src="../assets/06_impulse_noise1.png" width="60%" height="60%">  
<br />
<br />

<img src="../assets/06_impulse_noise2.png" width="60%" height="60%">

## Crosstalk

Unwanted coupling between two different signal paths. For example, hearing another conversation while talking on the telephone. Relatively constant and can be reduced with proper measures.

<img src="../assets/06_crosstalk.png" width="60%" height="60%">  
<br />
<br />

<img src="../assets/06_echo.png" width="60%" height="60%">

## Jitter

The result of small timing irregularities during the transmission of digital signals. Occurs when a digital signal is repeated over and over. If serious enough, jitter forces systems to slow down their transmission.

<img src="../assets/06_jitter.png" width="60%" height="60%">

## Other categories of noise

### Delay Distortion

Occurs because the velocity of propagation of a signal through a medium varies with the frequency of the signal.

### Attenuation

The continuous loss of a signal’s strength as it travels through a medium.
