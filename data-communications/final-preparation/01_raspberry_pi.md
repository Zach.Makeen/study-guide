---
title:  'Raspberry Pi'
---

## Pin reference

Each of the 40 GPIO pins has specific function, and most can be used as either input or output depending on your program or its function.

- Power
  - The power pins are used to power components such as sensors or small actuators.
  - 3v3 Power pins are located on the physical numbers 1 & 17
  - 5v Power pins are physically located on numbers 2 & 4
- SPI
  - Synchronous serial bus commonly used for sending data between micro controllers and small external peripheral devices.
  - Multiple peripheral devices can be interfaced as SPI slaves and be controlled with the SPI master.
  - SPI uses separate clock (SCK) and data lines (MISO, MOSI), along with a select line to choose among slave devices.
  - SPI0 pins are GPIO 7, 8, 9, 10, 11 and are physically located on numbers 19, 21, 23, 24, 26.
- I2C
  - I2C interface allows multiple slave integrated circuits to communicate with one or more master.
  - I2C uses two directional lines: Serial Data Line (SDA) and Serial Clock Line (SCL)
  - SPI needs 4 pins for communication; whereas I2C requires only 2 to connect any number of slaves.
  - SPI is good for high rate full-duplex connections.
  - I2C pins are physically located on numbers 3 & 5 and 27 & 28
- Ground
  - The ground is very useful for making a common reference between all components in your circuit.
  - Ground pins are physically located on numbers 6, 9, 14, 20, 25, 30, 34, 39

[source](https://pinout.xyz/)
