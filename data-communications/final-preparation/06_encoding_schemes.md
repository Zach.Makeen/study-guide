---
title: 'Data Encoding Schemes'
---

## ASCII

ASCII refers as The American Standard Code for Information Interchange. It is a character encoding standard that uses 7-bit codes. The 8<sup>th</sup> bit is most of the time unused unless it is for a parity bit. There is 128 codes (2<sup>7</sup>) and two general types of codes. The first type is the 95 "printable characters" code that are displayable on a console & the second type is the 33 "non-printable characters" codes
which is for control features/communication channel.

### Control Codes: 
- 00<sub>16</sub> - 1F<sub>16</sub> and 7F<sub>16</sub>
- Mostly used in telecomm, to control the transmission of data between a terminal and a host
- Used to control the presentation of data on a terminal
### Display Codes:
- 20<sub>16</sub> - 7E<sub>16</sub>
- Includes space, punctuation marks and special symbols. Upper and lower case letters and num

This data encoding scheme has limitations, such as the number of characters represented and it is missing the cent symbol.


## Extended ASCII

It is the ISO 8859 (International Organization for Standards #8859). It has been created to provide additional charactes to the already existing ASCII scheme. 
There are several sets to represent missing symbols. E.g.: Set 10 (ISO 8859-10) which is used to represent Nordic/Eskimo symbols.

This data encoding scheme has limitations, such as the inability to reliably interchange document between computers using different code pages (different characters encoding using 8-bits && identical only in the first 128 codes) and it is not sufficient to handle Asian languages that have 1000+ of symbols.


## Unicode

This character coding system was designed in order to support processing and display texts from diverse languages. Each character is represented by a code point, but it is a theoretical concept as it does not represent how the character is stored in memory. The representaion of Code point in memory is dependent on the encoding scheme (UTF-8 && UTF-16). The number of code points that can be represented is 1,114,112 (0<sub>hex</sub> to 10FFFF<sub>hex</sub>).
2<sup>16</sup> code points are referred as plane and there is 17 planes.

**Big Endian**: The byte order in memory is the most significant byte FIRST
E.g.: UTF-16BE -> "Hello" = 00 48 00 65 00 6C 00 6C 00 6F && Order mark is FE FF

**Little Endian**: The byte order in memory is the most significant byte LAST
E.g.: UTF-16LE -> "Hello" = 48 00 65 00 6C 00 6C 00 6F 00 && Order mark is FF FE

### UTF-8 Encoding
It is the most popular type of Unicode encoding. It uses **1** byte for standard English, **2** bytes for Latin/Middle Eastern characters, **3** bytes for Asian characters and **4** bytes for the rest.
This encoding is byte-oriented (there is no big/little endian). The byte order mark is EF BB BF.

[Unicode UTF-8 Table](https://www.utf8-chartable.de/unicode-utf8-table.pl)