---
title:  'Networks'
---

## Network Topology Overview

A computer network is how computers, servers, printers, and users share information and communicate with one another. A network-attachable device (sometimes simply known as a node) can physically connect to the network and has an assigned IP host address. The layout of how devices connect to a network is the **network topology**. The network topology is a map of the network that shows how devices connect to one another and how they use a connection medium to communicate.

There are two ways to consider a network topology. The
**physical topology** is the picture of the actual network devices and the medium the devices use to connect to the network.

<img src="../assets/02_physical_topology.png" width="60%" height="60%">  
<br />
<br />

The **logical topology** is how the actual network works and how you transfer data in a network. This topology view focuses more on how the network topology operates.

<img src="../assets/02_logical_topology.png" width="60%" height="60%">  
<br />
<br />

Most networks are made up of one or more of the following topologies:

- Point-to-point: A direct link or connection between two devices
- Ring: A shared ring connecting multiple devices together
- Star: A star-wired connection aggregation point, typically from a wiring closet
- Mesh: Multipoint connections and direct links between network devices
- Bus: A shared network transmission medium allowing only one device to communicate at a single time
- Hybrid/star-wired bus: Also known as a tree topology; a shared, star-wired connection aggregation point to a LAN switch

<img src="../assets/02_common_topologies.png" width="60%" height="60%">

### Point-to-Point

A network that consists of computers or devices that connect directly to one another is called a **point-to-point** network. Point-to-point communications are based on time slots and polling where communication controllers poll each dumb terminal one at a time to provide a time slot to transmit data to and from the mainframe computer. This type of network is difficult to scale. That means that it’s difficult to add many devices to the network, because each new device requires some new type of direct connection.

<img src="../assets/02_point_to_point.png" width="60%" height="60%">

### Bus

A **bus topology** starts with a coaxial cable that can support high-speed network communications but is subject to physical distance limitations and maximum number of devices per bus or Ethernet LAN segment. The main cable runs throughout the organization’s physical space and provides accessible connections anywhere along its length. Connecting to a bus network is easy. Any network device can attach an Ethernet transceiver to the bus, allowing for physical connectivity to the network-attached device.

<img src="../assets/02_bus.png" width="60%" height="60%">  
<br />
<br />

The flexibility of a bus does come with a performance cost. Communications signaling traverses in both directions where the transceiver connects to the bus or coaxial cable. Because of this, only one device can communicate at a given time. Each network device’s transceiver must listen on the network (coaxial cable) to see if it’s available before transmitting. When two devices transmit at the same time, a collision occurs on the network, requiring both devices to retransmit again when the network is available. This led to the creation of the IEEE 802 committees focusing on both IEEE 802.3 CSMA/CD (Carrier Sense Multiple Access with Collision Detection).

### Ring

Within **token ring**, all stations are connected in a logical ring while being star-wired from a centrally located wiring closet. Token ring–attached devices must have permission to transmit on the network. This permission is granted via a token that circulates around the ring.

Token ring networks, as shown in FIGURE 3-16, move a small frame called a token. Possession of the token grants the right to transmit. If the network device that receives the available token has no information to send, it alters one bit of the token, which flags it as available for the next network attached device in line. When a network device wants to send information, it must wait for an available token, transmit information to the next upstream device, wait for the destination device to receive the data, and then a new token is released. Token ring– attached devices can transmit only when they have an available token. This eliminates communication collisions. A network collision occurs when two or more computers transmit at the same time on a bus topology. Forcing all communications in a unidirectional manner eliminates network transmission collisions

<img src="../assets/02_ring.png" width="60%" height="60%">

### Star

Network-attached devices used this **star-wired cabling** to physically and electrically connect to an Ethernet or token ring network. This was the beginning of star-wired topology deployments for both Ethernet and token ring networks.

<img src="../assets/02_star.png" width="60%" height="60%">  
<br />
<br />

For Ethernet or IEEE 802.3 CSMA/CD networks, star wiring was used to provide a physical star, logical bus for the original IEEE 802.3i, 10Base-T standard and specification. The logical bus can be supported by either a hub or a switch. This hub or switch is typically installed in a centrally located wiring closet. Networks use two common types of devices to build star networks. A **network hub** is a very simple hardware device that has ports for multiple connections and echoes every message it receives to all connected ports. A hub operates at the Physical Layer and does not make any decisions about message routing—it simply echoes all input. This simple design makes hubs easy and cheap to make. It also means that every node on the network receives messages it doesn’t care about.

The other common star network device is the **switch**. A switch looks like a hub in that it has ports for multiple connections to network devices; however, it’s more sophisticated than a hub. A switch receives a message and examines the destination address. The switch maintains a table of connected devices and knows to which port it should forward the message, so it sends the message directly to the destination. Switches do not send messages to all destinations as hubs do.

<img src="../assets/02_hubs_vs_switches.png" width="60%" height="60%">

### Mesh

A **mesh topology** describes a network layout in which all nodes are directly connected to all other nodes. Actually, there are two different types of mesh topologies, depending on whether all or just most of the nodes are directly connected. Mesh networking is more common in wide area networking, given that alternative paths and redundancy are typically required.

#### Fully Connected Mesh

A **fully connected mesh** follows the strict definition of a mesh topology; that is, every node directly connects with every other node. A fully connected mesh provides a path length of one for every connection. It also provides the maximum number of alternate paths in case any available paths fail. That means fully connected mesh topologies have a high degree of fault tolerance. Fault tolerance describes the ability to encounter a fault, or error, of some type and still be available. A fully connected mesh topology with IP routing can easily bypass a failed connection and still communicate through the mesh using alternate connections and nodes.

<img src="../assets/02_fully_connected.png" width="60%" height="60%">  
<br />
<br />

The biggest drawback of a fully connected mesh topology is the required number of connections.

#### Partially Connected Mesh

A **partially connected mesh** is a network topology in which network nodes connect to only some of the other nodes. Network designers identify the most active or important nodes and ensure those nodes connect to most other nodes. Lower priority nodes do not connect directly. All nodes in the network do connect, but some connections will have a path length greater than one. Partially connected mesh topologies provide more fault tolerance than many other topologies without requiring the expense of a fully connected mesh. This topology can be a good compromise when an organization needs better fault tolerance but cannot afford to set up and maintain a fully connected mesh.

<img src="../assets/02_partially_connected.png" width="60%" height="60%">

### Hybrid

A **hybrid topology** is a network that contains several different topologies. Network nodes differ in traffic and use. Nodes that require high fault tolerance may benefit from a mesh topology. Lower usage networks may work better using a bus topology. A hybrid network that combines the two topologies can provide fault tolerance for some nodes while providing flexibility for other parts of the network.

## Internetworking with Bridges

The easiest way to interconnect two networks is to place a **MAC layer bridge** in between the two networks. A simple network bridge is a device with two network interface cards so that it can connect to both networks. A bridge operates at the OSI Data Link Layer or Layer 2. That means it interacts with the Physical Layer and the Data Link Layer. A bridge can read the Media Access Control (MAC) layer address of each Ethernet frame to make a forwarding or filtering decision.

## Internetworking with Switches

A **switch** is essentially a bridge with more than two ports. A bridge generally connects two networks. A switch often has the ability to connect many devices and networks. Switches operate as an OSI Layer 2 device. They use MAC addresses and build address tables of devices connected to each physical port. Switches that connect multiple devices or networks keep track of MAC addresses on all ports. When the switch sees an Ethernet frame destined for a specific MAC address, it forwards that frame to that port.

There are two kinds of LAN switches. A **Layer 2** switch operates at the Data Link Layer and examines the MAC layer addresses of Ethernet frames. Using this information, filtering and forwarding tasks are performed by the switch.

A **Layer 3** switch operates at either the Data Link Layer or Network Layer. Layer 3 switches typically have software that lets them function like a Layer 2 switch or multiport bridge. Layer 3 switches usually operate at the Network Layer that examines the network layer address within the Ethernet frame.

The IP addresses typically are found in the Network Layer of the packet. An IP address is like a zip code for mailing a letter. It has both a network number and a host number, which is used to route IP packets to the appropriate destination IP network. A Layer 3 switch can look up the destination IP network number in its IP routing table, and then make a path determination decision. A Layer 3 switch provides each device connected to its ports with its own Ethernet LAN segment. Layer 3 switches typically have resiliency or a redundant path connection, also called a redundant circuit, to a building or campus backbone network.

## Internetworking with Routers

A **router** operates at the Network Layer. It is the same thing as a Layer 3 switch, except it’s typically used for wide area network circuit connections, campus backbone connections, and building backbone connections. Routers can make intelligent decisions on where to send packets. Instead of just reading the MAC address and forwarding a packet based on a forwarding table, routers can see the packet’s Network Layer address or IP address. The Network Layer address contains information about the destination network number and host number. In essence, routers perform a path determination calculation to find the best path for the IP packets to traverse.
