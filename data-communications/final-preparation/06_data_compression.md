---
title: 'Data Compression Scheme'
---

## What is Data Compression?

It is a technique used to squeeze more data over a communication luine or into a storage space. It will transfer faster if the file if it is down to 1/2 of its original size.

There are two basic groups of compression:

### Lossless
When data is uncompressed, the complete original data returns.
E.g.: Financial files, Huffman codes, FLAC...

### Lossy

When data is uncompressed, the complete original data is not returned, some of the data is lost. Audio and video files do not compress well using lossless techniques. The human ear and eye can be tricked into hearing and seeing things that aren't really there. 
E.g.: Video image, movie, audio file, MPEG, JPEG, MP3

### Audio Compression

A lot of audio is now compressed, such as on our phones, MP3-like devices.
It is tricky and hard to discribe, an example would be a louder sound may mask a softer sound when both played together and so it will drop the softer sound.
If a person decides to store it in a lossless form, it will take more storage.

### Video Compression 

It does not compress well when using a lossless compresssion because the pixel values in an image are not alike. Frame to frame differences are usually very small.

<img src="../assets/06_videoframe.png" width="60%" height="60%">  
<br />
<br />

### MPEG

Motion Picture Experts Group is a group of people that created a set of standards that can use these small differences between frames to compress video/audio to a fraction of its original size

### JPEG
Joint Photographic Experts Group compresses still images and is a lossy compression.