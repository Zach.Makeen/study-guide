---
title: 'Network Infrastructure Implementation'
---

## Domain Name System (DNS)

The main purpose od dns is to translate hostnames or domain name to IP addresses. For example, the IP address 172.217.13.163 gets translated to google.ca which is easier to remember.

The hierarchical name space organized as an inverted tree structure

<img src="../assets/09_domain_name_space.png" width="60%" height="60%">  
<br />
<br />

A domain name is sequence of labels separated by dots “.”.

- Fully Qualified Domain Name (FQDN): A domain name that specifies its exact location in the tree hierarchy of the entity, e.g., dawsoncollege.qc.ca

<img src="../assets/09_resolution.png" width="60%" height="60%">

## Network Address Translation (NAT)

NAT lets router represent entire local area network to the Internet as single IP address. Thus, all traffic leaving LAN appears as originating from global IP address. All traffic coming into this LAN uses this global IP address. This feature allows a LAN to hide all the workstation IP addresses from the Internet. Since the outside world cannot see into LAN, you do not need to use registered IP addresses inside the LAN.

We can use the following blocks of addresses for private use:

- 10.0.0.0 to 10.255.255.255
- 172.16.0.0 to 172.31.255.255
- 192.168.0.0 to 192.168.255.255

When a user on the inside sends packet to outside, the NAT interface changes the user’s inside address to global IP address. This change is stored in a cache. When the response comes back, the NAT looks in its cache and switches the addresses back. If the address mapping isn’t in the cache, the packet is  dropped unless NAT has a service table of fixed IP address mappings.
