# References

- https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/struct
- https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/operator-overloading
- https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/user-defined-conversion-operators
- https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types/casting-and-type-conversions
