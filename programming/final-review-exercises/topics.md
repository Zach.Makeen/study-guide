# Topics that are not included

- Game loop pattern
- MonoGame framework
- Complex linq queries (lecture 27)
- Iterator pattern
- Generics (lecture 29)

> Library delegates (Func, Action) are included!

# Topics you can expect from the short/long programming questions

- Indexer
- Unit test
- Multi-dimensional/jagged arrays
- Operator overloading
- Decorator pattern
- Composite pattern
- Delegates, lambdas and the strategy pattern
- Events and observer pattern
- LINQ, LINQ-to-XML

> You may be asked to use a specific syntax (fluent/query) in the short answer section not the programming section