using System;
using System.Collections.Generic;

namespace FinalReviewExercises
{
    public delegate void DelAddProduct(Product p);
    public class OnlineStore
    {
        private List<Product> products;
        public event DelAddProduct NewProduct;
        public OnlineStore()
        {
            this.products = new List<Product>();
        }
        public void AddProduct(Product p)
        {
            this.products.Add(p);
            NewProduct?.Invoke(p);
        }
        public void CheckProduct(Product p)
        {
            if (p.Price >= 100 && p.Price <= 500)
                Console.WriteLine(p.Name);
        }
    }
}
