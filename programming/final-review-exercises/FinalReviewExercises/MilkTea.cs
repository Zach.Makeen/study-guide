using System;

namespace FinalReviewExercises
{
    public class MilkTea : TeaDecorator
    {
        public MilkTea(ITea t) : base(t)
        {
        }
        public override string Name { get => base.Name + " with milk"; }
        public override double Price { get => base.Price; }
    }
}
