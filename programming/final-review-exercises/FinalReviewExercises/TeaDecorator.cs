using System;

namespace FinalReviewExercises
{
    public abstract class TeaDecorator : ITea
    {
        protected ITea t;
        public TeaDecorator(ITea tea)
        {
            this.t = tea;
        }
        public virtual string Name { get => this.t.Name; }
        public virtual double Price { get => this.t.Price; }
    }
}
