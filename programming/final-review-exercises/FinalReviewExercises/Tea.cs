using System;

namespace FinalReviewExercises
{
    public class Tea : ITea
    {
        public string Name { get => "Tea"; }
        public double Price { get => 3.75; }
    }
}
