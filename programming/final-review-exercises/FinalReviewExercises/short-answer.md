# Lecture 30 & 31

1. Which of the following is a collection of files that are compiled into an assembly?

> B. A project (.csproj)

2. The following snippet of code does not throw an error in C#. Give reason why.

> C# has a unified type system which means that all types including value types like int inherit from the same base Object class. So all the object class methods like .ToString(), .Equals() are available for values types as well.

3. What is a C# property? How is a property different from an auto-property (or auto-implemented property)?

> Properties provide access to data through a method interface.
> Automatic properties (or auto-properties or auto-implemented propertied) lets you skip the step of creating a backing field.

4. What is the difference between a multi-dimensional array and a jagged array?

> A jagged array is an array of arrays where each array can be of different lengths and stored in different memory locations.
> A multi-dimensional array is stored in a continuous memory block. Every element of a multi-dimensional array is a single element and not an array. Made up of “rows” and “columns”.

5. What does the following snippet of code print?

    int[][] intArray = new int[3][];
    for (int i = 0; i < intArray.Length; i++)
        intArray[i] = new int[2];

    int[,] twoDArray = new int[3,2];
    Console.Write(intArray.Rank);
    Console.Write(twoDArray.Rank);

> 1 2

6. What does the following snippet of code print?

    int[][] intArray = new int[3][];
    for (int i = 0; i < intArray.Length; i++)
        intArray[i] = new int[2];

    int[,] twoDArray = new int[3,2];
    Console.Write(intArray.Length);
    Console.Write(twoDArray.Length);

> 3 6

7. What does the following snippet of code print?

    int[][] intArray = new int[3][];
    for (int i = 0; i < intArray.Length; i++)
        intArray[i] = new int[2];

    int[,] twoDArray = new int[3,2];
    Console.Write(twoDArray.GetLength(1));
    Console.Write(intArray.GetLength(1));

> 2 Throws exception

8. Given the following classes:

class A  {
  public virtual void M1() {
          Console.WriteLine("A.M1");
        }
    }
class B : A {
  public void M1() {
          Console.WriteLine("B.M1");
  }
}

Why does the following print: “A.M1”?

    A ab = new B();
    ab.M1();

> The default behavior in C# is hiding. The override keyword is missing in B’s M1, so C# goes with the compile time type of ab which is A and invokes A’s M1 method. If you want overriding, you must use the override keyword.

9. Suppose there is a struct Point that has a property X.

    public void DoSomething(Point somePoint)
    {
        somePoint.X = 10; 
    }
    //Elsewhere
    Point p = new  Point(2,3);
    DoScomething(p)
    Console.WriteLine(p.X);

> 2 -> Structs are value type!

10. What is difference between a single cast and multi-cast delegate instance? Where is a multi-cast delegate used?

> A single-cast delegate instance has only one method in its invocation list. This is commonly used in the strategy pattern.
> A multi-cast delegate instance has multiple methods in its invocation list. This is mostly used in the observer pattern.

11. What is the difference between an event and a delegate?

> A delegate is a type that defines a method signature and return type. It is not a member of a class and is defined directly within the namespace. You can instantiate a delegate and assign methods that have matching signatures.
> An event is an encapsulated delegate instance. Methods can be added to/removed from its invocation list. It is a member of a class. It can only be invoked within the class where it is declared.

12. Look at the program given and indicate which of the lines marked 1 to 6 are valid or invalid and give reason why

    public class A {
        private void M1() { }
        public void M2() { }
        protected void M3() { }
    }
    public class B : A{
        public void Foo() {
            M1(); //1.
            M2(); //2.
            M3(); //3.
        }
    }
    public class Program {
        public static void Main(){
            A a = new A();
            a.M1(); //4.
            a.M2(); //5.
            a.M3(); //6.
        }
    }

1. Invalid because M1 is private
2. Valid. M2 is public
3. Valid. M3 is protected and can be accessed by the derived class B
4. Invalid. M1 is private
5. Valid. M2 is public and can be accessed on an object of A
6. Invalid. M3 is protected and can only be accessed within a class that extends A

13. Fill the blanks with the most suitable keyword from the following list. (More than one keyword can match a method)

- abstract
- virtual 
- new
- override
- nothing (use this if you feel a blank is not needed here) 

    public abstract class A {
        public virtual void M1(int x) { …. }
        public abstract M2(double y);
        public (nothing)/virtual void M3(string s) { … }
    }
    public class B : A {
        public override/new void M1 (int i) { … }
        public (nothing)/virtual void M2 (int j) { … }
        public override void M2 (double k) { … }
    }

14. For each of the following list of design patterns, give an example of where you would use it. I have done one for you.

Composite design pattern – used when there is a hierarchical/tree-like relationship between objects. I.e. when an object can be composed of itself and similar objects.

Decorator pattern – used when you want to add extra features/functionality dynamically to an object

Strategy pattern – allows an algorithm’s to be selected at runtime

Observer pattern – used when the change of state/action in one object must reflect a change/perform an action in other objects

15. What does the following snippet of code print?

delegate void Action();
class Test {
    static void Main(){
        Action action = CreateAction();
        action();
        action();
    }
    static Action CreateAction() {
        int counter = 2;
        return ( () => { Console.WriteLine(counter); counter+=5; } );
    }
}

> 2 7

16. Match the following delegate instances with the corresponding lambda expressions

1 -> d
2 -> c
3 -> a
4 -> b

16. Given the following list of students:

    List<Student> students = new List<Student> {
    new Student {First=“Terry", Last=“Adams", ID=111, Scores= new List<int> {97, 92, 81, 60}},
    new Student {First=“Claire", Last=“Adams", ID=112, Scores= new List<int> {75, 84, 91, 39}},
    new Student {First="Sven", Last="Mortensen",ID=113, Scores= new List<int> {88, 94, 65, 91}},
    new Student {First="Cesar", Last="Garcia", ID=114, Scores= new List<int> {97, 89, 85, 82}},
    new Student {First="Debra", Last="Garcia", ID=115, Scores= new List<int> {35, 72, 91, 70}}
    };

    from s in students
    orderby s.First ascending
    select new {
        Name = s.First + " " + s.Last,
        AvgScore = s.Scores.Average()
    };

or

    students.OrderBy(s => s.First)
        .Select(s => new {
            Name = s.First + " " + s.Last,
            AvgScore = s.Scores.Average()
        });

//What is the type of sortedStudents and sortedStudents2?
IEnumerable<‘a> where a is an anonymous type with a string Name and double AvgScore property

17. Given the following list of students:

    List<Student> students = new List<Student> {
    new Student {First=“Terry", Last=“Adams", ID=111, Scores= new List<int> {97, 92, 81, 60}},
    new Student {First=“Claire", Last=“Adams", ID=112, Scores= new List<int> {75, 84, 91, 39}},
    new Student {First="Sven", Last="Mortensen",ID=113, Scores= new List<int> {88, 94, 65, 91}},
    new Student {First="Cesar", Last="Garcia", ID=114, Scores= new List<int> {97, 89, 85, 82}},
    new Student {First="Debra", Last="Garcia", ID=115, Scores= new List<int> {35, 72, 91, 70}}
    };

from s in students
where s.Scores[0] > 60
groupby s.Last[0]
select s;

students.Where(s => s.Scores[0] > 60)
    .GroupBy(s => s.Last[0])
    .Select(s);

//What is the type of grouped and grouped2?
IEnumberable<IGrouping<char, Student>>

18. Given the following “students.xml” file. Write a method to read the contents of the file and print all the full time students with a grade > 60 on the console. A full-time student is a student for whom the type attribute is null.


    <?xml version="1.0" encoding="UTF-8" ?>
    <students>
        <student type="part-time">
            <ID>100</ID>
            <Name>Adrian Vincent</Name>
            <Grade>69</Grade>
            <Program>Geography</Program>
            <Age>19</Age>
        </student>
        <student>
            <ID>101</ID>
            <Name>Thomas Camacho</Name>
            <Grade>72</Grade>
            <Program>History</Program>
            <Age>18</Age>
        </student>
        <student>
            <ID>102</ID>
            <Name>Anne Parks</Name>
            <Grade>39</Grade>
            <Program>Science</Program>
            <Age>21</Age>
        </student>
    </students>

public void PrintPassingStudents()
{
    var studentsXML = XElement.Load("students.xml);
    var allStudents = studentsXML.Descendants("student);
    var fullTimeStudents = from s in allStudents
                        where s.Attribute("type") == null && int.Parse(s.Element("Grade").Value) > 60
                        select new {
                            Name = s.Element("Name").Value,
                            ID = s.Element("ID").Value,
                            Program = s.Element("Program").Value,
                            Grade = int.Parse(s.Element("Grade").Value),
                            Age = int.Parse(s.Element("Age").Value)
                        }
    var fullTimeStudents = allStudents.Where(s.Attribute("type") == null && int.Parse(s.Element("Grade").Value) > 60)
                        .Select(s => new {
                            Name = s.Element("Name").Value,
                            ID = s.Element("ID").Value,
                            Program = s.Element("Program").Value,
                            Grade = int.Parse(s.Element("Grade").Value),
                            Age = int.Parse(s.Element("Age").Value)
                        })
}
