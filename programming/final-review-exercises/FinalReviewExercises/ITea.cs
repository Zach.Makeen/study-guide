using System;

namespace FinalReviewExercises
{
    public interface ITea
    {
        string Name { get; }
        double Price { get; }
    }
}
