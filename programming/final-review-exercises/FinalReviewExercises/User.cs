using System;

namespace FinalReviewExercises
{
    class User
    {
        public string Username { get; }
        public string Email { get; }
        public User(string name, string email)
        {
            this.Username = name;
            this.Email = email;
        }
    }
}
