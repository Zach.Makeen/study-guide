using System;

namespace FinalReviewExercises
{
    public class Food
    {
        public string Name { get; }
        public double PricePerUnit { get; private set; }
        public Food(string name, double price)
        {
            this.Name = name;
            this.PricePerUnit = price;
        }
        public void AddPromotion(Del d)
        {
            this.PricePerUnit = d(this);
        }
    }
}
