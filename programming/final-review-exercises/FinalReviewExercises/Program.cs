﻿using System;
using System.Collections.Generic;

namespace FinalReviewExercises
{
    public delegate double Del(Food f);
    public delegate bool DelCheck(ITea t);
    public class Program
    {
        static void Main(string[] args)
        {
            // UserList list = new UserList();
            // string name = "Rodrigo";
            // Console.WriteLine($"Username: {list[name].Username} Email: {list[name].Email}");

            // Food chocolateAlmonds = new Food("Chocolate Almonds", 3);
            // chocolateAlmonds.AddPromotion(x => x.PricePerUnit - (x.PricePerUnit * 0.05));
            // Console.WriteLine(chocolateAlmonds.PricePerUnit);

            // ITea tea = new BubbleTea(new Tea());
            // Console.WriteLine($"{tea.Name} + {tea.Price}");
            // TeaShop shop = new TeaShop();
            // List<ITea> teas = shop.Filter(x => x.Name.Contains("milk") || x.Name.Contains("tapioca"));
            // foreach(ITea tea in teas)
            // {
            //     Console.WriteLine($"{tea.Name} - {tea.Price}");
            // }

            OnlineStore giftShop = new OnlineStore();
            giftShop.NewProduct += giftShop.CheckProduct;

            giftShop.AddProduct(new Product("Clock", 30));
            giftShop.AddProduct(new Product("Art", 150));
            giftShop.AddProduct(new Product("Rug", 400));

        }
    }
}
