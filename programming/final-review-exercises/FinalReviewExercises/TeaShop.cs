using System;
using System.Collections.Generic;

namespace FinalReviewExercises
{
    public class TeaShop
    {
        public List<ITea> teas;
        public TeaShop()
        {
            this.teas = new List<ITea>();
            this.teas.Add(new BubbleTea(new Tea()));
            this.teas.Add(new MilkTea(new Tea()));
        }
        public List<ITea> Filter(DelCheck d)
        {
            List<ITea> list = new List<ITea>();
            foreach(ITea tea in this.teas)
            {
                if (d(tea))
                {
                    list.Add(tea);
                }
            }
            return list;
        }
    }
}
