using System;
using System.Linq;

namespace FinalReviewExercises
{
    class UserList
    {
        private User[] array;
        public User this[string index]
        {
            get
            {
                // Option 1
                User user = this.array.Where(u => u.Username.Equals(index))
                    .Select(u => u)
                    .SingleOrDefault();

                // Option 2
                // User user = (from u in this.array
                //              where u.Username.Equals(index)
                //              select u).SingleOrDefault();

                // Option 3
                // User user = null;
                // foreach (User u in array)
                // {
                //     if(u.Username.Equals(index))
                //         user = u;
                // }

                if(user != null) return user;

                throw new ArgumentException($"User {index} does not exist!");
            }
        }
        public UserList()
        {
            this.array = new User[] {new User("Rodrigo", "rod@email.com"), new User("Albin", "albin@email.com"), new User("Castiel", "cas@email.com")};
        }
    }
}
