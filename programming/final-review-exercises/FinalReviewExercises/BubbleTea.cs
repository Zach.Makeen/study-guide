using System;

namespace FinalReviewExercises
{
    public class BubbleTea : TeaDecorator
    {
        public BubbleTea(ITea t) : base(t)
        {
        }
        public override string Name { get => base.Name + " with tapioca"; }
        public override double Price { get => base.Price + (base.Price * 0.02); }
    }
}
