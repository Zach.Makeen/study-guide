using System;

namespace Observer
{
    public class Counter
    {
        public int counter;
        public Counter(Ball ball)
        {
            ball.BounceEvent += IncreaseCounter;
        }
        private void IncreaseCounter()
        {
            this.counter++;
        }
    }
}