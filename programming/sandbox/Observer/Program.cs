﻿using System;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            Ball ball = new Ball();
            Counter counter = new Counter(ball);

            ball.BounceOffPaddle();

            Console.WriteLine(counter.counter);
        }
    }
}
