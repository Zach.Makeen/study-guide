using System;

namespace Observer
{
    public delegate void BounceEventHandler();
    public class Ball
    {
        //delegate instance 
        public event BounceEventHandler BounceEvent;

        public void BounceOffPaddle()
        {
            //fire an event => invoke the delegate instance
            // if (BounceEvent != null)
            //     BounceEvent();

            //OR use the Null-conditional operator!

            BounceEvent?.Invoke();
        }
    }
}
