using System;

namespace Struct
{
    struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
