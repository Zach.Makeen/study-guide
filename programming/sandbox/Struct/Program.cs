﻿using System;

namespace Struct
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p = new Point();
            Point q = p;
            q.X = 20;
            Console.WriteLine(p.X);
        }
    }
}
