using System;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            // Question to create/return new array from bigger array.
            int[,] arr = new int[3,5] {{1,2,3,4,5}, {6,7,8,9,10}, {11, 12, 13 ,14, 15}};
            int [,] newArr = ReduceArray(arr,0 ,0, 2, 5);
            PrintArray(newArr);

            /*  Create Delegate, returns bool takes in a Student Array with names.
                Create method that checks if name starts with A. Assign to delegate.
                Create method that returns number of times delegate returns True and takes as input Delegate.
            */
            string[] names = {"Arnold", "Makumba", "Alex", "Lorry", "America"};

            TestDel del = CheckIfA;

            // Should print 3.
            Console.WriteLine("Total Trues: " + CheckSum(del, names));

        }

        public delegate bool TestDel(string s);

        public static bool CheckIfA(String name)
        {
            if(name[0] == 'A')
            {
                return true;
            }
            return false;
        }

        public static int CheckSum(TestDel del, string[] arr)
        {
            int trueCount = 0;
            for(int i = 0; i< arr.Length; i++)
            {
                if(del(arr[i]))
                {
                    trueCount += 1;
                }
            }
            return trueCount;
        }

        public static int[,] ReduceArray(int[,] arr, int srow, int scol, int rows, int columns)
        {
            int realRow = 0 ;
            int realColumn = 0;
            int[,] returnArray = new int[rows, columns];

            for(int i = srow; i< rows; i++)
            {
                for(int j = scol; j< columns; j++)
                {
                    returnArray[realRow, realColumn] = arr[i,j];
                    realColumn+=1;
                }
                realRow += 1;
                realColumn = 0;
            }

            return returnArray;
        }

        public static void PrintArray(int [,] newArr)
        {
            for (int i = 0; i < newArr.GetLength(0); i++)
            {
                for (int j = 0; j < newArr.GetLength(1); j++)
                {
                    Console.Write(string.Format("{0} ", newArr[i, j]));
                }
            Console.Write(Environment.NewLine + Environment.NewLine);
            }
        }

    }
}






namespace Tests
{
    public class NegativeArray
    {
        private int[] arr ;

        public NegativeArray()
        {
            this.arr = new int[] {2, 5, 7, 8, 6};
        }

        public int this [int a]
        {
            get
            {
                if (a > this.arr.Length || a + this.arr.Length < 0 )
                {
                    throw new System.Exception();
                }

                if(a>0)
                {
                    return arr[a];
                }

                if(a<0)
                {
                    return arr[a+this.arr.Length];
                }
                return 0;

            }

        }
    }
}





/*  Create Indexer that can take negative and positive indexes.
      If positive, return normally.
      If negative, return in reverse, -1 being the final value.
          Ex: arr = {2, 5, 7, 8, 6}
              arr[1] = 5
              arr[-1] = 6
              arr[-3] = 7
      See NegativeArray.cs
  */

  NegativeArray negArray = new NegativeArray();

  //Should print 8
  Console.WriteLine(negArray[-2]);
