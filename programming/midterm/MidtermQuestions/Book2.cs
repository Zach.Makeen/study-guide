using System;

namespace MidtermQuestions
{
    class Book : IBook
    {
        private Random random = new Random();
        public string Title { get => "book" + random.Next(1, 20); }
        public double Price { get => random.Next(1, 20); }
    }
}
