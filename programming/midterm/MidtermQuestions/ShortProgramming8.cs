using System;

namespace MidtermQuestions
{
    class Matrix
    {
        public int[,] array;
        public Matrix()
        {
            this.array = new int[,] { { 1, 2, 4 }, { 3, 6, 5 }, { 7, 8, 3 }, { 9, 1, 0 } };
        }
        public int[,] Subset(int rowIndex, int columnIndex, int numRows, int numColumns)
        {
            int[,] sub = new int[numRows, numColumns];
            for (int i = 0; i < numRows; i++)
            {
                for (int j = 0; j < numColumns; j++)
                {
                    sub[i, j] = this.array[i + rowIndex, j + columnIndex];
                }
            }
            return sub;
        }
    }
}
