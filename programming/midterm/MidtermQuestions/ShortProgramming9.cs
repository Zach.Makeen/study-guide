using System;

namespace MidtermQuestions
{
    class ReversibleArray
    {
        public int[] array;
        public int this[int i]
        {
            get
            {
                if (i < 0 - this.array.Length || i > this.array.Length - 1)
                {
                    throw new IndexOutOfRangeException("Not within the appropriate range!");
                }
                else if (i >= 0)
                {
                    return array[i];
                } else {
                    return array[i + this.array.Length];
                }
            }
        }

        public ReversibleArray()
        {
            this.array = new int[] { 2, 4, 6, 8 };
        }
    }
}
