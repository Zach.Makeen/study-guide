1. 

> The property "Title" only had a get; meaning that it is read-only. Therefore you cannot assign a value without a set;

2. 

> In C#, every type inherits from the Object class which includes .ToString as one of its methods.

3. 

> The following throws an error because the Base class does not have a parameterless constructor. By default, when you create new instances of a derived class, it will call the base() constructor, but if it is no parameterless, you need to call : base(parameters). In this case we would add : base(x) to the constructor in the Derived class.

4. 

> In order for the Mansion class to be considered a decorator class as part of the decorator pattern, it must include a protected field of type IHouseDesign. This is because the decorator class need to have access to the interface's properties.

5. 

    public delegate double Func(double x);

    Calc(Math.Sqrt)

6. 

- DerivedMethodB(int)
- BaseMethodC
- DerivedMethodB(double)
- DerivedMethodC
- DerivedMethodB(double)

7. 

- 50
- 50
- 15
- 15