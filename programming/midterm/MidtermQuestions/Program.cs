﻿using System;

namespace MidtermQuestions
{
    public delegate bool DelCheck(IBook b);
    class Program
    {
        static void Main(string[] args)
        {
            // Short Programming 8
            // Matrix matrixOne = new Matrix();
            // int[,] array = matrixOne.Subset(0, 0, 3, 2);
            // foreach(int num in array)
            // {
            //     Console.WriteLine(num);
            // }

            // Short Programming 9
            // ReversibleArray reverse = new ReversibleArray();
            // for (int i = 0; i < reverse.array.Length; i++)
            // {
            //     Console.WriteLine(reverse.array[i]);
            // }

            // Short Programming 10
            // IProduct watch = new LuxuryWatch(new Watch());
            // Console.WriteLine(watch.Price);

            // Short Programming 11
            // DelCheck f = IsOnSale;
            // BookShelf shelf = new BookShelf();
            // Console.WriteLine(shelf.GetNumberOf(f));

        }

        public static bool IsOnSale(IBook b)
        {
            if(b.Price <= 10)
            {
                return true;
            }
            return false;
        }
    }
}
