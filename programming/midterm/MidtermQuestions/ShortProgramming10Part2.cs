using System;

namespace MidtermQuestions
{
    public class LuxuryWatch : ProductDecorator
    {
        public LuxuryWatch(IProduct p) : base(p)
        {
        }
        public override double Price { get => base.Price * 10; }
    }
}
