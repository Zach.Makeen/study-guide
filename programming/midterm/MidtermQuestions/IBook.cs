using System;

namespace MidtermQuestions
{
    public interface IBook 
    {
        string Title { get; }
        double Price { get; }
    }
}
