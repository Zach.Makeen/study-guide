using System;

namespace MidtermQuestions
{
    public interface IProduct
    {
        double Price { get; }
    }
}
