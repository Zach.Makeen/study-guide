using System;

namespace MidtermQuestions
{
    public abstract class ProductDecorator : IProduct
    {
        protected IProduct product;
        public ProductDecorator(IProduct p)
        {
            this.product = p;
        }
        public virtual double Price { get => this.product.Price; }
    }
}
