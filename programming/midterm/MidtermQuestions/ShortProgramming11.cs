using System;

namespace MidtermQuestions
{
    public class BookShelf
    {
        private IBook[] array;
        public BookShelf()
        {
            this.array = new IBook[] {new Book(), new Book(), new Book()};
        }

        public int GetNumberOf(DelCheck f)
        {
            int count = 0;
            foreach (IBook b in this.array)
            {
                if(f(b))
                {
                    count++;
                }
            }
            return count;
        }
    }
}
